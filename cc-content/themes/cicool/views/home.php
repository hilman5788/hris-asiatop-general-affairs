<?= get_header(); ?>
<body id="page-top">
   <?= get_navigation(); ?>

     <header>
      <div class="header-content" >
         <div class="header-content-inner">
            <h1 id="homeHeading">Employee Portal</h1>
            <hr>
            <p> Human Resources Information System - Asiatop</p>
            <a href="<?= base_url(); ?>administrator/login" class="btn btn-primary btn-xl page-scroll" target="blank"><i class="fa fa-signin"></i> SIGN IN</a>
         </div>
      </div>
   </header>
   <?= get_footer(); ?>