<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Employees Controller
*| --------------------------------------------------------------------------
*| Form Employees site
*|
*/
class Form_employees extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_employees');
	}

	/**
	* Submit Form Employeess
	*
	*/
	public function submit()
	{
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('departemen', 'Departemen', 'trim|required');
		$this->form_validation->set_rules('grade', 'Grade', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'nik' => $this->input->post('nik'),
				'departemen' => $this->input->post('departemen'),
				'grade' => $this->input->post('grade'),
				'address' => $this->input->post('address'),
			];

			
			$save_form_employees = $this->model_form_employees->store($save_data);

			$this->data['success'] = true;
			$this->data['id'] 	   = $save_form_employees;
			$this->data['message'] = cclang('your_data_has_been_successfully_submitted');
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	
}


/* End of file form_employees.php */
/* Location: ./application/controllers/administrator/Form Employees.php */