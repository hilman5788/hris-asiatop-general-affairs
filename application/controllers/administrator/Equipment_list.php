<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Equipment List Controller
*| --------------------------------------------------------------------------
*| Equipment List site
*|
*/
class Equipment_list extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_equipment_list');
	}

	/**
	* show all Equipment Lists
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('equipment_list_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['equipment_lists'] = $this->model_equipment_list->get($filter, $field, $this->limit_page, $offset);
		$this->data['equipment_list_counts'] = $this->model_equipment_list->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/equipment_list/index/',
			'total_rows'   => $this->model_equipment_list->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Equipment List List');
		$this->render('backend/standard/administrator/equipment_list/equipment_list_list', $this->data);
	}
	
	/**
	* Add new equipment_lists
	*
	*/
	public function add()
	{
		$this->is_allowed('equipment_list_add');

		$this->template->title('Equipment List New');
		$this->render('backend/standard/administrator/equipment_list/equipment_list_add', $this->data);
	}

	/**
	* Add New Equipment Lists
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('equipment_list_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('equipment_list_name', 'Equipment List Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'equipment_list_name' => $this->input->post('equipment_list_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'stock' => $this->input->post('stock'),
				'price' => $this->input->post('price'),
				'type' => $this->input->post('type'),
				'unit' => $this->input->post('unit'),
				'size' => $this->input->post('size'),
			];

			
			$save_equipment_list = $this->model_equipment_list->store($save_data);

			if ($save_equipment_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_equipment_list;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/equipment_list/edit/' . $save_equipment_list, 'Edit Equipment List'),
						anchor('administrator/equipment_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/equipment_list/edit/' . $save_equipment_list, 'Edit Equipment List')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/equipment_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/equipment_list');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Equipment Lists
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('equipment_list_update');

		$this->data['equipment_list'] = $this->model_equipment_list->find($id);

		$this->template->title('Equipment List Update');
		$this->render('backend/standard/administrator/equipment_list/equipment_list_update', $this->data);
	}

	/**
	* Update Equipment Lists
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('equipment_list_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('equipment_list_name', 'Equipment List Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('type', 'Type', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('size', 'Size', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'equipment_list_name' => $this->input->post('equipment_list_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'stock' => $this->input->post('stock'),
				'price' => $this->input->post('price'),
				'type' => $this->input->post('type'),
				'unit' => $this->input->post('unit'),
				'size' => $this->input->post('size'),
			];

			
			$save_equipment_list = $this->model_equipment_list->change($id, $save_data);

			if ($save_equipment_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/equipment_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/equipment_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/equipment_list');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Equipment Lists
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('equipment_list_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'equipment_list'), 'success');
        } else {
            set_message(cclang('error_delete', 'equipment_list'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Equipment Lists
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('equipment_list_view');

		$this->data['equipment_list'] = $this->model_equipment_list->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Equipment List Detail');
		$this->render('backend/standard/administrator/equipment_list/equipment_list_view', $this->data);
	}
	
	/**
	* delete Equipment Lists
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$equipment_list = $this->model_equipment_list->find($id);

		
		
		return $this->model_equipment_list->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('equipment_list_export');

		$this->model_equipment_list->export('equipment_list', 'equipment_list');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('equipment_list_export');

		$this->model_equipment_list->pdf('equipment_list', 'equipment_list');
	}
}


/* End of file equipment_list.php */
/* Location: ./application/controllers/administrator/Equipment List.php */