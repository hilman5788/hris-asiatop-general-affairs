<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Equipment Receive Controller
*| --------------------------------------------------------------------------
*| Equipment Receive site
*|
*/
class Equipment_receive extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_equipment_receive');
	}

	/**
	* show all Equipment Receives
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('equipment_receive_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['equipment_receives'] = $this->model_equipment_receive->get($filter, $field, $this->limit_page, $offset);
		$this->data['equipment_receive_counts'] = $this->model_equipment_receive->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/equipment_receive/index/',
			'total_rows'   => $this->model_equipment_receive->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Equipment Receive List');
		$this->render('backend/standard/administrator/equipment_receive/equipment_receive_list', $this->data);
	}
	
	/**
	* Add new equipment_receives
	*
	*/
	public function add()
	{
		$this->is_allowed('equipment_receive_add');

		$this->template->title('Equipment Receive New');
		$this->render('backend/standard/administrator/equipment_receive/equipment_receive_add', $this->data);
	}

	/**
	* Add New Equipment Receives
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('equipment_receive_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('equipment_receive_name', 'Equipment Receive Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_category', 'Goods Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'equipment_receive_name' => $this->input->post('equipment_receive_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'goods_category' => $this->input->post('goods_category'),
				'total' => $this->input->post('total'),
				'date' => $this->input->post('date'),
				'price' => $this->input->post('price'),
			];

			
			$save_equipment_receive = $this->model_equipment_receive->store($save_data);

			if ($save_equipment_receive) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_equipment_receive;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/equipment_receive/edit/' . $save_equipment_receive, 'Edit Equipment Receive'),
						anchor('administrator/equipment_receive', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/equipment_receive/edit/' . $save_equipment_receive, 'Edit Equipment Receive')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/equipment_receive');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/equipment_receive');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Equipment Receives
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('equipment_receive_update');

		$this->data['equipment_receive'] = $this->model_equipment_receive->find($id);

		$this->template->title('Equipment Receive Update');
		$this->render('backend/standard/administrator/equipment_receive/equipment_receive_update', $this->data);
	}

	/**
	* Update Equipment Receives
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('equipment_receive_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('equipment_receive_name', 'Equipment Receive Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_category', 'Goods Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'equipment_receive_name' => $this->input->post('equipment_receive_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'goods_category' => $this->input->post('goods_category'),
				'total' => $this->input->post('total'),
				'date' => $this->input->post('date'),
				'price' => $this->input->post('price'),
			];

			
			$save_equipment_receive = $this->model_equipment_receive->change($id, $save_data);

			if ($save_equipment_receive) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/equipment_receive', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/equipment_receive');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/equipment_receive');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Equipment Receives
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('equipment_receive_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'equipment_receive'), 'success');
        } else {
            set_message(cclang('error_delete', 'equipment_receive'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Equipment Receives
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('equipment_receive_view');

		$this->data['equipment_receive'] = $this->model_equipment_receive->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Equipment Receive Detail');
		$this->render('backend/standard/administrator/equipment_receive/equipment_receive_view', $this->data);
	}
	
	/**
	* delete Equipment Receives
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$equipment_receive = $this->model_equipment_receive->find($id);

		
		
		return $this->model_equipment_receive->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('equipment_receive_export');

		$this->model_equipment_receive->export('equipment_receive', 'equipment_receive');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('equipment_receive_export');

		$this->model_equipment_receive->pdf('equipment_receive', 'equipment_receive');
	}
}


/* End of file equipment_receive.php */
/* Location: ./application/controllers/administrator/Equipment Receive.php */