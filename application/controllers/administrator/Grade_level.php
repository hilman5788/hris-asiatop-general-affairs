<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Grade Level Controller
*| --------------------------------------------------------------------------
*| Grade Level site
*|
*/
class Grade_level extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_grade_level');
	}

	/**
	* show all Grade Levels
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('grade_level_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['grade_levels'] = $this->model_grade_level->get($filter, $field, $this->limit_page, $offset);
		$this->data['grade_level_counts'] = $this->model_grade_level->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/grade_level/index/',
			'total_rows'   => $this->model_grade_level->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Grade Level List');
		$this->render('backend/standard/administrator/grade_level/grade_level_list', $this->data);
	}
	
	/**
	* Add new grade_levels
	*
	*/
	public function add()
	{
		$this->is_allowed('grade_level_add');

		$this->template->title('Grade Level New');
		$this->render('backend/standard/administrator/grade_level/grade_level_add', $this->data);
	}

	/**
	* Add New Grade Levels
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('grade_level_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('grade_level_name', 'Grade Level Name', 'trim|required');
		$this->form_validation->set_rules('grade_level_range_min', 'Range Min', 'trim|required');
		$this->form_validation->set_rules('grade_level_range_max', 'Range Max', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'grade_level_name' => $this->input->post('grade_level_name'),
				'grade_level_range_min' => $this->input->post('grade_level_range_min'),
				'grade_level_range_max' => $this->input->post('grade_level_range_max'),
			];

			
			$save_grade_level = $this->model_grade_level->store($save_data);

			if ($save_grade_level) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_grade_level;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/grade_level/edit/' . $save_grade_level, 'Edit Grade Level'),
						anchor('administrator/grade_level', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/grade_level/edit/' . $save_grade_level, 'Edit Grade Level')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/grade_level');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/grade_level');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Grade Levels
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('grade_level_update');

		$this->data['grade_level'] = $this->model_grade_level->find($id);

		$this->template->title('Grade Level Update');
		$this->render('backend/standard/administrator/grade_level/grade_level_update', $this->data);
	}

	/**
	* Update Grade Levels
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('grade_level_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('grade_level_name', 'Grade Level Name', 'trim|required');
		$this->form_validation->set_rules('grade_level_range_min', 'Range Min', 'trim|required');
		$this->form_validation->set_rules('grade_level_range_max', 'Range Max', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'grade_level_name' => $this->input->post('grade_level_name'),
				'grade_level_range_min' => $this->input->post('grade_level_range_min'),
				'grade_level_range_max' => $this->input->post('grade_level_range_max'),
			];

			
			$save_grade_level = $this->model_grade_level->change($id, $save_data);

			if ($save_grade_level) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/grade_level', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/grade_level');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/grade_level');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Grade Levels
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('grade_level_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'grade_level'), 'success');
        } else {
            set_message(cclang('error_delete', 'grade_level'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Grade Levels
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('grade_level_view');

		$this->data['grade_level'] = $this->model_grade_level->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Grade Level Detail');
		$this->render('backend/standard/administrator/grade_level/grade_level_view', $this->data);
	}
	
	/**
	* delete Grade Levels
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$grade_level = $this->model_grade_level->find($id);

		
		
		return $this->model_grade_level->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('grade_level_export');

		$this->model_grade_level->export('grade_level', 'grade_level');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('grade_level_export');

		$this->model_grade_level->pdf('grade_level', 'grade_level');
	}
}


/* End of file grade_level.php */
/* Location: ./application/controllers/administrator/Grade Level.php */