<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Employees Controller
*| --------------------------------------------------------------------------
*| Form Employees site
*|
*/
class Form_employees extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_employees');
	}

	/**
	* show all Form Employeess
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('form_employees_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['form_employeess'] = $this->model_form_employees->get($filter, $field, $this->limit_page, $offset);
		$this->data['form_employees_counts'] = $this->model_form_employees->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/form_employees/index/',
			'total_rows'   => $this->model_form_employees->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Employees List');
		$this->render('backend/standard/administrator/form_builder/form_employees/form_employees_list', $this->data);
	}

	/**
	* Update view Form Employeess
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('form_employees_update');

		$this->data['form_employees'] = $this->model_form_employees->find($id);

		$this->template->title('Employees Update');
		$this->render('backend/standard/administrator/form_builder/form_employees/form_employees_update', $this->data);
	}

	/**
	* Update Form Employeess
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('form_employees_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nik', 'NIK', 'trim|required');
		$this->form_validation->set_rules('departemen', 'Departemen', 'trim|required');
		$this->form_validation->set_rules('grade', 'Grade', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
							'nik' => $this->input->post('nik'),
				'departemen' => $this->input->post('departemen'),
				'grade' => $this->input->post('grade'),
				'address' => $this->input->post('address'),
			];

			
			$save_form_employees = $this->model_form_employees->change($id, $save_data);

			if ($save_form_employees) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/form_employees', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/form_employees');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					set_message('Your data not change.', 'error');
					
            		$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/form_employees');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	/**
	* delete Form Employeess
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('form_employees_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'Form Employees'), 'success');
        } else {
            set_message(cclang('error_delete', 'Form Employees'), 'error');
        }

		redirect_back();
	}

	/**
	* View view Form Employeess
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('form_employees_view');

		$this->data['form_employees'] = $this->model_form_employees->find($id);

		$this->template->title('Employees Detail');
		$this->render('backend/standard/administrator/form_builder/form_employees/form_employees_view', $this->data);
	}

	/**
	* delete Form Employeess
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$form_employees = $this->model_form_employees->find($id);

		
		return $this->model_form_employees->remove($id);
	}
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('form_employees_export');

		$this->model_form_employees->export('form_employees', 'form_employees');
	}
}


/* End of file form_employees.php */
/* Location: ./application/controllers/administrator/Form Employees.php */