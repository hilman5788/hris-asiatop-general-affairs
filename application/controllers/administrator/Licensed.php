<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Licensed Controller
*| --------------------------------------------------------------------------
*| Licensed site
*|
*/
class Licensed extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_licensed');
	}

	/**
	* show all Licenseds
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('licensed_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['licenseds'] = $this->model_licensed->get($filter, $field, $this->limit_page, $offset);
		$this->data['licensed_counts'] = $this->model_licensed->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/licensed/index/',
			'total_rows'   => $this->model_licensed->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Licensed List');
		$this->render('backend/standard/administrator/licensed/licensed_list', $this->data);
	}
	
	/**
	* Add new licenseds
	*
	*/
	public function add()
	{
		$this->is_allowed('licensed_add');

		$this->template->title('Licensed New');
		$this->render('backend/standard/administrator/licensed/licensed_add', $this->data);
	}

	/**
	* Add New Licenseds
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('licensed_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('licensed_name', 'Licensed Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('label', 'Label', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('licensed_number', 'Licensed Number', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date_licensed', 'Date Licensed', 'trim|required');
		$this->form_validation->set_rules('licensed_type', 'Licensed Type', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('licensed_description', 'Licensed Description', 'trim|required');
		$this->form_validation->set_rules('attachment', 'Attachment', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'licensed_name' => $this->input->post('licensed_name'),
				'label' => $this->input->post('label'),
				'licensed_number' => $this->input->post('licensed_number'),
				'date_licensed' => $this->input->post('date_licensed'),
				'licensed_type' => $this->input->post('licensed_type'),
				'address' => $this->input->post('address'),
				'licensed_description' => $this->input->post('licensed_description'),
				'attachment' => $this->input->post('attachment'),
			];

			
			$save_licensed = $this->model_licensed->store($save_data);

			if ($save_licensed) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_licensed;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/licensed/edit/' . $save_licensed, 'Edit Licensed'),
						anchor('administrator/licensed', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/licensed/edit/' . $save_licensed, 'Edit Licensed')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/licensed');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/licensed');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Licenseds
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('licensed_update');

		$this->data['licensed'] = $this->model_licensed->find($id);

		$this->template->title('Licensed Update');
		$this->render('backend/standard/administrator/licensed/licensed_update', $this->data);
	}

	/**
	* Update Licenseds
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('licensed_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('licensed_name', 'Licensed Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('label', 'Label', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('licensed_number', 'Licensed Number', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date_licensed', 'Date Licensed', 'trim|required');
		$this->form_validation->set_rules('licensed_type', 'Licensed Type', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('licensed_description', 'Licensed Description', 'trim|required');
		$this->form_validation->set_rules('attachment', 'Attachment', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'licensed_name' => $this->input->post('licensed_name'),
				'label' => $this->input->post('label'),
				'licensed_number' => $this->input->post('licensed_number'),
				'date_licensed' => $this->input->post('date_licensed'),
				'licensed_type' => $this->input->post('licensed_type'),
				'address' => $this->input->post('address'),
				'licensed_description' => $this->input->post('licensed_description'),
				'attachment' => $this->input->post('attachment'),
			];

			
			$save_licensed = $this->model_licensed->change($id, $save_data);

			if ($save_licensed) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/licensed', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/licensed');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/licensed');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Licenseds
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('licensed_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'licensed'), 'success');
        } else {
            set_message(cclang('error_delete', 'licensed'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Licenseds
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('licensed_view');

		$this->data['licensed'] = $this->model_licensed->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Licensed Detail');
		$this->render('backend/standard/administrator/licensed/licensed_view', $this->data);
	}
	
	/**
	* delete Licenseds
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$licensed = $this->model_licensed->find($id);

		
		
		return $this->model_licensed->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('licensed_export');

		$this->model_licensed->export('licensed', 'licensed');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('licensed_export');

		$this->model_licensed->pdf('licensed', 'licensed');
	}
}


/* End of file licensed.php */
/* Location: ./application/controllers/administrator/Licensed.php */