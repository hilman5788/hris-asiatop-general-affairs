<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Vehicle Expenses Controller
*| --------------------------------------------------------------------------
*| Vehicle Expenses site
*|
*/
class Vehicle_expenses extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_vehicle_expenses');
	}

	/**
	* show all Vehicle Expensess
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('vehicle_expenses_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['vehicle_expensess'] = $this->model_vehicle_expenses->get($filter, $field, $this->limit_page, $offset);
		$this->data['vehicle_expenses_counts'] = $this->model_vehicle_expenses->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/vehicle_expenses/index/',
			'total_rows'   => $this->model_vehicle_expenses->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Vehicle Expenses List');
		$this->render('backend/standard/administrator/vehicle_expenses/vehicle_expenses_list', $this->data);
	}
	
	/**
	* Add new vehicle_expensess
	*
	*/
	public function add()
	{
		$this->is_allowed('vehicle_expenses_add');

		$this->template->title('Vehicle Expenses New');
		$this->render('backend/standard/administrator/vehicle_expenses/vehicle_expenses_add', $this->data);
	}

	/**
	* Add New Vehicle Expensess
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('vehicle_expenses_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('vehicle_expenses_name', 'Vehicle Expenses Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_category', 'Goods Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'vehicle_expenses_name' => $this->input->post('vehicle_expenses_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'goods_category' => $this->input->post('goods_category'),
				'total' => $this->input->post('total'),
				'date' => $this->input->post('date'),
				'price' => $this->input->post('price'),
			];

			
			$save_vehicle_expenses = $this->model_vehicle_expenses->store($save_data);

			if ($save_vehicle_expenses) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_vehicle_expenses;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/vehicle_expenses/edit/' . $save_vehicle_expenses, 'Edit Vehicle Expenses'),
						anchor('administrator/vehicle_expenses', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/vehicle_expenses/edit/' . $save_vehicle_expenses, 'Edit Vehicle Expenses')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/vehicle_expenses');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/vehicle_expenses');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Vehicle Expensess
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('vehicle_expenses_update');

		$this->data['vehicle_expenses'] = $this->model_vehicle_expenses->find($id);

		$this->template->title('Vehicle Expenses Update');
		$this->render('backend/standard/administrator/vehicle_expenses/vehicle_expenses_update', $this->data);
	}

	/**
	* Update Vehicle Expensess
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('vehicle_expenses_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('vehicle_expenses_name', 'Vehicle Expenses Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_name', 'Goods Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('goods_category', 'Goods Category', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'vehicle_expenses_name' => $this->input->post('vehicle_expenses_name'),
				'description' => $this->input->post('description'),
				'goods_name' => $this->input->post('goods_name'),
				'goods_category' => $this->input->post('goods_category'),
				'total' => $this->input->post('total'),
				'date' => $this->input->post('date'),
				'price' => $this->input->post('price'),
			];

			
			$save_vehicle_expenses = $this->model_vehicle_expenses->change($id, $save_data);

			if ($save_vehicle_expenses) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/vehicle_expenses', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/vehicle_expenses');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/vehicle_expenses');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Vehicle Expensess
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('vehicle_expenses_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'vehicle_expenses'), 'success');
        } else {
            set_message(cclang('error_delete', 'vehicle_expenses'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Vehicle Expensess
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('vehicle_expenses_view');

		$this->data['vehicle_expenses'] = $this->model_vehicle_expenses->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Vehicle Expenses Detail');
		$this->render('backend/standard/administrator/vehicle_expenses/vehicle_expenses_view', $this->data);
	}
	
	/**
	* delete Vehicle Expensess
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$vehicle_expenses = $this->model_vehicle_expenses->find($id);

		
		
		return $this->model_vehicle_expenses->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('vehicle_expenses_export');

		$this->model_vehicle_expenses->export('vehicle_expenses', 'vehicle_expenses');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('vehicle_expenses_export');

		$this->model_vehicle_expenses->pdf('vehicle_expenses', 'vehicle_expenses');
	}
}


/* End of file vehicle_expenses.php */
/* Location: ./application/controllers/administrator/Vehicle Expenses.php */