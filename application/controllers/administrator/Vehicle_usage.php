<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Vehicle Usage Controller
*| --------------------------------------------------------------------------
*| Vehicle Usage site
*|
*/
class Vehicle_usage extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_vehicle_usage');
	}

	/**
	* show all Vehicle Usages
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('vehicle_usage_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['vehicle_usages'] = $this->model_vehicle_usage->get($filter, $field, $this->limit_page, $offset);
		$this->data['vehicle_usage_counts'] = $this->model_vehicle_usage->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/vehicle_usage/index/',
			'total_rows'   => $this->model_vehicle_usage->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Vehicle Usage List');
		$this->render('backend/standard/administrator/vehicle_usage/vehicle_usage_list', $this->data);
	}
	
	/**
	* Add new vehicle_usages
	*
	*/
	public function add()
	{
		$this->is_allowed('vehicle_usage_add');

		$this->template->title('Vehicle Usage New');
		$this->render('backend/standard/administrator/vehicle_usage/vehicle_usage_add', $this->data);
	}

	/**
	* Add New Vehicle Usages
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('vehicle_usage_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('colour', 'Colour', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('years', 'Years', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'username' => $this->input->post('username'),
				'item_description' => $this->input->post('item_description'),
				'date' => $this->input->post('date'),
				'colour' => $this->input->post('colour'),
				'years' => $this->input->post('years'),
				'total' => $this->input->post('total'),
				'price' => $this->input->post('price'),
			];

			
			$save_vehicle_usage = $this->model_vehicle_usage->store($save_data);

			if ($save_vehicle_usage) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_vehicle_usage;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/vehicle_usage/edit/' . $save_vehicle_usage, 'Edit Vehicle Usage'),
						anchor('administrator/vehicle_usage', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/vehicle_usage/edit/' . $save_vehicle_usage, 'Edit Vehicle Usage')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/vehicle_usage');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/vehicle_usage');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Vehicle Usages
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('vehicle_usage_update');

		$this->data['vehicle_usage'] = $this->model_vehicle_usage->find($id);

		$this->template->title('Vehicle Usage Update');
		$this->render('backend/standard/administrator/vehicle_usage/vehicle_usage_update', $this->data);
	}

	/**
	* Update Vehicle Usages
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('vehicle_usage_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('colour', 'Colour', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('years', 'Years', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('total', 'Total', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'username' => $this->input->post('username'),
				'item_description' => $this->input->post('item_description'),
				'date' => $this->input->post('date'),
				'colour' => $this->input->post('colour'),
				'years' => $this->input->post('years'),
				'total' => $this->input->post('total'),
				'price' => $this->input->post('price'),
			];

			
			$save_vehicle_usage = $this->model_vehicle_usage->change($id, $save_data);

			if ($save_vehicle_usage) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/vehicle_usage', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/vehicle_usage');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/vehicle_usage');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Vehicle Usages
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('vehicle_usage_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'vehicle_usage'), 'success');
        } else {
            set_message(cclang('error_delete', 'vehicle_usage'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Vehicle Usages
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('vehicle_usage_view');

		$this->data['vehicle_usage'] = $this->model_vehicle_usage->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Vehicle Usage Detail');
		$this->render('backend/standard/administrator/vehicle_usage/vehicle_usage_view', $this->data);
	}
	
	/**
	* delete Vehicle Usages
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$vehicle_usage = $this->model_vehicle_usage->find($id);

		
		
		return $this->model_vehicle_usage->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('vehicle_usage_export');

		$this->model_vehicle_usage->export('vehicle_usage', 'vehicle_usage');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('vehicle_usage_export');

		$this->model_vehicle_usage->pdf('vehicle_usage', 'vehicle_usage');
	}
}


/* End of file vehicle_usage.php */
/* Location: ./application/controllers/administrator/Vehicle Usage.php */