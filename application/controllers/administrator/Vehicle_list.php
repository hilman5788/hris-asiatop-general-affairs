<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Vehicle List Controller
*| --------------------------------------------------------------------------
*| Vehicle List site
*|
*/
class Vehicle_list extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_vehicle_list');
	}

	/**
	* show all Vehicle Lists
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('vehicle_list_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['vehicle_lists'] = $this->model_vehicle_list->get($filter, $field, $this->limit_page, $offset);
		$this->data['vehicle_list_counts'] = $this->model_vehicle_list->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/vehicle_list/index/',
			'total_rows'   => $this->model_vehicle_list->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Vehicle List List');
		$this->render('backend/standard/administrator/vehicle_list/vehicle_list_list', $this->data);
	}
	
	/**
	* Add new vehicle_lists
	*
	*/
	public function add()
	{
		$this->is_allowed('vehicle_list_add');

		$this->template->title('Vehicle List New');
		$this->render('backend/standard/administrator/vehicle_list/vehicle_list_add', $this->data);
	}

	/**
	* Add New Vehicle Lists
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('vehicle_list_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('colour', 'Colour', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('years', 'Years', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('machine_number', 'Machine Number', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('merk', 'Merk', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('model', 'Model', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('number_chassis', 'Number Chassis', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'username' => $this->input->post('username'),
				'item_description' => $this->input->post('item_description'),
				'date' => $this->input->post('date'),
				'colour' => $this->input->post('colour'),
				'years' => $this->input->post('years'),
				'machine_number' => $this->input->post('machine_number'),
				'merk' => $this->input->post('merk'),
				'model' => $this->input->post('model'),
				'number_chassis' => $this->input->post('number_chassis'),
			];

			
			$save_vehicle_list = $this->model_vehicle_list->store($save_data);

			if ($save_vehicle_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_vehicle_list;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/vehicle_list/edit/' . $save_vehicle_list, 'Edit Vehicle List'),
						anchor('administrator/vehicle_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/vehicle_list/edit/' . $save_vehicle_list, 'Edit Vehicle List')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/vehicle_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/vehicle_list');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Vehicle Lists
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('vehicle_list_update');

		$this->data['vehicle_list'] = $this->model_vehicle_list->find($id);

		$this->template->title('Vehicle List Update');
		$this->render('backend/standard/administrator/vehicle_list/vehicle_list_update', $this->data);
	}

	/**
	* Update Vehicle Lists
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('vehicle_list_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date', 'Date', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('colour', 'Colour', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('years', 'Years', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('machine_number', 'Machine Number', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('merk', 'Merk', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('model', 'Model', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('number_chassis', 'Number Chassis', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'username' => $this->input->post('username'),
				'item_description' => $this->input->post('item_description'),
				'date' => $this->input->post('date'),
				'colour' => $this->input->post('colour'),
				'years' => $this->input->post('years'),
				'machine_number' => $this->input->post('machine_number'),
				'merk' => $this->input->post('merk'),
				'model' => $this->input->post('model'),
				'number_chassis' => $this->input->post('number_chassis'),
			];

			
			$save_vehicle_list = $this->model_vehicle_list->change($id, $save_data);

			if ($save_vehicle_list) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/vehicle_list', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/vehicle_list');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/vehicle_list');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Vehicle Lists
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('vehicle_list_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'vehicle_list'), 'success');
        } else {
            set_message(cclang('error_delete', 'vehicle_list'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Vehicle Lists
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('vehicle_list_view');

		$this->data['vehicle_list'] = $this->model_vehicle_list->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Vehicle List Detail');
		$this->render('backend/standard/administrator/vehicle_list/vehicle_list_view', $this->data);
	}
	
	/**
	* delete Vehicle Lists
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$vehicle_list = $this->model_vehicle_list->find($id);

		
		
		return $this->model_vehicle_list->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('vehicle_list_export');

		$this->model_vehicle_list->export('vehicle_list', 'vehicle_list');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('vehicle_list_export');

		$this->model_vehicle_list->pdf('vehicle_list', 'vehicle_list');
	}
}


/* End of file vehicle_list.php */
/* Location: ./application/controllers/administrator/Vehicle List.php */