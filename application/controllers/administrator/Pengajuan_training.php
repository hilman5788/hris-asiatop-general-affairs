<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Pengajuan Training Controller
*| --------------------------------------------------------------------------
*| Pengajuan Training site
*|
*/
class Pengajuan_training extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_pengajuan_training');
	}

	/**
	* show all Pengajuan Trainings
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('pengajuan_training_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['pengajuan_trainings'] = $this->model_pengajuan_training->get($filter, $field, $this->limit_page, $offset);
		$this->data['pengajuan_training_counts'] = $this->model_pengajuan_training->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/pengajuan_training/index/',
			'total_rows'   => $this->model_pengajuan_training->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Pengajuan Training List');
		$this->render('backend/standard/administrator/pengajuan_training/pengajuan_training_list', $this->data);
	}
	
	/**
	* Add new pengajuan_trainings
	*
	*/
	public function add()
	{
		$this->is_allowed('pengajuan_training_add');

		$this->template->title('Pengajuan Training New');
		$this->render('backend/standard/administrator/pengajuan_training/pengajuan_training_add', $this->data);
	}

	/**
	* Add New Pengajuan Trainings
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('pengajuan_training_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('nama_pemohon', 'Nama Pemohon', 'trim|required');
		$this->form_validation->set_rules('calon_peserta[]', 'Calon Peserta', 'trim|required');
		$this->form_validation->set_rules('jenis_training', 'Jenis Training', 'trim|required');
		$this->form_validation->set_rules('pengadaan_training', 'Pengadaan Training', 'trim|required');
		$this->form_validation->set_rules('nama_training', 'Nama Training', 'trim|required');
		$this->form_validation->set_rules('tujuan_training', 'Tujuan Training', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'nama_pemohon' => $this->input->post('nama_pemohon'),
				'calon_peserta' => implode(',', (array) $this->input->post('calon_peserta')),
				'jenis_training' => $this->input->post('jenis_training'),
				'pengadaan_training' => $this->input->post('pengadaan_training'),
				'nama_training' => $this->input->post('nama_training'),
				'deskripsi_training' => $this->input->post('deskripsi_training'),
				'tujuan_training' => $this->input->post('tujuan_training'),
				'alasan_pengajuan' => $this->input->post('alasan_pengajuan'),
				'nama_trainer' => $this->input->post('nama_trainer'),
				'lokasi_training' => $this->input->post('lokasi_training'),
				'lembaga_pelaksana' => $this->input->post('lembaga_pelaksana'),
				'tgl_mulai_training' => $this->input->post('tgl_mulai_training'),
				'tgl_selesai_training' => $this->input->post('tgl_selesai_training'),
				'lama_training' => $this->input->post('lama_training'),
				'jumlah_investasi' => $this->input->post('jumlah_investasi'),
				'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),
				'harapan_training' => $this->input->post('harapan_training'),
				'rencana_implementasi' => $this->input->post('rencana_implementasi'),
				'sharing_knowledge' => $this->input->post('sharing_knowledge'),
			];

			
			$save_pengajuan_training = $this->model_pengajuan_training->store($save_data);

			if ($save_pengajuan_training) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_pengajuan_training;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/pengajuan_training/edit/' . $save_pengajuan_training, 'Edit Pengajuan Training'),
						anchor('administrator/pengajuan_training', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/pengajuan_training/edit/' . $save_pengajuan_training, 'Edit Pengajuan Training')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pengajuan_training');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pengajuan_training');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Pengajuan Trainings
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('pengajuan_training_update');

		$this->data['pengajuan_training'] = $this->model_pengajuan_training->find($id);

		$this->template->title('Pengajuan Training Update');
		$this->render('backend/standard/administrator/pengajuan_training/pengajuan_training_update', $this->data);
	}

	/**
	* Update Pengajuan Trainings
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('pengajuan_training_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('nama_pemohon', 'Nama Pemohon', 'trim|required');
		$this->form_validation->set_rules('calon_peserta[]', 'Calon Peserta', 'trim|required');
		$this->form_validation->set_rules('jenis_training', 'Jenis Training', 'trim|required');
		$this->form_validation->set_rules('pengadaan_training', 'Pengadaan Training', 'trim|required');
		$this->form_validation->set_rules('nama_training', 'Nama Training', 'trim|required');
		$this->form_validation->set_rules('tujuan_training', 'Tujuan Training', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'id' => $this->input->post('id'),
				'nama_pemohon' => $this->input->post('nama_pemohon'),
				'calon_peserta' => implode(',', (array) $this->input->post('calon_peserta')),
				'jenis_training' => $this->input->post('jenis_training'),
				'pengadaan_training' => $this->input->post('pengadaan_training'),
				'nama_training' => $this->input->post('nama_training'),
				'deskripsi_training' => $this->input->post('deskripsi_training'),
				'tujuan_training' => $this->input->post('tujuan_training'),
				'alasan_pengajuan' => $this->input->post('alasan_pengajuan'),
				'nama_trainer' => $this->input->post('nama_trainer'),
				'lokasi_training' => $this->input->post('lokasi_training'),
				'lembaga_pelaksana' => $this->input->post('lembaga_pelaksana'),
				'tgl_mulai_training' => $this->input->post('tgl_mulai_training'),
				'tgl_selesai_training' => $this->input->post('tgl_selesai_training'),
				'lama_training' => $this->input->post('lama_training'),
				'jumlah_investasi' => $this->input->post('jumlah_investasi'),
				'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),
				'harapan_training' => $this->input->post('harapan_training'),
				'rencana_implementasi' => $this->input->post('rencana_implementasi'),
				'sharing_knowledge' => $this->input->post('sharing_knowledge'),
			];

			
			$save_pengajuan_training = $this->model_pengajuan_training->change($id, $save_data);

			if ($save_pengajuan_training) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/pengajuan_training', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/pengajuan_training');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/pengajuan_training');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Pengajuan Trainings
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('pengajuan_training_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'pengajuan_training'), 'success');
        } else {
            set_message(cclang('error_delete', 'pengajuan_training'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Pengajuan Trainings
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('pengajuan_training_view');

		$this->data['pengajuan_training'] = $this->model_pengajuan_training->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Pengajuan Training Detail');
		$this->render('backend/standard/administrator/pengajuan_training/pengajuan_training_view', $this->data);
	}
	
	/**
	* delete Pengajuan Trainings
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$pengajuan_training = $this->model_pengajuan_training->find($id);

		
		
		return $this->model_pengajuan_training->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('pengajuan_training_export');

		$this->model_pengajuan_training->export('pengajuan_training', 'pengajuan_training');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('pengajuan_training_export');

		$this->model_pengajuan_training->pdf('pengajuan_training', 'pengajuan_training');
	}
}


/* End of file pengajuan_training.php */
/* Location: ./application/controllers/administrator/Pengajuan Training.php */