<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Grade Job Controller
*| --------------------------------------------------------------------------
*| Grade Job site
*|
*/
class Grade_job extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_grade_job');
	}

	/**
	* show all Grade Jobs
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('grade_job_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['grade_jobs'] = $this->model_grade_job->get($filter, $field, $this->limit_page, $offset);
		$this->data['grade_job_counts'] = $this->model_grade_job->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/grade_job/index/',
			'total_rows'   => $this->model_grade_job->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Grade Job List');
		$this->render('backend/standard/administrator/grade_job/grade_job_list', $this->data);
	}
	
	/**
	* Add new grade_jobs
	*
	*/
	public function add()
	{
		$this->is_allowed('grade_job_add');

		$this->template->title('Grade Job New');
		$this->render('backend/standard/administrator/grade_job/grade_job_add', $this->data);
	}

	/**
	* Add New Grade Jobs
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('grade_job_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('grade_job_name', 'Grade Job Name', 'trim|required');
		$this->form_validation->set_rules('grade_range_min', 'Range Min', 'trim|required');
		$this->form_validation->set_rules('grade_range_max', 'Range Max', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'grade_job_name' => $this->input->post('grade_job_name'),
				'grade_range_min' => $this->input->post('grade_range_min'),
				'grade_range_max' => $this->input->post('grade_range_max'),
				'grade_rank' => $this->input->post('grade_rank'),
				'grade_title' => $this->input->post('grade_title'),
				'grade_job_title' => $this->input->post('grade_job_title'),
				'grade_sort' => $this->input->post('grade_sort'),
			];

			
			$save_grade_job = $this->model_grade_job->store($save_data);

			if ($save_grade_job) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_grade_job;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/grade_job/edit/' . $save_grade_job, 'Edit Grade Job'),
						anchor('administrator/grade_job', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/grade_job/edit/' . $save_grade_job, 'Edit Grade Job')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/grade_job');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/grade_job');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Grade Jobs
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('grade_job_update');

		$this->data['grade_job'] = $this->model_grade_job->find($id);

		$this->template->title('Grade Job Update');
		$this->render('backend/standard/administrator/grade_job/grade_job_update', $this->data);
	}

	/**
	* Update Grade Jobs
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('grade_job_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('grade_job_name', 'Grade Job Name', 'trim|required');
		$this->form_validation->set_rules('grade_range_min', 'Range Min', 'trim|required');
		$this->form_validation->set_rules('grade_range_max', 'Range Max', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'grade_job_name' => $this->input->post('grade_job_name'),
				'grade_range_min' => $this->input->post('grade_range_min'),
				'grade_range_max' => $this->input->post('grade_range_max'),
				'grade_rank' => $this->input->post('grade_rank'),
				'grade_title' => $this->input->post('grade_title'),
				'grade_job_title' => $this->input->post('grade_job_title'),
				'grade_sort' => $this->input->post('grade_sort'),
			];

			
			$save_grade_job = $this->model_grade_job->change($id, $save_data);

			if ($save_grade_job) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/grade_job', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/grade_job');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/grade_job');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Grade Jobs
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('grade_job_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'grade_job'), 'success');
        } else {
            set_message(cclang('error_delete', 'grade_job'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Grade Jobs
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('grade_job_view');

		$this->data['grade_job'] = $this->model_grade_job->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Grade Job Detail');
		$this->render('backend/standard/administrator/grade_job/grade_job_view', $this->data);
	}
	
	/**
	* delete Grade Jobs
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$grade_job = $this->model_grade_job->find($id);

		
		
		return $this->model_grade_job->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('grade_job_export');

		$this->model_grade_job->export('grade_job', 'grade_job');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('grade_job_export');

		$this->model_grade_job->pdf('grade_job', 'grade_job');
	}
}


/* End of file grade_job.php */
/* Location: ./application/controllers/administrator/Grade Job.php */