<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Suplies Request Controller
*| --------------------------------------------------------------------------
*| Suplies Request site
*|
*/
class Suplies_request extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_suplies_request');
	}

	/**
	* show all Suplies Requests
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('suplies_request_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['suplies_requests'] = $this->model_suplies_request->get($filter, $field, $this->limit_page, $offset);
		$this->data['suplies_request_counts'] = $this->model_suplies_request->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/suplies_request/index/',
			'total_rows'   => $this->model_suplies_request->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Suplies Request List');
		$this->render('backend/standard/administrator/suplies_request/suplies_request_list', $this->data);
	}
	
	/**
	* Add new suplies_requests
	*
	*/
	public function add()
	{
		$this->is_allowed('suplies_request_add');

		$this->template->title('Suplies Request New');
		$this->render('backend/standard/administrator/suplies_request/suplies_request_add', $this->data);
	}

	/**
	* Add New Suplies Requests
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('suplies_request_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('suplies_request_name', 'Suplies Request Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('depatment', 'Depatment', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date_request', 'Date Request', 'trim|required');
		$this->form_validation->set_rules('date_needed', 'Date Needed', 'trim|required');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('item_number', 'Item Number', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'suplies_request_name' => $this->input->post('suplies_request_name'),
				'description' => $this->input->post('description'),
				'depatment' => $this->input->post('depatment'),
				'date_request' => $this->input->post('date_request'),
				'date_needed' => $this->input->post('date_needed'),
				'item_description' => $this->input->post('item_description'),
				'item_number' => $this->input->post('item_number'),
				'price' => $this->input->post('price'),
				'quantity' => $this->input->post('quantity'),
			];

			
			$save_suplies_request = $this->model_suplies_request->store($save_data);

			if ($save_suplies_request) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_suplies_request;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/suplies_request/edit/' . $save_suplies_request, 'Edit Suplies Request'),
						anchor('administrator/suplies_request', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/suplies_request/edit/' . $save_suplies_request, 'Edit Suplies Request')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_request');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_request');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Suplies Requests
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('suplies_request_update');

		$this->data['suplies_request'] = $this->model_suplies_request->find($id);

		$this->template->title('Suplies Request Update');
		$this->render('backend/standard/administrator/suplies_request/suplies_request_update', $this->data);
	}

	/**
	* Update Suplies Requests
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('suplies_request_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('suplies_request_name', 'Suplies Request Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('description', 'Description', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('depatment', 'Depatment', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('date_request', 'Date Request', 'trim|required');
		$this->form_validation->set_rules('date_needed', 'Date Needed', 'trim|required');
		$this->form_validation->set_rules('item_description', 'Item Description', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('item_number', 'Item Number', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'suplies_request_name' => $this->input->post('suplies_request_name'),
				'description' => $this->input->post('description'),
				'depatment' => $this->input->post('depatment'),
				'date_request' => $this->input->post('date_request'),
				'date_needed' => $this->input->post('date_needed'),
				'item_description' => $this->input->post('item_description'),
				'item_number' => $this->input->post('item_number'),
				'price' => $this->input->post('price'),
				'quantity' => $this->input->post('quantity'),
			];

			
			$save_suplies_request = $this->model_suplies_request->change($id, $save_data);

			if ($save_suplies_request) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/suplies_request', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/suplies_request');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/suplies_request');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Suplies Requests
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('suplies_request_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'suplies_request'), 'success');
        } else {
            set_message(cclang('error_delete', 'suplies_request'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Suplies Requests
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('suplies_request_view');

		$this->data['suplies_request'] = $this->model_suplies_request->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Suplies Request Detail');
		$this->render('backend/standard/administrator/suplies_request/suplies_request_view', $this->data);
	}
	
	/**
	* delete Suplies Requests
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$suplies_request = $this->model_suplies_request->find($id);

		
		
		return $this->model_suplies_request->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('suplies_request_export');

		$this->model_suplies_request->export('suplies_request', 'suplies_request');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('suplies_request_export');

		$this->model_suplies_request->pdf('suplies_request', 'suplies_request');
	}
}


/* End of file suplies_request.php */
/* Location: ./application/controllers/administrator/Suplies Request.php */