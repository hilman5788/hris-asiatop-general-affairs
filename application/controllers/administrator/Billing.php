<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Billing Controller
*| --------------------------------------------------------------------------
*| Billing site
*|
*/
class Billing extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_billing');
	}

	/**
	* show all Billings
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('billing_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['billings'] = $this->model_billing->get($filter, $field, $this->limit_page, $offset);
		$this->data['billing_counts'] = $this->model_billing->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/billing/index/',
			'total_rows'   => $this->model_billing->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Billing List');
		$this->render('backend/standard/administrator/billing/billing_list', $this->data);
	}
	
	/**
	* Add new billings
	*
	*/
	public function add()
	{
		$this->is_allowed('billing_add');

		$this->template->title('Billing New');
		$this->render('backend/standard/administrator/billing/billing_add', $this->data);
	}

	/**
	* Add New Billings
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('billing_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('billing_number', 'Billing Number', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
		$this->form_validation->set_rules('due_date', 'Due Date', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('cost', 'Cost', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('billing_descrpition', 'Billing Descrpition', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'billing_number' => $this->input->post('billing_number'),
				'username' => $this->input->post('username'),
				'payment_date' => $this->input->post('payment_date'),
				'due_date' => $this->input->post('due_date'),
				'address' => $this->input->post('address'),
				'cost' => $this->input->post('cost'),
				'quantity' => $this->input->post('quantity'),
				'billing_descrpition' => $this->input->post('billing_descrpition'),
			];

			
			$save_billing = $this->model_billing->store($save_data);

			if ($save_billing) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_billing;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/billing/edit/' . $save_billing, 'Edit Billing'),
						anchor('administrator/billing', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/billing/edit/' . $save_billing, 'Edit Billing')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/billing');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/billing');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Billings
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('billing_update');

		$this->data['billing'] = $this->model_billing->find($id);

		$this->template->title('Billing Update');
		$this->render('backend/standard/administrator/billing/billing_update', $this->data);
	}

	/**
	* Update Billings
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('billing_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('billing_number', 'Billing Number', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required');
		$this->form_validation->set_rules('due_date', 'Due Date', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('cost', 'Cost', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('quantity', 'Quantity', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('billing_descrpition', 'Billing Descrpition', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'billing_number' => $this->input->post('billing_number'),
				'username' => $this->input->post('username'),
				'payment_date' => $this->input->post('payment_date'),
				'due_date' => $this->input->post('due_date'),
				'address' => $this->input->post('address'),
				'cost' => $this->input->post('cost'),
				'quantity' => $this->input->post('quantity'),
				'billing_descrpition' => $this->input->post('billing_descrpition'),
			];

			
			$save_billing = $this->model_billing->change($id, $save_data);

			if ($save_billing) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/billing', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/billing');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/billing');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Billings
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('billing_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'billing'), 'success');
        } else {
            set_message(cclang('error_delete', 'billing'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Billings
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('billing_view');

		$this->data['billing'] = $this->model_billing->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Billing Detail');
		$this->render('backend/standard/administrator/billing/billing_view', $this->data);
	}
	
	/**
	* delete Billings
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$billing = $this->model_billing->find($id);

		
		
		return $this->model_billing->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('billing_export');

		$this->model_billing->export('billing', 'billing');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('billing_export');

		$this->model_billing->pdf('billing', 'billing');
	}
}


/* End of file billing.php */
/* Location: ./application/controllers/administrator/Billing.php */