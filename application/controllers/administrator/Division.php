<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Division Controller
*| --------------------------------------------------------------------------
*| Division site
*|
*/
class Division extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_division');
	}

	/**
	* show all Divisions
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('division_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['divisions'] = $this->model_division->get($filter, $field, $this->limit_page, $offset);
		$this->data['division_counts'] = $this->model_division->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/division/index/',
			'total_rows'   => $this->model_division->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Division List');
		$this->render('backend/standard/administrator/division/division_list', $this->data);
	}
	
	/**
	* Add new divisions
	*
	*/
	public function add()
	{
		$this->is_allowed('division_add');

		$this->template->title('Division New');
		$this->render('backend/standard/administrator/division/division_add', $this->data);
	}

	/**
	* Add New Divisions
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('division_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('division_name', 'Division Name', 'trim|required');
		$this->form_validation->set_rules('division_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('directorate_id', 'Directorate Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'division_name' => $this->input->post('division_name'),
				'division_grade' => $this->input->post('division_grade'),
				'division_rank' => $this->input->post('division_rank'),
				'division_title' => $this->input->post('division_title'),
				'division_job_title' => $this->input->post('division_job_title'),
				'directorate_id' => $this->input->post('directorate_id'),
			];

			
			$save_division = $this->model_division->store($save_data);

			if ($save_division) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_division;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/division/edit/' . $save_division, 'Edit Division'),
						anchor('administrator/division', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/division/edit/' . $save_division, 'Edit Division')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/division');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/division');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Divisions
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('division_update');

		$this->data['division'] = $this->model_division->find($id);

		$this->template->title('Division Update');
		$this->render('backend/standard/administrator/division/division_update', $this->data);
	}

	/**
	* Update Divisions
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('division_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('division_name', 'Division Name', 'trim|required');
		$this->form_validation->set_rules('division_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('directorate_id', 'Directorate Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'division_name' => $this->input->post('division_name'),
				'division_grade' => $this->input->post('division_grade'),
				'division_rank' => $this->input->post('division_rank'),
				'division_title' => $this->input->post('division_title'),
				'division_job_title' => $this->input->post('division_job_title'),
				'directorate_id' => $this->input->post('directorate_id'),
			];

			
			$save_division = $this->model_division->change($id, $save_data);

			if ($save_division) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/division', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/division');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/division');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Divisions
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('division_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'division'), 'success');
        } else {
            set_message(cclang('error_delete', 'division'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Divisions
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('division_view');

		$this->data['division'] = $this->model_division->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Division Detail');
		$this->render('backend/standard/administrator/division/division_view', $this->data);
	}
	
	/**
	* delete Divisions
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$division = $this->model_division->find($id);

		
		
		return $this->model_division->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('division_export');

		$this->model_division->export('division', 'division');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('division_export');

		$this->model_division->pdf('division', 'division');
	}
}


/* End of file division.php */
/* Location: ./application/controllers/administrator/Division.php */