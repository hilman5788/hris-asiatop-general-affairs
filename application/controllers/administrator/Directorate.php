<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Directorate Controller
*| --------------------------------------------------------------------------
*| Directorate site
*|
*/
class Directorate extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_directorate');
	}

	/**
	* show all Directorates
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('directorate_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['directorates'] = $this->model_directorate->get($filter, $field, $this->limit_page, $offset);
		$this->data['directorate_counts'] = $this->model_directorate->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/directorate/index/',
			'total_rows'   => $this->model_directorate->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Directorate List');
		$this->render('backend/standard/administrator/directorate/directorate_list', $this->data);
	}
	
	/**
	* Add new directorates
	*
	*/
	public function add()
	{
		$this->is_allowed('directorate_add');

		$this->template->title('Directorate New');
		$this->render('backend/standard/administrator/directorate/directorate_add', $this->data);
	}

	/**
	* Add New Directorates
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('directorate_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('directorate_name', 'Directorate Name', 'trim|required');
		$this->form_validation->set_rules('directorate_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('corporate_id', 'Corporate Name', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'directorate_name' => $this->input->post('directorate_name'),
				'directorate_grade' => $this->input->post('directorate_grade'),
				'directorate_rank' => $this->input->post('directorate_rank'),
				'directorate_job_title' => $this->input->post('directorate_job_title'),
				'corporate_id' => $this->input->post('corporate_id'),
			];

			
			$save_directorate = $this->model_directorate->store($save_data);

			if ($save_directorate) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_directorate;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/directorate/edit/' . $save_directorate, 'Edit Directorate'),
						anchor('administrator/directorate', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/directorate/edit/' . $save_directorate, 'Edit Directorate')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/directorate');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/directorate');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Directorates
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('directorate_update');

		$this->data['directorate'] = $this->model_directorate->find($id);

		$this->template->title('Directorate Update');
		$this->render('backend/standard/administrator/directorate/directorate_update', $this->data);
	}

	/**
	* Update Directorates
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('directorate_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('directorate_name', 'Directorate Name', 'trim|required');
		$this->form_validation->set_rules('directorate_grade', 'Grade Level', 'trim|required');
		$this->form_validation->set_rules('corporate_id', 'Corporate Name', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'directorate_name' => $this->input->post('directorate_name'),
				'directorate_grade' => $this->input->post('directorate_grade'),
				'directorate_rank' => $this->input->post('directorate_rank'),
				'directorate_job_title' => $this->input->post('directorate_job_title'),
				'corporate_id' => $this->input->post('corporate_id'),
			];

			
			$save_directorate = $this->model_directorate->change($id, $save_data);

			if ($save_directorate) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/directorate', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/directorate');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/directorate');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Directorates
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('directorate_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'directorate'), 'success');
        } else {
            set_message(cclang('error_delete', 'directorate'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Directorates
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('directorate_view');

		$this->data['directorate'] = $this->model_directorate->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Directorate Detail');
		$this->render('backend/standard/administrator/directorate/directorate_view', $this->data);
	}
	
	/**
	* delete Directorates
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$directorate = $this->model_directorate->find($id);

		
		
		return $this->model_directorate->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('directorate_export');

		$this->model_directorate->export('directorate', 'directorate');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('directorate_export');

		$this->model_directorate->pdf('directorate', 'directorate');
	}
}


/* End of file directorate.php */
/* Location: ./application/controllers/administrator/Directorate.php */