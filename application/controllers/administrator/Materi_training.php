<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Materi Training Controller
*| --------------------------------------------------------------------------
*| Materi Training site
*|
*/
class Materi_training extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_materi_training');
	}

	/**
	* show all Materi Trainings
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('materi_training_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['materi_trainings'] = $this->model_materi_training->get($filter, $field, $this->limit_page, $offset);
		$this->data['materi_training_counts'] = $this->model_materi_training->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/materi_training/index/',
			'total_rows'   => $this->model_materi_training->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Materi Training List');
		$this->render('backend/standard/administrator/materi_training/materi_training_list', $this->data);
	}
	
	/**
	* Add new materi_trainings
	*
	*/
	public function add()
	{
		$this->is_allowed('materi_training_add');

		$this->template->title('Materi Training New');
		$this->render('backend/standard/administrator/materi_training/materi_training_add', $this->data);
	}

	/**
	* Add New Materi Trainings
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('materi_training_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('materi_training', 'Materi Training', 'trim|required');
		$this->form_validation->set_rules('materi_training_file_training_name[]', 'File Training', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'materi_training' => $this->input->post('materi_training'),
			];

			if (!is_dir(FCPATH . '/uploads/materi_training/')) {
				mkdir(FCPATH . '/uploads/materi_training/');
			}

			if (count((array) $this->input->post('materi_training_file_training_name'))) {
				foreach ((array) $_POST['materi_training_file_training_name'] as $idx => $file_name) {
					$materi_training_file_training_name_copy = date('YmdHis') . '-' . $file_name;

					rename(FCPATH . 'uploads/tmp/' . $_POST['materi_training_file_training_uuid'][$idx] . '/' .  $file_name, 
							FCPATH . 'uploads/materi_training/' . $materi_training_file_training_name_copy);

					$listed_image[] = $materi_training_file_training_name_copy;

					if (!is_file(FCPATH . '/uploads/materi_training/' . $materi_training_file_training_name_copy)) {
						echo json_encode([
							'success' => false,
							'message' => 'Error uploading file'
							]);
						exit;
					}
				}

				$save_data['file_training'] = implode($listed_image, ',');
			}
		
			
			$save_materi_training = $this->model_materi_training->store($save_data);

			if ($save_materi_training) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_materi_training;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/materi_training/edit/' . $save_materi_training, 'Edit Materi Training'),
						anchor('administrator/materi_training', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/materi_training/edit/' . $save_materi_training, 'Edit Materi Training')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/materi_training');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/materi_training');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Materi Trainings
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('materi_training_update');

		$this->data['materi_training'] = $this->model_materi_training->find($id);

		$this->template->title('Materi Training Update');
		$this->render('backend/standard/administrator/materi_training/materi_training_update', $this->data);
	}

	/**
	* Update Materi Trainings
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('materi_training_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('materi_training', 'Materi Training', 'trim|required');
		$this->form_validation->set_rules('materi_training_file_training_name[]', 'File Training', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'materi_training' => $this->input->post('materi_training'),
			];

			$listed_image = [];
			if (count((array) $this->input->post('materi_training_file_training_name'))) {
				foreach ((array) $_POST['materi_training_file_training_name'] as $idx => $file_name) {
					if (isset($_POST['materi_training_file_training_uuid'][$idx]) AND !empty($_POST['materi_training_file_training_uuid'][$idx])) {
						$materi_training_file_training_name_copy = date('YmdHis') . '-' . $file_name;

						rename(FCPATH . 'uploads/tmp/' . $_POST['materi_training_file_training_uuid'][$idx] . '/' .  $file_name, 
								FCPATH . 'uploads/materi_training/' . $materi_training_file_training_name_copy);

						$listed_image[] = $materi_training_file_training_name_copy;

						if (!is_file(FCPATH . '/uploads/materi_training/' . $materi_training_file_training_name_copy)) {
							echo json_encode([
								'success' => false,
								'message' => 'Error uploading file'
								]);
							exit;
						}
					} else {
						$listed_image[] = $file_name;
					}
				}
			}
			
			$save_data['file_training'] = implode($listed_image, ',');
		
			
			$save_materi_training = $this->model_materi_training->change($id, $save_data);

			if ($save_materi_training) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/materi_training', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/materi_training');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/materi_training');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Materi Trainings
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('materi_training_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'materi_training'), 'success');
        } else {
            set_message(cclang('error_delete', 'materi_training'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Materi Trainings
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('materi_training_view');

		$this->data['materi_training'] = $this->model_materi_training->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Materi Training Detail');
		$this->render('backend/standard/administrator/materi_training/materi_training_view', $this->data);
	}
	
	/**
	* delete Materi Trainings
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$materi_training = $this->model_materi_training->find($id);

		
		if (!empty($materi_training->file_training)) {
			foreach ((array) explode(',', $materi_training->file_training) as $filename) {
				$path = FCPATH . '/uploads/materi_training/' . $filename;

				if (is_file($path)) {
					$delete_file = unlink($path);
				}
			}
		}
		
		return $this->model_materi_training->remove($id);
	}
	
	
	/**
	* Upload Image Materi Training	* 
	* @return JSON
	*/
	public function upload_file_training_file()
	{
		if (!$this->is_allowed('materi_training_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'materi_training',
		]);
	}

	/**
	* Delete Image Materi Training	* 
	* @return JSON
	*/
	public function delete_file_training_file($uuid)
	{
		if (!$this->is_allowed('materi_training_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'file_training', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'materi_training',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/materi_training/'
        ]);
	}

	/**
	* Get Image Materi Training	* 
	* @return JSON
	*/
	public function get_file_training_file($id)
	{
		if (!$this->is_allowed('materi_training_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$materi_training = $this->model_materi_training->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'file_training', 
            'table_name'        => 'materi_training',
            'primary_key'       => 'id',
            'upload_path'       => 'uploads/materi_training/',
            'delete_endpoint'   => 'administrator/materi_training/delete_file_training_file'
        ]);
	}
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('materi_training_export');

		$this->model_materi_training->export('materi_training', 'materi_training');
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('materi_training_export');

		$this->model_materi_training->pdf('materi_training', 'materi_training');
	}
}


/* End of file materi_training.php */
/* Location: ./application/controllers/administrator/Materi Training.php */