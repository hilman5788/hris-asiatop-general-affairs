<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Naskah_nusantara extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_naskah_nusantara');
	}

	/**
	 * @api {get} /naskah_nusantara/all Get all naskah_nusantaras.
	 * @apiVersion 0.1.0
	 * @apiName AllNaskahnusantara 
	 * @apiGroup naskah_nusantara
	 * @apiHeader {String} X-Api-Key Naskah nusantaras unique access-key.
	 * @apiHeader {String} X-Token Naskah nusantaras unique token.
	 * @apiPermission Naskah nusantara Cant be Accessed permission name : api_naskah_nusantara_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Naskah nusantaras.
	 * @apiParam {String} [Field="All Field"] Optional field of Naskah nusantaras : id, jenis_bahan, judul, pengarang, deskripsi_fisik, informasi_teknis, subjek, abstrak, catatan, file_cover.
	 * @apiParam {String} [Start=0] Optional start index of Naskah nusantaras.
	 * @apiParam {String} [Limit=10] Optional limit data of Naskah nusantaras.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of naskah_nusantara.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataNaskah nusantara Naskah nusantara data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_naskah_nusantara_all');

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['id', 'jenis_bahan', 'judul', 'pengarang', 'deskripsi_fisik', 'informasi_teknis', 'subjek', 'abstrak', 'catatan', 'file_cover'];
		$naskah_nusantaras = $this->model_api_naskah_nusantara->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_naskah_nusantara->count_all($filter, $field);

		$data['naskah_nusantara'] = $naskah_nusantaras;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Naskah nusantara',
			'data'	 	=> $data,
			'total' 	=> $total
		], API::HTTP_OK);
	}

	
	/**
	 * @api {get} /naskah_nusantara/detail Detail Naskah nusantara.
	 * @apiVersion 0.1.0
	 * @apiName DetailNaskah nusantara
	 * @apiGroup naskah_nusantara
	 * @apiHeader {String} X-Api-Key Naskah nusantaras unique access-key.
	 * @apiHeader {String} X-Token Naskah nusantaras unique token.
	 * @apiPermission Naskah nusantara Cant be Accessed permission name : api_naskah_nusantara_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Naskah nusantaras.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of naskah_nusantara.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError Naskah nusantaraNotFound Naskah nusantara data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_naskah_nusantara_detail');

		$this->requiredInput(['id']);

		$id = $this->get('id');

		$select_field = ['id', 'jenis_bahan', 'judul', 'pengarang', 'deskripsi_fisik', 'informasi_teknis', 'subjek', 'abstrak', 'catatan', 'file_cover'];
		$data['naskah_nusantara'] = $this->model_api_naskah_nusantara->find($id, $select_field);

		if ($data['naskah_nusantara']) {
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Naskah nusantara',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Naskah nusantara not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /naskah_nusantara/add Add Naskah nusantara.
	 * @apiVersion 0.1.0
	 * @apiName AddNaskah nusantara
	 * @apiGroup naskah_nusantara
	 * @apiHeader {String} X-Api-Key Naskah nusantaras unique access-key.
	 * @apiHeader {String} X-Token Naskah nusantaras unique token.
	 * @apiPermission Naskah nusantara Cant be Accessed permission name : api_naskah_nusantara_add
	 *
 	 * @apiParam {String} Jenis_bahan Mandatory jenis_bahan of Naskah nusantaras. Input Jenis Bahan Max Length : 20. 
	 * @apiParam {String} Judul Mandatory judul of Naskah nusantaras. Input Judul Max Length : 100. 
	 * @apiParam {String} Pengarang Mandatory pengarang of Naskah nusantaras. Input Pengarang Max Length : 50. 
	 * @apiParam {String} Deskripsi_fisik Mandatory deskripsi_fisik of Naskah nusantaras. Input Deskripsi Fisik Max Length : 255. 
	 * @apiParam {String} Informasi_teknis Mandatory informasi_teknis of Naskah nusantaras. Input Informasi Teknis Max Length : 255. 
	 * @apiParam {String} Subjek Mandatory subjek of Naskah nusantaras. Input Subjek Max Length : 20. 
	 * @apiParam {String} Abstrak Mandatory abstrak of Naskah nusantaras. Input Abstrak Max Length : 150. 
	 * @apiParam {String} Catatan Mandatory catatan of Naskah nusantaras. Input Catatan Max Length : 255. 
	 * @apiParam {String} File_cover Mandatory file_cover of Naskah nusantaras. Input File Cover Max Length : 150. 
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_naskah_nusantara_add');

		$this->form_validation->set_rules('jenis_bahan', 'Jenis Bahan', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('pengarang', 'Pengarang', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('deskripsi_fisik', 'Deskripsi Fisik', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('informasi_teknis', 'Informasi Teknis', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('subjek', 'Subjek', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('abstrak', 'Abstrak', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('catatan', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('file_cover', 'File Cover', 'trim|required|max_length[150]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'jenis_bahan' => $this->input->post('jenis_bahan'),
				'judul' => $this->input->post('judul'),
				'pengarang' => $this->input->post('pengarang'),
				'deskripsi_fisik' => $this->input->post('deskripsi_fisik'),
				'informasi_teknis' => $this->input->post('informasi_teknis'),
				'subjek' => $this->input->post('subjek'),
				'abstrak' => $this->input->post('abstrak'),
				'catatan' => $this->input->post('catatan'),
				'file_cover' => $this->input->post('file_cover'),
			];
			
			$save_naskah_nusantara = $this->model_api_naskah_nusantara->store($save_data);

			if ($save_naskah_nusantara) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /naskah_nusantara/update Update Naskah nusantara.
	 * @apiVersion 0.1.0
	 * @apiName UpdateNaskah nusantara
	 * @apiGroup naskah_nusantara
	 * @apiHeader {String} X-Api-Key Naskah nusantaras unique access-key.
	 * @apiHeader {String} X-Token Naskah nusantaras unique token.
	 * @apiPermission Naskah nusantara Cant be Accessed permission name : api_naskah_nusantara_update
	 *
	 * @apiParam {String} Jenis_bahan Mandatory jenis_bahan of Naskah nusantaras. Input Jenis Bahan Max Length : 20. 
	 * @apiParam {String} Judul Mandatory judul of Naskah nusantaras. Input Judul Max Length : 100. 
	 * @apiParam {String} Pengarang Mandatory pengarang of Naskah nusantaras. Input Pengarang Max Length : 50. 
	 * @apiParam {String} Deskripsi_fisik Mandatory deskripsi_fisik of Naskah nusantaras. Input Deskripsi Fisik Max Length : 255. 
	 * @apiParam {String} Informasi_teknis Mandatory informasi_teknis of Naskah nusantaras. Input Informasi Teknis Max Length : 255. 
	 * @apiParam {String} Subjek Mandatory subjek of Naskah nusantaras. Input Subjek Max Length : 20. 
	 * @apiParam {String} Abstrak Mandatory abstrak of Naskah nusantaras. Input Abstrak Max Length : 150. 
	 * @apiParam {String} Catatan Mandatory catatan of Naskah nusantaras. Input Catatan Max Length : 255. 
	 * @apiParam {String} File_cover Mandatory file_cover of Naskah nusantaras. Input File Cover Max Length : 150. 
	 * @apiParam {Integer} id Mandatory id of Naskah Nusantara.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_naskah_nusantara_update');

		
		$this->form_validation->set_rules('jenis_bahan', 'Jenis Bahan', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('judul', 'Judul', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('pengarang', 'Pengarang', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('deskripsi_fisik', 'Deskripsi Fisik', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('informasi_teknis', 'Informasi Teknis', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('subjek', 'Subjek', 'trim|required|max_length[20]');
		$this->form_validation->set_rules('abstrak', 'Abstrak', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('catatan', 'Catatan', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('file_cover', 'File Cover', 'trim|required|max_length[150]');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'jenis_bahan' => $this->input->post('jenis_bahan'),
				'judul' => $this->input->post('judul'),
				'pengarang' => $this->input->post('pengarang'),
				'deskripsi_fisik' => $this->input->post('deskripsi_fisik'),
				'informasi_teknis' => $this->input->post('informasi_teknis'),
				'subjek' => $this->input->post('subjek'),
				'abstrak' => $this->input->post('abstrak'),
				'catatan' => $this->input->post('catatan'),
				'file_cover' => $this->input->post('file_cover'),
			];
			
			$save_naskah_nusantara = $this->model_api_naskah_nusantara->change($this->post('id'), $save_data);

			if ($save_naskah_nusantara) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> validation_errors()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /naskah_nusantara/delete Delete Naskah nusantara. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteNaskah nusantara
	 * @apiGroup naskah_nusantara
	 * @apiHeader {String} X-Api-Key Naskah nusantaras unique access-key.
	 * @apiHeader {String} X-Token Naskah nusantaras unique token.
	 	 * @apiPermission Naskah nusantara Cant be Accessed permission name : api_naskah_nusantara_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Naskah nusantaras .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_naskah_nusantara_delete');

		$naskah_nusantara = $this->model_api_naskah_nusantara->find($this->post('id'));

		if (!$naskah_nusantara) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Naskah nusantara not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_naskah_nusantara->remove($this->post('id'));

			}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Naskah nusantara deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Naskah nusantara not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

}

/* End of file Naskah nusantara.php */
/* Location: ./application/controllers/api/Naskah nusantara.php */