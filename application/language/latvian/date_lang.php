<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['date_year'] = 'Gads';
$lang['date_years'] = 'Gadi';
$lang['date_month'] = 'Mēnesis';
$lang['date_months'] = 'Mēneši';
$lang['date_week'] = 'Nedēļa';
$lang['date_weeks'] = 'Nedēļas';
$lang['date_day'] = 'Diena';
$lang['date_days'] = 'Dienas';
$lang['date_hour'] = 'Stunda';
$lang['date_hours'] = 'Stundas';
$lang['date_minute'] = 'Minūte';
$lang['date_minutes'] = 'Minūtes';
$lang['date_second'] = 'Sekunde';
$lang['date_seconds'] = 'Sekundes';

$lang['UM12']	= '(UTC -12:00) Beikera/Haulenda Salas';
$lang['UM11']	= '(UTC -11:00) Niue';
$lang['UM10']	= '(UTC -10:00) Havaju-Aleutu standarda Laiks, Kuka Salas, Taiti';
$lang['UM95']	= '(UTC -9:30) Marķīza Salas';
$lang['UM9']	= '(UTC -9:00) Aļaskas standarda Laiks, Gambjē Salas';
$lang['UM8']	= '(UTC -8:00) Klusā Okeāna standarda Laiks (ASV, Kanāda), Klipertona Sala';
$lang['UM7']	= '(UTC -7:00) Kalnu standarda Laiks (ASV, Kanāda)';
$lang['UM6']	= '(UTC -6:00) Centrālais standarda Laiks (ASV, Kanāda)';
$lang['UM5']	= '(UTC -5:00) Austrumu standarda Laiks (ASV, Kanāda), Rietumu Karību standarda Laiks';
$lang['UM45']	= '(UTC -4:30) Venecuēlas standarda Laiks';
$lang['UM4']	= '(UTC -4:00) Atlantijas standarda Laiks, Austrumu Karību standarda Laiks';
$lang['UM35']	= '(UTC -3:30) Ņūfaundlendas standarda Laiks';
$lang['UM3']	= '(UTC -3:00) Argentīna, Brazīlija, Franču Gviāna, Urugvaja';
$lang['UM2']	= '(UTC -2:00) Dienviddžordžija/Dienvidsendviču Salas';
$lang['UM1']	= '(UTC -1:00) Azoru Salas, Zaļā Raga Salas (Kaboverde)';
$lang['UTC']	= '(UTC) Griničas Laiks, Rietumeiropas Laiks';
$lang['UP1']	= '(UTC +1:00) Centrāleiropas Laiks, Rietumāfrikas Laiks';
$lang['UP2']	= '(UTC +2:00) Centrālāfrikas Laiks, Austrumeiropas Laiks, Kaļiņingradas Laiks';
$lang['UP3']	= '(UTC +3:00) Maskavas Laiks, Austrumāfrikas Laiks, Arābijas standarda Laiks';
$lang['UP35']	= '(UTC +3:30) Irānas standarda Laiks';
$lang['UP4']	= '(UTC +4:00) Azerbaidžānas standarda Laiks, Samaras Laiks';
$lang['UP45']	= '(UTC +4:30) Afganistāna';
$lang['UP5']	= '(UTC +5:00) Pakistānas standarda Laiks, Jekaterinburgas Laiks';
$lang['UP55']	= '(UTC +5:30) Indijas standarda Laiks, Šrilankas Laiks';
$lang['UP575']	= '(UTC +5:45) Nepālas Laiks';
$lang['UP6']	= '(UTC +6:00) Bangladešas standarda Laiks, Butānas Laiks, Omskas Laiks';
$lang['UP65']	= '(UTC +6:30) Kokosu Salas, Mjanma (Birma)';
$lang['UP7']	= '(UTC +7:00) Krasnojarskas Laiks, Kambodža, Laosa, Taizeme, Vjetnama';
$lang['UP8']	= '(UTC +8:00) Austrālijas Rietumu standarda Laiks, Pekinas Laiks, Irkutskas Laiks';
$lang['UP875']	= '(UTC +8:45) Austrālijas Centrālo Rietumu standarda Laiks';
$lang['UP9']	= '(UTC +9:00) Japānas standarda Laiks, Korejas standarda Laiks, Jakutskas Laiks';
$lang['UP95']	= '(UTC +9:30) Ausrālijas Centrālais standarda Laiks';
$lang['UP10']	= '(UTC +10:00) Austrālijas Austrumu standarda Laiks, Vladivostokas Laiks';
$lang['UP105']	= '(UTC +10:30) Lorda Hava Sala';
$lang['UP11']	= '(UTC +11:00) Srednekolyimskas Laiks, Zālamana Salas, Vanuatu';
$lang['UP115']	= '(UTC +11:30) Norfolkas Sala';
$lang['UP12']	= '(UTC +12:00) Fidži, Gilberta Salas, Kamčatkas Laiks, Jaunzēlandes standarda Laiks';
$lang['UP1275']	= '(UTC +12:45) Četema Salu standarda Laiks';
$lang['UP13']	= '(UTC +13:00) Samoas Laika Zona, Fēniksa Salu Laiks, Tonga';
$lang['UP14']	= '(UTC +14:00) Lainas Salas';
