<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['date_year']      = 'İl';
$lang['date_years']     = 'İllər';
$lang['date_month']     = 'Ay';
$lang['date_months']    = 'Aylar';
$lang['date_week']      = 'Həftə';
$lang['date_weeks']     = 'Həftələr';
$lang['date_day']       = 'Gün';
$lang['date_days']      = 'Günlər';
$lang['date_hour']      = 'Saat';
$lang['date_hours']     = 'Saatlar';
$lang['date_minute']    = 'Dəqiqə';
$lang['date_minutes']   = 'Dəqiqələr';
$lang['date_second']    = 'Saniyə';
$lang['date_seconds']   = 'Saniyələr';

$lang['UM12']   = '(UTC -12:00) Baker/Howland Adası';
$lang['UM11']   = '(UTC -11:00) Niue';
$lang['UM10']   = '(UTC -10:00) Hawaii-Aleutian standard Saatı, Cook Adaları, Tahiti';
$lang['UM95']   = '(UTC -9:30) Marquesas Adaları';
$lang['UM9']    = '(UTC -9:00) Alaska standard Saatı, Gambier Adaları';
$lang['UM8']    = '(UTC -8:00) Pacific standard Saatı, Clipperton Adası';
$lang['UM7']    = '(UTC -7:00) Mountain standard Saatı';
$lang['UM6']    = '(UTC -6:00) Mərkəzi standard Saatı';
$lang['UM5']    = '(UTC -5:00) Şərq standard Saatı, Batı Caribbean standard Saatı';
$lang['UM45']   = '(UTC -4:30) Venezuela standard Saatı';
$lang['UM4']    = '(UTC -4:00) Atlantic standard Saatı, Şərq Caribbean standard Saatı';
$lang['UM35']   = '(UTC -3:30) Newfoundland standard Saatı';
$lang['UM3']    = '(UTC -3:00) Argentina, Brezilya, Fransız Guanası, Uruguay';
$lang['UM2']    = '(UTC -2:00) Güney Georgia/Güney Sandwich Adaları';
$lang['UM1']    = '(UTC -1:00) Azores, Cape Verde Adaları';
$lang['UTC']    = '(UTC) Greenwich Ortalama Saatı, Batı Avrupa Saatı';
$lang['UP1']    = '(UTC +1:00) Orta Avropa Saatı, Qərbi Afrika Saatı';
$lang['UP2']    = '(UTC +2:00) Orta Afrika Saatı, Şərq Avropa Saatı, Kaliningrad Saatı';
$lang['UP3']    = '(UTC +3:00) Moskva Saatı, Şərq Afrika Saatı, Ərəbistan standard Saatı';
$lang['UP35']   = '(UTC +3:30) İran standard Saatı';
$lang['UP4']    = '(UTC +4:00) Azərbaycan standard Saatı, Samara Saatı';
$lang['UP45']   = '(UTC +4:30) Əfqanıstan';
$lang['UP5']    = '(UTC +5:00) Pakistan standard Saatı, Yekaterinburg Saatı';
$lang['UP55']   = '(UTC +5:30) Hindistan standard Saatı, Sri Lanka Saatı';
$lang['UP575']  = '(UTC +5:45) Nepal Saatı';
$lang['UP6']    = '(UTC +6:00) Banqladeş standard Saatı, Bhutan Saatı, Omsk Saatı';
$lang['UP65']   = '(UTC +6:30) Cocos Adaları, Myanmar';
$lang['UP7']    = '(UTC +7:00) Krasnoyarsk Saatı, Cambodia, Laos, Thailand, Vietnam';
$lang['UP8']    = '(UTC +8:00) Avstraliya Qərb standard Saatı, Beijing Saatı, Irkutsk Saatı';
$lang['UP875']  = '(UTC +8:45) Avstraliya Orta Qərb standard Saatı';
$lang['UP9']    = '(UTC +9:00) Yaponiya standard Saatı, Kore standard Saatı, Yakutsk Saatı';
$lang['UP95']   = '(UTC +9:30) Avstraliya Mərkəzi standard Saatı';
$lang['UP10']   = '(UTC +10:00) Avstraliya Şərq standard Saatı, Vladivostok Saatı';
$lang['UP105']  = '(UTC +10:30) Lord Howe Adası';
$lang['UP11']   = '(UTC +11:00) Srednekolymsk Saatı, Solomon Adaları, Vanuatu';
$lang['UP115']  = '(UTC +11:30) Norfolk Adası';
$lang['UP12']   = '(UTC +12:00) Fiji, Gilbert Adaları, Kamchatka Saatı, Yeni Zelandiya standard Saatı';
$lang['UP1275'] = '(UTC +12:45) Chatham Adaları standard Saatı';
$lang['UP13']   = '(UTC +13:00) Samoa Saatı Bölgəsi, Phoenix Adaları Saatı, Tonga';
$lang['UP14']   = '(UTC +14:00) Line Adaları';
