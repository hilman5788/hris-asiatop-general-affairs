<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pengajuan_training extends MY_Model {

	private $primary_key 	= 'id';
	private $table_name 	= 'pengajuan_training';
	private $field_search 	= ['nama_pemohon', 'jenis_training', 'pengadaan_training', 'nama_training', 'tujuan_training', 'lokasi_training', 'jumlah_investasi', 'jenis_pembayaran'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "pengajuan_training.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "pengajuan_training.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "pengajuan_training.".$field . " LIKE '%" . $q . "%' )";
        }

		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "pengajuan_training.".$field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . "pengajuan_training.".$field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . "pengajuan_training.".$field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }
		
		$this->join_avaiable()->filter_avaiable();
        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by('pengajuan_training.'.$this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

    public function join_avaiable() {
        $this->db->join('employee', 'employee.employee_id = pengajuan_training.nama_pemohon', 'LEFT');
        $this->db->join('employee employee1', 'employee1.employee_id = pengajuan_training.calon_peserta', 'LEFT');
        
        return $this;
    }

    public function filter_avaiable() {
        
        return $this;
    }

}

/* End of file Model_pengajuan_training.php */
/* Location: ./application/models/Model_pengajuan_training.php */