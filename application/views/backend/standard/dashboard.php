<style type="text/css">
   .widget-user-header {
      padding-left: 20px !important;
   }
</style>

<link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.css">

<section class="content-header">
    <h1>
        <?= cclang('dashboard') ?>
        <small>
            
        <?= cclang('control_panel') ?>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard">
                </i>
                <?= cclang('home') ?>
            </a>
        </li>
        <li class="active">
            <?= cclang('dashboard') ?>
        </li>
    </ol>
</section>

<section class="content">
    <div class="row">
      <?php cicool()->eventListen('dashboard_content_top'); ?>

       <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/crud')">
                <span class="info-box-icon bg-green">
                    <i class="fa fa-users">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        <b>10 </b> <br>
                        <small>Karyawan Baru</small>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/rest')">
                <span class="info-box-icon bg-blue">
                    <i class="fa fa-users">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        <b>200</b> <br>
                        <small>Karyawan Aktif</small>
                    </span>
                </div>
            </div>
        </div>

         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/page')">
                <span class="info-box-icon bg-red">
                    <i class="fa fa-users">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        <b>30</b> <br>
                        <small>Karyawan Resign</small>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box button" onclick="goUrl('administrator/form')">
                <span class="info-box-icon bg-yellow">
                    <i class="fa fa-users">
                    </i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">
                        <b>250</b> <br>
                        <small>Jumlah Karyawan</small>
                    </span>
                </div>
            </div>
        </div>

    </div>

     <div class="row">
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik 1</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="sales-chart" style="height: 255px; position: relative;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          

        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik 2</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="line-chart" style="height: 255px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript">
          google.charts.load('current', {packages:["orgchart"]});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() {
              var data = new google.visualization.DataTable();
              data.addColumn('string', 'Name');
              data.addColumn('string', 'Manager');
              data.addColumn('string', 'ToolTip');

              // For each orgchart box, provide the name, manager, and tooltip to show.
              data.addRows([
                ['Corporate', '', 'Presiden Direktur'],

                ['Directorate SCM', 'Corporate', 'Directorate'],
                ['Directorate Finance & Acc', 'Corporate', 'Directorate'],
                ['Directorate HC-GA', 'Corporate', 'Directorate'],

                ['Div Head SCM', 'Directorate SCM', 'Division'],
                ['Div Head Finance & Acc', 'Directorate Finance & Acc', 'Division'],
                ['Div Head HC-GA', 'Directorate HC-GA', 'Division'],

                ['Dept Head Warehouse', 'Div Head SCM', 'Department'],
                ['Dept Head Finance', 'Div Head Finance & Acc', 'Department'],
                ['Dept Head Accounting', 'Div Head Finance & Acc', 'Department'],
                ['Dept Head HCM', 'Div Head HC-GA', 'Department'],
                ['Dept Head GA', 'Div Head HC-GA', 'Department'],

                ['Sect Head RMW', 'Dept Head Warehouse', 'Section'],
                ['Sect Head PPIC', 'Dept Head Warehouse', 'Section'],
                ['Chasier', 'Dept Head Finance', 'Section'],
                ['Sect Head Tax', 'Dept Head Accounting', 'Section'],
                ['Sect Head Recruitment', 'Dept Head HCM', 'Section'],
                ['Sect Head GA', 'Dept Head GA', 'Section'],

                ['Supervisor of RMW', 'Sect Head RMW', 'Supervisor'],
                ['Supervisor of PPIC', 'Sect Head PPIC', 'Supervisor'],
                ['Supervisor of Chasier', 'Chasier', 'Supervisor'],
                ['Supervisor of Tax', 'Sect Head Tax', 'Supervisor'],
                ['Supervisor of Recruitment', 'Sect Head Recruitment', 'Supervisor'],
                ['Supervisor of GA', 'Sect Head GA', 'Supervisor'],

                ['Coordinator of RMW', 'Supervisor of RMW', 'Officer'],
                ['Staff RMW', 'Supervisor of RMW', 'Officer'],
                ['Coordinator of PPIC', 'Supervisor of PPIC', 'Officer'],
                ['Staff PPIC', 'Supervisor of PPIC', 'Officer'],
                ['Coordinator of Chasier', 'Supervisor of Chasier', 'Officer'],
                ['Staff Chasier', 'Supervisor of Chasier', 'Officer'],
                ['Coordinator of Tax', 'Supervisor of Tax', 'Officer'],
                ['Staff Tax', 'Supervisor of Tax', 'Officer'],
                ['Coordinator of Recruitment', 'Supervisor of Recruitment', 'Officer'],
                ['Staff Recruitment', 'Supervisor of Recruitment', 'Officer'],
                ['Coordinator of GA', 'Supervisor of GA', 'Officer'],
                ['Staff GA', 'Supervisor of GA', 'Officer'],

                  // ['Corporate', '', ''],
                  // ['Directorate', 'Corporate', ''],
                  // ['Division', 'Directorate', ''],
                  // ['Departement', 'Division', ''],

                  // ['Section', 'Departement', ''],
                  // ['Supervisor', 'Section', ''],
                  // ['Officer', 'Supervisor', ''],
              ]);

              // Create the chart.
              var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
              // Draw the chart, setting the allowHtml option to true for the tooltips.
              chart.draw(data, {allowHtml:true});
          }
      </script>
      <div class="row">
          <div class="box box-info">
            <div class="box-body">
              <div id="chart_div"></div>
            </div>
          </div>
      </div>

<!--       <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Peta Keberadaan Naskah Daerah</h3>
            </div>
            <div class="box-body chart-responsive">
              <img src="<?= base_url(); ?>uploads/maps.png" class="img-responsive">

            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div> -->
  
      <!-- /.row -->
      <?php cicool()->eventListen('dashboard_content_bottom'); ?>

</section>
<!-- /.content -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= BASE_ASSET; ?>admin-lte/plugins/morris/morris.min.js"></script>
<script>
  $(function () {
    "use strict";


    // LINE CHART
    var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: [
        {y: '2016 Q1', item1: 2666},
        {y: '2016 Q2', item1: 2778},
        {y: '2016 Q3', item1: 4912},
        {y: '2016 Q4', item1: 3767},
        {y: '2017 Q1', item1: 6810},
        {y: '2017 Q2', item1: 5670},
        {y: '2017 Q3', item1: 4820},
        {y: '2017 Q4', item1: 15073},
        {y: '2018 Q1', item1: 10687},
        {y: '2018 Q2', item1: 8432}
      ],
      xkey: 'y',
      ykeys: ['item1'],
      labels: ['Item 1'],
      lineColors: ['#3c8dbc'],
      hideHover: 'auto'
    });

    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954", "#00a65a"],
      data: [
        {label: "Category 1", value: 20},
        {label: "Category 2", value: 50},
        {label: "Category 3", value: 30}
      ],
      hideHover: 'auto'
    });


  });
</script>

