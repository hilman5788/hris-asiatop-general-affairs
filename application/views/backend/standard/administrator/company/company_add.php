
<!-- Fine Uploader Gallery CSS file
    ====================================================================== -->
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Company        <small><?= cclang('new', ['Company']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/company'); ?>">Company</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Company</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Company']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_company', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_company', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="company_name" class="col-sm-3 control-label">Company Name 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Company Name" value="<?= set_value('company_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_logo" class="col-sm-3 control-label">Logo 
                            </label>
                            <div class="col-sm-8">
                                <div id="company_company_logo_galery"></div>
                                <input class="data_file" name="company_company_logo_uuid" id="company_company_logo_uuid" type="hidden" value="<?= set_value('company_company_logo_uuid'); ?>">
                                <input class="data_file" name="company_company_logo_name" id="company_company_logo_name" type="hidden" value="<?= set_value('company_company_logo_name'); ?>">
                                <small class="info help-block">
                                <b>Extension file must</b> JPG.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_start_date" class="col-sm-3 control-label">Start Date 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="company_start_date"  placeholder="Start Date" id="company_start_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_vision" class="col-sm-3 control-label">Vision 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="company_vision" name="company_vision" rows="5" class="textarea"><?= set_value('company_vision'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_mission" class="col-sm-3 control-label">Mission 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="company_mission" name="company_mission" rows="5" class="textarea"><?= set_value('company_mission'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_contact_email" class="col-sm-3 control-label">Email 
                            </label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" name="company_contact_email" id="company_contact_email" placeholder="Email" value="<?= set_value('company_contact_email'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_contact_phone" class="col-sm-3 control-label">Phone 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_contact_phone" id="company_contact_phone" placeholder="Phone" value="<?= set_value('company_contact_phone'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_contact_fax" class="col-sm-3 control-label">Fax 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_contact_fax" id="company_contact_fax" placeholder="Fax" value="<?= set_value('company_contact_fax'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_type" class="col-sm-3 control-label">Company Type 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_type" id="company_type" placeholder="Company Type" value="<?= set_value('company_type'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_business_type" class="col-sm-3 control-label">Business Type 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_business_type" id="company_business_type" placeholder="Business Type" value="<?= set_value('company_business_type'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_address_location" class="col-sm-3 control-label">Address 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="company_address_location" name="company_address_location" rows="5" class="textarea"><?= set_value('company_address_location'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_address_city" class="col-sm-3 control-label">City 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_address_city" id="company_address_city" placeholder="City" value="<?= set_value('company_address_city'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_address_zip_code" class="col-sm-3 control-label">Zip Code 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_address_zip_code" id="company_address_zip_code" placeholder="Zip Code" value="<?= set_value('company_address_zip_code'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_address_country" class="col-sm-3 control-label">Country 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_address_country" id="company_address_country" placeholder="Country" value="<?= set_value('company_address_country'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_tax_npkp" class="col-sm-3 control-label">NPPKP 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_tax_npkp" id="company_tax_npkp" placeholder="NPPKP" value="<?= set_value('company_tax_npkp'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_tax_npwp" class="col-sm-3 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_tax_npwp" id="company_tax_npwp" placeholder="NPWP" value="<?= set_value('company_tax_npwp'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_tax_location" class="col-sm-3 control-label">Tax Location 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="company_tax_location" name="company_tax_location" rows="5" class="textarea"><?= set_value('company_tax_location'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_bank_name" class="col-sm-3 control-label">Bank Name 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_bank_name" id="company_bank_name" placeholder="Bank Name" value="<?= set_value('company_bank_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_bank_account_no" class="col-sm-3 control-label">Bank Account No. 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="company_bank_account_no" id="company_bank_account_no" placeholder="Bank Account No." value="<?= set_value('company_bank_account_no'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_bank_account_name" class="col-sm-3 control-label">Bank Account Name 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="company_bank_account_name" id="company_bank_account_name" placeholder="Bank Account Name" value="<?= set_value('company_bank_account_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="company_is_holding" class="col-sm-3 control-label">Is Holding 
                            </label>
                            <div class="col-sm-6">
                                <div class="col-md-6 padding-left-0">
                                    <label>
                                        <input type="radio" class="flat-red" name="company_is_holding" id="company_is_holding"  value="1">
                                        <?= cclang('yes'); ?>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label>
                                        <input type="radio" class="flat-red" name="company_is_holding" id="company_is_holding"  value="0">
                                        <?= cclang('no'); ?>
                                    </label>
                                </div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/company';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_company = $('#form_company');
        var data_post = form_company.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/company/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id_company_logo = $('#company_company_logo_galery').find('li').attr('qq-file-id');
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            if (typeof id_company_logo !== 'undefined') {
                    $('#company_company_logo_galery').fineUploader('deleteFile', id_company_logo);
                }
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
              var params = {};
       params[csrf] = token;

       $('#company_company_logo_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/company/upload_company_logo_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/company/delete_company_logo_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          multiple : false,
          validation: {
              allowedExtensions: ["jpg"],
              sizeLimit : 0,
                        },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#company_company_logo_galery').fineUploader('getUuid', id);
                   $('#company_company_logo_uuid').val(uuid);
                   $('#company_company_logo_name').val(xhr.uploadName);
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onSubmit : function(id, name) {
                  var uuid = $('#company_company_logo_uuid').val();
                  $.get(BASE_URL + '/administrator/company/delete_company_logo_file/' + uuid);
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#company_company_logo_uuid').val('');
                  $('#company_company_logo_name').val('');
                }
              }
          }
      }); /*end company_logo galery*/
              
 
       
    
    
    }); /*end doc ready*/
</script>