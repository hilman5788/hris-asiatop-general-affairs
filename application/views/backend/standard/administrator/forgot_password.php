<?php require __DIR__ . '/header.php'; ?>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b><?= cclang('login'); ?></b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><?= cclang('send_me_link_to_reset_password'); ?></p>
    <?php if(isset($error) AND !empty($error)): ?>
         <div class="callout callout-error"  style="color:#C82626">
              <h4><?= cclang('error'); ?>!</h4>
              <p><?= $error; ?></p>
            </div>
    <?php endif; ?>
    <?php
    $message = $this->session->flashdata('f_message'); 
    $type = $this->session->flashdata('f_type'); 
    if ($message):
    ?>
   <div class="callout callout-<?= $type; ?>"  style="color:#C82626">
        <p><?= $message; ?></p>
      </div>
    <?php endif; ?>
     <?= form_open('', [
        'name'    => 'form_forgot_password', 
        'id'      => 'form_forgot_password', 
        'method'  => 'POST'
      ]); ?>
      <div class="form-group has-feedback <?= form_error('email') ? 'has-error' :''; ?>">
        <label><?= cclang('email') ?> <span class="required">*</span> </label>
        <input type="email" class="form-control" placeholder="Email" name="email" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <?php $cap = get_captcha(); ?>
      <div class="form-group <?= form_error('email') ? 'has-error' :''; ?>">
      <label><?= cclang('human_challenge'); ?> <span class="required">*</span> </label>
      <div class="captcha-box"  data-captcha-time="<?= $cap['time']; ?>">
          <input type="text" name="captcha" placeholder="">
          <a class="btn btn-flat  refresh-captcha  "><i class="fa fa-refresh text-danger"></i></a>
          <span  class="box-image"><?= $cap['image']; ?></span>
      </div>
      <small class="info help-block">
      </small>
      </div>
      <div class="row">
        <div class="col-xs-8">
         
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?= cclang('reset'); ?></button>
        </div>
        <!-- /.col -->
      </div>
    <?= form_close(); ?>

    <!-- /.social-auth-links -->

    <a href="<?= site_url('administrator/register'); ?>" class="text-center"><?= cclang('register_a_new_membership'); ?></a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function() {
     var BASE_URL = "<?= base_url(); ?>";

     $.fn.printMessage = function(opsi) {
         var opsi = $.extend({
             type: 'success',
             message: 'Success',
             timeout: 500000
         }, opsi);

         $(this).hide();
         $(this).html(' <div class="col-md-12 message-alert" ><div class="callout callout-' + opsi.type + '"><h4>' + opsi.type + '!</h4>' + opsi.message + '</div></div>');
         $(this).slideDown('slow');
         // Run the effect
         setTimeout(function() {
             $('.message-alert').slideUp('slow');
         }, opsi.timeout);
     };
     $('.refresh-captcha').on('click', function() {
         var capparent = $(this);

         $.ajax({
                 url: BASE_URL + '/captcha/reload/' + capparent.parent('.captcha-box').attr('data-captcha-time'),
                 dataType: 'JSON',
             })
             .done(function(res) {
                 capparent.parent('.captcha-box').find('.box-image').html(res.image);
                 capparent.parent('.captcha-box').attr('data-captcha-time', res.captcha.time);
             })
             .fail(function() {
                 $('.message').printMessage({
                     message: 'Error getting captcha',
                     type: 'warning'
                 });
             })
             .always(function() {});
     });


     $('input').iCheck({
         checkboxClass: 'icheckbox_square-blue',
         radioClass: 'iradio_square-blue',
         increaseArea: '20%' // optional
     });
 });
</script>
</body>
</html>
