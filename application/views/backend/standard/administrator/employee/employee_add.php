
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Employee        <small><?= cclang('new', ['Employee']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/employee'); ?>">Employee</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Employee</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Employee']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_employee', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_employee', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="employee_nik" class="col-sm-3 control-label">NIK 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_nik" id="employee_nik" placeholder="NIK" value="<?= set_value('employee_nik'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_name" class="col-sm-3 control-label">Employee Name 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_name" id="employee_name" placeholder="Employee Name" value="<?= set_value('employee_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_hire_date" class="col-sm-3 control-label">Hire Date 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="employee_hire_date"  placeholder="Hire Date" id="employee_hire_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_seniority_date" class="col-sm-3 control-label">Seniority Date 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="employee_seniority_date"  placeholder="Seniority Date" id="employee_seniority_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_grade_id" class="col-sm-3 control-label">Grade 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_grade_id" id="employee_grade_id" placeholder="Grade" value="<?= set_value('employee_grade_id'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_department_id" class="col-sm-3 control-label">Department Name 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="employee_department_id" id="employee_department_id" data-placeholder="Department Name" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('department') as $row): ?>
                                    <option value="<?= $row->department_id ?>"><?= $row->department_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="employee_position_id" class="col-sm-3 control-label">Position Name 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="employee_position_id" id="employee_position_id" data-placeholder="Position Name" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('position') as $row): ?>
                                    <option value="<?= $row->position_id ?>"><?= $row->position_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="employee_work_location_id" class="col-sm-3 control-label">Work Location 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="employee_work_location_id" id="employee_work_location_id" data-placeholder="Work Location" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('work_location') as $row): ?>
                                    <option value="<?= $row->work_location_id ?>"><?= $row->work_location_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="employee_sex" class="col-sm-3 control-label">Sex 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_sex" id="employee_sex" data-placeholder="Sex" >
                                    <option value=""></option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_birth_place" class="col-sm-3 control-label">Birth Place 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_birth_place" id="employee_birth_place" placeholder="Birth Place" value="<?= set_value('employee_birth_place'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_birth_date" class="col-sm-3 control-label">Birth Date 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="employee_birth_date"  placeholder="Birth Date" id="employee_birth_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_marital_status" class="col-sm-3 control-label">Marital Status 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_marital_status" id="employee_marital_status" data-placeholder="Marital Status" >
                                    <option value=""></option>
                                    <option value="S-0">S-0</option>
                                    <option value="M-0">M-0</option>
                                    <option value="M-1">M-1</option>
                                    <option value="M-2">M-2</option>
                                    <option value="M-3">M-3</option>
                                    <option value="M-4">M-4</option>
                                    <option value="D-0">D-0</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_religion" class="col-sm-3 control-label">Religion 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_religion" id="employee_religion" data-placeholder="Religion" >
                                    <option value=""></option>
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen">Kristen</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Budha">Budha</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_social_id" class="col-sm-3 control-label">KTP No. 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_social_id" id="employee_social_id" placeholder="KTP No." value="<?= set_value('employee_social_id'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_last_education" class="col-sm-3 control-label">Last Education 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_last_education" id="employee_last_education" data-placeholder="Last Education" >
                                    <option value=""></option>
                                    <option value="SMA/STM/SMK">SMA/STM/SMK</option>
                                    <option value="Diploma 1">Diploma 1</option>
                                    <option value="Diploma 2">Diploma 2</option>
                                    <option value="Diploma 3">Diploma 3</option>
                                    <option value="Strata 1">Strata 1</option>
                                    <option value="Strata 2">Strata 2</option>
                                    <option value="Strata 3">Strata 3</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_npwp" class="col-sm-3 control-label">NPWP 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_npwp" id="employee_npwp" placeholder="NPWP" value="<?= set_value('employee_npwp'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_blood_type" class="col-sm-3 control-label">Blood Type 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_blood_type" id="employee_blood_type" data-placeholder="Blood Type" >
                                    <option value=""></option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="AB">AB</option>
                                    <option value="O">O</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_pay_group" class="col-sm-3 control-label">Pay Group 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_pay_group" id="employee_pay_group" data-placeholder="Pay Group" >
                                    <option value=""></option>
                                    <option value="Salary Code 1">Salary Code 1</option>
                                    <option value="Salary Code 2">Salary Code 2</option>
                                    <option value="Salary Code 3">Salary Code 3</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_cost_group" class="col-sm-3 control-label">Cost Group 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_cost_group" id="employee_cost_group" placeholder="Cost Group" value="<?= set_value('employee_cost_group'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_tax_category" class="col-sm-3 control-label">Tax Category 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_tax_category" id="employee_tax_category" data-placeholder="Tax Category" >
                                    <option value=""></option>
                                    <option value="Peg. Tetap">Peg. Tetap</option>
                                    <option value="Peg. Kontrak">Peg. Kontrak</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_status" class="col-sm-3 control-label">Employee Status 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_status" id="employee_status" data-placeholder="Employee Status" >
                                    <option value=""></option>
                                    <option value="Permanent">Permanent</option>
                                    <option value="Contract">Contract</option>
                                    <option value="Probation">Probation</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_termination_date" class="col-sm-3 control-label">Termination Date 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="employee_termination_date"  placeholder="Termination Date" id="employee_termination_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_contact_phone" class="col-sm-3 control-label">Phone No.  
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_contact_phone" id="employee_contact_phone" placeholder="Phone No. " value="<?= set_value('employee_contact_phone'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_contact_fax" class="col-sm-3 control-label">Fax No. 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_contact_fax" id="employee_contact_fax" placeholder="Fax No." value="<?= set_value('employee_contact_fax'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_1" class="col-sm-3 control-label">Address 1 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="employee_address_1" name="employee_address_1" rows="5" class="textarea"><?= set_value('employee_address_1'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_2" class="col-sm-3 control-label">Address 2 
                            </label>
                            <div class="col-sm-8">
                                <textarea id="employee_address_2" name="employee_address_2" rows="5" class="textarea"><?= set_value('employee_address_2'); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_rt" class="col-sm-3 control-label">RT 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_address_rt" id="employee_address_rt" placeholder="RT" value="<?= set_value('employee_address_rt'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_rw" class="col-sm-3 control-label">RW 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_address_rw" id="employee_address_rw" placeholder="RW" value="<?= set_value('employee_address_rw'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_city" class="col-sm-3 control-label">City 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_address_city" id="employee_address_city" placeholder="City" value="<?= set_value('employee_address_city'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_zip_code" class="col-sm-3 control-label">Zip Code 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_address_zip_code" id="employee_address_zip_code" placeholder="Zip Code" value="<?= set_value('employee_address_zip_code'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_address_state" class="col-sm-3 control-label">State 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_address_state" id="employee_address_state" placeholder="State" value="<?= set_value('employee_address_state'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_bank_account_name" class="col-sm-3 control-label">Bank Account Name 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_bank_account_name" id="employee_bank_account_name" placeholder="Bank Account Name" value="<?= set_value('employee_bank_account_name'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_bank_account_no" class="col-sm-3 control-label">Bank Account No. 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="employee_bank_account_no" id="employee_bank_account_no" placeholder="Bank Account No." value="<?= set_value('employee_bank_account_no'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="employee_bank_name" class="col-sm-3 control-label">Bank Name 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="employee_bank_name" id="employee_bank_name" data-placeholder="Bank Name" >
                                    <option value=""></option>
                                    <option value="BCA">BCA</option>
                                    <option value="CIMB NIAGA I">CIMB NIAGA I</option>
                                    <option value="CIMB NIAGA II">CIMB NIAGA II</option>
                                    <option value="DANAMON">DANAMON</option>
                                    <option value="MANDIRI">MANDIRI</option>
                                    <option value="PERMATA">PERMATA</option>
                                    <option value="BNI">BNI</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/employee';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_employee = $('#form_employee');
        var data_post = form_employee.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/employee/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
 
       
    
    
    }); /*end doc ready*/
</script>