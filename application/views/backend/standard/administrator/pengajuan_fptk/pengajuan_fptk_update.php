
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pengajuan Fptk        <small>Edit Pengajuan Fptk</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/pengajuan_fptk'); ?>">Pengajuan Fptk</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pengajuan Fptk</h3>
                            <h5 class="widget-user-desc">Edit Pengajuan Fptk</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/pengajuan_fptk/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_pengajuan_fptk', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_pengajuan_fptk', 
                            'method'  => 'POST'
                            ]); ?>
                         
                         
                                                <div class="form-group ">
                            <label for="jabatan" class="col-sm-3 control-label">Jabatan 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?= set_value('jabatan', $pengajuan_fptk->jabatan); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jumlah" class="col-sm-3 control-label">Jumlah 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah" value="<?= set_value('jumlah', $pengajuan_fptk->jumlah); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="status_dibutuhkan" class="col-sm-3 control-label">Status Dibutuhkan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="status_dibutuhkan" id="status_dibutuhkan" data-placeholder="Status Dibutuhkan" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->status_dibutuhkan == "Urgent" ? 'selected' :''; ?> value="Urgent">Urgent</option>
                                    <option <?= $pengajuan_fptk->status_dibutuhkan == "Normal" ? 'selected' :''; ?> value="Normal">Normal</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="atasan_langsung" class="col-sm-3 control-label">Atasan Langsung 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="atasan_langsung" id="atasan_langsung" data-placeholder="Atasan Langsung" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('aauth_users') as $row): ?>
                                    <option <?=  $row->id ==  $pengajuan_fptk->atasan_langsung ? 'selected' : ''; ?> value="<?= $row->id ?>"><?= $row->full_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="alasan_permohonan" class="col-sm-3 control-label">Alasan Permohonan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="alasan_permohonan" id="alasan_permohonan" data-placeholder="Alasan Permohonan" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->alasan_permohonan == "Penambahan MPP" ? 'selected' :''; ?> value="Penambahan MPP">Penambahan MPP</option>
                                    <option <?= $pengajuan_fptk->alasan_permohonan == "Penggantian Karyawan Resign" ? 'selected' :''; ?> value="Penggantian Karyawan Resign">Penggantian Karyawan Resign</option>
                                    <option <?= $pengajuan_fptk->alasan_permohonan == "Penggantian Karyawan Resign" ? 'selected' :''; ?> value="Penggantian Karyawan Resign">Penggantian Karyawan Resign</option>
                                    <option <?= $pengajuan_fptk->alasan_permohonan == "Pengisian Jabatan Baru" ? 'selected' :''; ?> value="Pengisian Jabatan Baru">Pengisian Jabatan Baru</option>
                                    <option <?= $pengajuan_fptk->alasan_permohonan == "Penggantian Akibat Rotasi/Mutasi" ? 'selected' :''; ?> value="Penggantian Akibat Rotasi/Mutasi">Penggantian Akibat Rotasi/Mutasi</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="penempatan" class="col-sm-3 control-label">Penempatan 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="penempatan" id="penempatan" placeholder="Penempatan" value="<?= set_value('penempatan', $pengajuan_fptk->penempatan); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="jenis_kelamin" class="col-sm-3 control-label">Jenis Kelamin 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="jenis_kelamin" id="jenis_kelamin" data-placeholder="Jenis Kelamin" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->jenis_kelamin == "L" ? 'selected' :''; ?> value="L">Laki-laki</option>
                                    <option <?= $pengajuan_fptk->jenis_kelamin == "P" ? 'selected' :''; ?> value="P">Perempuan</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="usia" class="col-sm-3 control-label">Usia 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="usia" id="usia" placeholder="Usia" value="<?= set_value('usia', $pengajuan_fptk->usia); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="status_karyawan" class="col-sm-3 control-label">Status Karyawan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="status_karyawan" id="status_karyawan" data-placeholder="Status Karyawan" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->status_karyawan == "PKWT" ? 'selected' :''; ?> value="PKWT">PKWT</option>
                                    <option <?= $pengajuan_fptk->status_karyawan == "PWTT" ? 'selected' :''; ?> value="PWTT">PWTT</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="marital_status" class="col-sm-3 control-label">Marital Status 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="marital_status" id="marital_status" data-placeholder="Marital Status" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->marital_status == "Single" ? 'selected' :''; ?> value="Single">Single</option>
                                    <option <?= $pengajuan_fptk->marital_status == "Married" ? 'selected' :''; ?> value="Married">Married</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pendidikan" class="col-sm-3 control-label">Pendidikan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="pendidikan" id="pendidikan" data-placeholder="Pendidikan" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->pendidikan == "SMA / STM / SMK" ? 'selected' :''; ?> value="SMA / STM / SMK">SMA / STM / SMK</option>
                                    <option <?= $pengajuan_fptk->pendidikan == "Diploma 3" ? 'selected' :''; ?> value="Diploma 3">Diploma 3</option>
                                    <option <?= $pengajuan_fptk->pendidikan == "S1" ? 'selected' :''; ?> value="S1">S1</option>
                                    <option <?= $pengajuan_fptk->pendidikan == "S2" ? 'selected' :''; ?> value="S2">S2</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pengalaman" class="col-sm-3 control-label">Pengalaman 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="pengalaman" id="pengalaman" data-placeholder="Pengalaman" >
                                    <option value=""></option>
                                    <option <?= $pengajuan_fptk->pengalaman == "1 - 2 tahun" ? 'selected' :''; ?> value="1 - 2 tahun">1 - 2 tahun</option>
                                    <option <?= $pengajuan_fptk->pengalaman == "2 - 3 tahun" ? 'selected' :''; ?> value="2 - 3 tahun">2 - 3 tahun</option>
                                    <option <?= $pengajuan_fptk->pengalaman == "> 3 tahun" ? 'selected' :''; ?> value="> 3 tahun">> 3 tahun</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="keterampilan" class="col-sm-3 control-label">Keterampilan 
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="keterampilan[]" id="keterampilan" data-placeholder="Keterampilan" multiple >
                                    <option value=""></option>
                                    <option <?= in_array('Administratif / Filling', explode(',', $pengajuan_fptk->keterampilan)) ? 'selected' : ''; ?>  value="Administratif / Filling">Administratif / Filling</option>
                                    <option <?= in_array('Pelayanan Pelanggan / Collection', explode(',', $pengajuan_fptk->keterampilan)) ? 'selected' : ''; ?>  value="Pelayanan Pelanggan / Collection">Pelayanan Pelanggan / Collection</option>
                                    <option <?= in_array('Teknisi Elektronik', explode(',', $pengajuan_fptk->keterampilan)) ? 'selected' : ''; ?>  value="Teknisi Elektronik">Teknisi Elektronik</option>
                                    <option <?= in_array('Komputer Ms Word / Excel', explode(',', $pengajuan_fptk->keterampilan)) ? 'selected' : ''; ?>  value="Komputer Ms Word / Excel">Komputer Ms Word / Excel</option>
                                    <option <?= in_array('Pemrograman', explode(',', $pengajuan_fptk->keterampilan)) ? 'selected' : ''; ?>  value="Pemrograman">Pemrograman</option>
                                    <option <?= in_array('Lainnya', explode(',', $pengajuan_fptk->keterampilan)) ? 'selected' : ''; ?>  value="Lainnya">Lainnya</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/pengajuan_fptk';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_pengajuan_fptk = $('#form_pengajuan_fptk');
        var data_post = form_pengajuan_fptk.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_pengajuan_fptk.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id = $('#pengajuan_fptk_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
           
    
    }); /*end doc ready*/
</script>