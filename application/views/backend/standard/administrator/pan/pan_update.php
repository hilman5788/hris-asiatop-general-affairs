
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        PAN        <small>Edit PAN</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/pan'); ?>">PAN</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">PAN</h3>
                            <h5 class="widget-user-desc">Edit PAN</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/pan/edit_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_pan', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_pan', 
                            'method'  => 'POST'
                            ]); ?>
                         
                                                <div class="form-group ">
                            <label for="nama_karyawan" class="col-sm-3 control-label">Nama Karyawan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="nama_karyawan" id="nama_karyawan" data-placeholder="Nama Karyawan" >
                                    <option value=""></option>
                                    <?php foreach (db_get_all_data('employee') as $row): ?>
                                    <option <?=  $row->employee_id ==  $pan->nama_karyawan ? 'selected' : ''; ?> value="<?= $row->employee_id ?>"><?= $row->employee_name; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                                                 
                                                <div class="form-group ">
                            <label for="jenis_usulan" class="col-sm-3 control-label">Jenis Usulan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select" name="jenis_usulan" id="jenis_usulan" data-placeholder="Jenis Usulan" >
                                    <option value=""></option>
                                    <option <?= $pan->jenis_usulan == "Mutasi" ? 'selected' :''; ?> value="Mutasi">Mutasi</option>
                                    <option <?= $pan->jenis_usulan == "Promosi" ? 'selected' :''; ?> value="Promosi">Promosi</option>
                                    <option <?= $pan->jenis_usulan == "Demosi" ? 'selected' :''; ?> value="Demosi">Demosi</option>
                                    <option <?= $pan->jenis_usulan == "Surat Teguran" ? 'selected' :''; ?> value="Surat Teguran">Surat Teguran</option>
                                    <option <?= $pan->jenis_usulan == "Surat Peringatan I" ? 'selected' :''; ?> value="Surat Peringatan I">Surat Peringatan I</option>
                                    <option <?= $pan->jenis_usulan == "Surat Peringatan II" ? 'selected' :''; ?> value="Surat Peringatan II">Surat Peringatan II</option>
                                    <option <?= $pan->jenis_usulan == "Surat Peringatan III" ? 'selected' :''; ?> value="Surat Peringatan III">Surat Peringatan III</option>
                                    <option <?= $pan->jenis_usulan == "PHK" ? 'selected' :''; ?> value="PHK">PHK</option>
                                    <option <?= $pan->jenis_usulan == "Penugasan Jabatan Sementara" ? 'selected' :''; ?> value="Penugasan Jabatan Sementara">Penugasan Jabatan Sementara</option>
                                    <option <?= $pan->jenis_usulan == "Lain-lain" ? 'selected' :''; ?> value="Lain-lain">Lain-lain</option>
                                    </select>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="tgl_berlaku_mulai" class="col-sm-3 control-label">Tgl Berlaku Mulai 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="tgl_berlaku_mulai"  placeholder="Tgl Berlaku Mulai" id="tgl_berlaku_mulai" value="<?= set_value('pan_tgl_berlaku_mulai_name', $pan->tgl_berlaku_mulai); ?>">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                       
                                                 
                                                <div class="form-group ">
                            <label for="tgl_berlaku_selesai" class="col-sm-3 control-label">Tgl Berlaku Selesai 
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="tgl_berlaku_selesai"  placeholder="Tgl Berlaku Selesai" id="tgl_berlaku_selesai" value="<?= set_value('pan_tgl_berlaku_selesai_name', $pan->tgl_berlaku_selesai); ?>">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                       
                                                 
                                                <div class="form-group ">
                            <label for="perubahan_dari" class="col-sm-3 control-label">Perubahan Dari 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="perubahan_dari" id="perubahan_dari" placeholder="Perubahan Dari" value="<?= set_value('perubahan_dari', $pan->perubahan_dari); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="perubahan_menjadi" class="col-sm-3 control-label">Perubahan Menjadi 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="perubahan_menjadi" id="perubahan_menjadi" placeholder="Perubahan Menjadi" value="<?= set_value('perubahan_menjadi', $pan->perubahan_menjadi); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="dasar_usulan" class="col-sm-3 control-label">Dasar Usulan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <textarea id="dasar_usulan" name="dasar_usulan" rows="5" class="textarea"><?= set_value('dasar_usulan', $pan->dasar_usulan); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group  wrapper-options-crud">
                            <label for="lampiran_pendukung" class="col-sm-3 control-label">Lampiran Pendukung 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('Hasil Assessment', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="Hasil Assessment"> Hasil Assessment                                    </label>
                                    </div>
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('Lampiran KPI', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="Lampiran KPI"> Lampiran KPI                                    </label>
                                    </div>
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('Hasil Penilaian Tahunan', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="Hasil Penilaian Tahunan"> Hasil Penilaian Tahunan                                    </label>
                                    </div>
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('Dokumen Karyawan', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="Dokumen Karyawan"> Dokumen Karyawan                                    </label>
                                    </div>
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('Folder Merah', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="Folder Merah"> Folder Merah                                    </label>
                                    </div>
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('Laporan Absensi', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="Laporan Absensi"> Laporan Absensi                                    </label>
                                    </div>
                                    <div class="col-md-3  padding-left-0">
                                    <label>
                                    <input <?= in_array('FPTK', explode(',', $pan->lampiran_pendukung)) ? 'checked' : ''; ?>  type="checkbox" class="flat-red" name="lampiran_pendukung[]" value="FPTK"> FPTK                                    </label>
                                    </div>
                                                                        <div class="row-fluid clear-both">
                                    <small class="info help-block">
                                    </small>
                                    </div>
                                    
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="pencapaian_KPI" class="col-sm-3 control-label">Pencapaian KPI 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <textarea id="pencapaian_KPI" name="pencapaian_KPI" rows="5" class="textarea"><?= set_value('pencapaian_KPI', $pan->pencapaian_KPI); ?></textarea>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/pan';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_pan = $('#form_pan');
        var data_post = form_pan.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_pan.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id = $('#pan_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
           
    
    }); /*end doc ready*/
</script>