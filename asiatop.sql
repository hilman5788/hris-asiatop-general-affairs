-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 14, 2019 at 04:50 AM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manp3368_asiatop`
--

-- --------------------------------------------------------

--
-- Table structure for table `aauth_groups`
--

CREATE TABLE IF NOT EXISTS `aauth_groups` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'HC', 'HC'),
(3, 'Direksi', 'Direksi'),
(4, 'Manager', 'Manager'),
(5, 'SPV', 'SPV'),
(6, 'Staff', 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_group_to_group`
--

CREATE TABLE IF NOT EXISTS `aauth_group_to_group` (
  `group_id` int(11) unsigned NOT NULL,
  `subgroup_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_login_attempts`
--

CREATE TABLE IF NOT EXISTS `aauth_login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_login_attempts`
--

INSERT INTO `aauth_login_attempts` (`id`, `ip_address`, `timestamp`, `login_attempts`) VALUES
(19, '115.124.78.162', '2018-09-27 07:42:23', 1),
(20, '114.124.174.237', '2018-09-27 08:06:26', 1),
(21, '115.124.76.195', '2018-09-27 09:43:31', 2);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perms`
--

CREATE TABLE IF NOT EXISTS `aauth_perms` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB AUTO_INCREMENT=628 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_perms`
--

INSERT INTO `aauth_perms` (`id`, `name`, `definition`) VALUES
(1, 'menu_dashboard', NULL),
(2, 'menu_crud_builder', NULL),
(3, 'menu_api_builder', NULL),
(4, 'menu_page_builder', NULL),
(5, 'menu_form_builder', NULL),
(6, 'menu_menu', NULL),
(7, 'menu_auth', NULL),
(8, 'menu_user', NULL),
(9, 'menu_group', NULL),
(10, 'menu_access', NULL),
(11, 'menu_permission', NULL),
(12, 'menu_api_documentation', NULL),
(13, 'menu_web_documentation', NULL),
(14, 'menu_settings', NULL),
(15, 'user_list', NULL),
(16, 'user_update_status', NULL),
(17, 'user_export', NULL),
(18, 'user_add', NULL),
(19, 'user_update', NULL),
(20, 'user_update_profile', NULL),
(21, 'user_update_password', NULL),
(22, 'user_profile', NULL),
(23, 'user_view', NULL),
(24, 'user_delete', NULL),
(25, 'blog_list', NULL),
(26, 'blog_export', NULL),
(27, 'blog_add', NULL),
(28, 'blog_update', NULL),
(29, 'blog_view', NULL),
(30, 'blog_delete', NULL),
(31, 'form_list', NULL),
(32, 'form_export', NULL),
(33, 'form_add', NULL),
(34, 'form_update', NULL),
(35, 'form_view', NULL),
(36, 'form_manage', NULL),
(37, 'form_delete', NULL),
(38, 'crud_list', NULL),
(39, 'crud_export', NULL),
(40, 'crud_add', NULL),
(41, 'crud_update', NULL),
(42, 'crud_view', NULL),
(43, 'crud_delete', NULL),
(44, 'rest_list', NULL),
(45, 'rest_export', NULL),
(46, 'rest_add', NULL),
(47, 'rest_update', NULL),
(48, 'rest_view', NULL),
(49, 'rest_delete', NULL),
(50, 'group_list', NULL),
(51, 'group_export', NULL),
(52, 'group_add', NULL),
(53, 'group_update', NULL),
(54, 'group_view', NULL),
(55, 'group_delete', NULL),
(56, 'permission_list', NULL),
(57, 'permission_export', NULL),
(58, 'permission_add', NULL),
(59, 'permission_update', NULL),
(60, 'permission_view', NULL),
(61, 'permission_delete', NULL),
(62, 'access_list', NULL),
(63, 'access_add', NULL),
(64, 'access_update', NULL),
(65, 'menu_list', NULL),
(66, 'menu_add', NULL),
(67, 'menu_update', NULL),
(68, 'menu_delete', NULL),
(69, 'menu_save_ordering', NULL),
(70, 'menu_type_add', NULL),
(71, 'page_list', NULL),
(72, 'page_export', NULL),
(73, 'page_add', NULL),
(74, 'page_update', NULL),
(75, 'page_view', NULL),
(76, 'page_delete', NULL),
(77, 'blog_list', NULL),
(78, 'blog_export', NULL),
(79, 'blog_add', NULL),
(80, 'blog_update', NULL),
(81, 'blog_view', NULL),
(82, 'blog_delete', NULL),
(83, 'setting', NULL),
(84, 'setting_update', NULL),
(85, 'dashboard', NULL),
(86, 'extension_list', NULL),
(87, 'extension_activate', NULL),
(88, 'extension_deactivate', NULL),
(94, 'menu_naskah_nusantara', ''),
(95, 'menu_super_admin', ''),
(96, 'menu_groups', ''),
(97, 'menu_manajemen_pengguna', ''),
(98, 'menu_pengguna', ''),
(99, 'menu_blog', ''),
(100, 'menu_kelola_halaman', ''),
(101, 'menu_halaman_static', ''),
(102, 'menu_halaman_blog', ''),
(103, 'menu_akses', ''),
(104, 'menu_permisi', ''),
(105, 'menu_hak_akses', ''),
(106, 'menu_grup', ''),
(107, 'menu_jenis_preservasi', ''),
(108, 'menu_cara_penggunaan', ''),
(109, 'menu_faq', ''),
(110, 'menu_syarat_&_ketentuan', ''),
(111, 'menu_kebijakan_&_privasi', ''),
(112, 'menu_kebijakan_privasi', ''),
(113, 'menu_prosedur', ''),
(149, 'menu_kelola_berita', ''),
(150, 'menu_berita', ''),
(151, 'menu_kategori_berita', ''),
(152, 'menu_kategori', ''),
(158, 'menu_halaman', ''),
(159, 'menu_jenis', ''),
(165, 'menu_kelola_pengguna', ''),
(166, 'menu_backup_data', ''),
(167, 'menu_builder', ''),
(168, 'menu_menu_builder', ''),
(169, 'menu_setting', ''),
(170, 'menu_dokumentasi', ''),
(171, 'menu_superadmin', ''),
(172, 'menu_crud', ''),
(173, 'menu_rest', ''),
(174, 'menu_global', ''),
(175, 'menu_daftar_api_key', ''),
(176, 'menu_api_key', ''),
(177, 'menu_ekstensi', ''),
(178, 'menu_pengaturan', ''),
(179, 'menu_umum', ''),
(180, 'menu_kelola_naskah', ''),
(181, 'menu_naskah_daerah', ''),
(182, 'menu_pemilik_naskah', ''),
(183, 'menu_web', ''),
(184, 'menu_api', ''),
(185, 'menu_rest_api', ''),
(186, 'menu_preservasi', ''),
(187, 'menu_navigasi_utama', ''),
(188, 'menu_navigasi_preservasi', ''),
(189, 'menu_administrasi', ''),
(190, 'menu_pemetaan_naskah', ''),
(191, 'menu_survey_kondisi_fisik', ''),
(192, 'menu_tindakan_preservasi', ''),
(193, 'menu_koleksi_digital', ''),
(194, 'menu_perawatan_badan_pustaka', ''),
(195, 'menu_perbaikan_bahan_pustaka', ''),
(196, 'menu_alih_media', ''),
(197, 'menu_perawatan_bahan_pustaka', ''),
(198, 'api_naskah_nusantara_all', ''),
(199, 'api_naskah_nusantara_detail', ''),
(200, 'api_naskah_nusantara_add', ''),
(201, 'api_naskah_nusantara_update', ''),
(202, 'api_naskah_nusantara_delete', ''),
(213, 'menu_survey_kondisi', ''),
(239, 'menu_referensi', ''),
(240, 'menu_master_referensi', ''),
(246, 'menu_preferensi', ''),
(257, 'menu_kategori_preferensi', ''),
(278, 'menu_database', ''),
(364, 'menu_alih_media_format_digital', ''),
(365, 'menu_alih_media_format_mikro', ''),
(366, 'menu_alih_media_reproduksi_foto', ''),
(367, 'menu_format_digital', ''),
(368, 'menu_format_mikro', ''),
(369, 'menu_reproduksi_foto', ''),
(370, 'menu_kelola_koleksi_digital', ''),
(371, 'menu_pengambilan_objek', ''),
(372, 'menu_preservasi_digital', ''),
(378, 'menu_pemetaan_naskah_nusantara', ''),
(379, 'menu_pelestarian_naskah', ''),
(380, 'menu_laporan', ''),
(381, 'menu_administrator', ''),
(422, 'menu_analitik_&_laporan', ''),
(423, 'menu_analitik_dan_laporan', ''),
(434, 'menu_employee_self_service', ''),
(435, 'menu_check_in/out', ''),
(436, 'menu_request_cuti', ''),
(437, 'menu_permit_request', ''),
(438, 'menu_leave_request', ''),
(439, 'menu_sick_request', ''),
(440, 'menu_leave', ''),
(441, 'menu_permit', ''),
(442, 'menu_sick', ''),
(443, 'menu_overtime', ''),
(444, 'menu_my_hr', ''),
(445, 'menu_my_request', ''),
(446, 'menu_education', ''),
(447, 'menu_family', ''),
(448, 'menu_training', ''),
(449, 'menu_org_development', ''),
(450, 'menu_work_structure', ''),
(451, 'menu_performance_(pms)', ''),
(452, 'menu_recruitment', ''),
(453, 'menu_pengajuan_fptk', ''),
(454, 'menu_proses_recruitment', ''),
(455, 'menu_training_&_dev', ''),
(456, 'menu_training_and_development', ''),
(457, 'menu_training_and_dev', ''),
(458, 'menu_organisation_dev', ''),
(459, 'menu_jadwal_training', ''),
(460, 'menu_materi_training', ''),
(461, 'menu_pengajuan_training', ''),
(462, 'menu_personal_admin', ''),
(463, 'menu_pan', ''),
(464, 'menu_evaluasi_kerja', ''),
(465, 'menu_exit_interview', ''),
(466, 'jadwal_training_add', ''),
(467, 'jadwal_training_update', ''),
(468, 'jadwal_training_view', ''),
(469, 'jadwal_training_delete', ''),
(470, 'jadwal_training_list', ''),
(471, 'materi_training_add', ''),
(472, 'materi_training_update', ''),
(473, 'materi_training_view', ''),
(474, 'materi_training_delete', ''),
(475, 'materi_training_list', ''),
(476, 'pengajuan_fptk_add', ''),
(477, 'pengajuan_fptk_update', ''),
(478, 'pengajuan_fptk_view', ''),
(479, 'pengajuan_fptk_delete', ''),
(480, 'pengajuan_fptk_list', ''),
(481, 'menu_company', ''),
(497, 'menu_grade', ''),
(498, 'menu_employment', ''),
(499, 'menu_report', ''),
(500, 'menu_employee', ''),
(506, 'menu_department', ''),
(512, 'menu_position', ''),
(513, 'position_add', ''),
(514, 'position_update', ''),
(515, 'position_view', ''),
(516, 'position_delete', ''),
(517, 'position_list', ''),
(518, 'menu_job_title', ''),
(524, 'work_location_add', ''),
(525, 'work_location_update', ''),
(526, 'work_location_view', ''),
(527, 'work_location_delete', ''),
(528, 'work_location_list', ''),
(529, 'menu_work_location', ''),
(535, 'employee_add', ''),
(536, 'employee_update', ''),
(537, 'employee_view', ''),
(538, 'employee_delete', ''),
(539, 'employee_list', ''),
(540, 'company_add', ''),
(541, 'company_update', ''),
(542, 'company_view', ''),
(543, 'company_delete', ''),
(544, 'company_list', ''),
(545, 'pengajuan_training_add', ''),
(546, 'pengajuan_training_update', ''),
(547, 'pengajuan_training_view', ''),
(548, 'pengajuan_training_delete', ''),
(549, 'pengajuan_training_list', ''),
(550, 'pan_add', ''),
(551, 'pan_update', ''),
(552, 'pan_view', ''),
(553, 'pan_delete', ''),
(554, 'pan_list', ''),
(555, 'fptk_add', ''),
(556, 'fptk_update', ''),
(557, 'fptk_view', ''),
(558, 'fptk_delete', ''),
(559, 'fptk_list', ''),
(565, 'menu_division', ''),
(566, 'form_employees_add', ''),
(567, 'form_employees_update', ''),
(568, 'form_employees_view', ''),
(569, 'form_employees_delete', ''),
(570, 'corporate_add', ''),
(571, 'corporate_update', ''),
(572, 'corporate_view', ''),
(573, 'corporate_delete', ''),
(574, 'corporate_list', ''),
(575, 'menu_corporate', ''),
(576, 'directorate_add', ''),
(577, 'directorate_update', ''),
(578, 'directorate_view', ''),
(579, 'directorate_delete', ''),
(580, 'directorate_list', ''),
(581, 'menu_directorate', ''),
(582, 'division_add', ''),
(583, 'division_update', ''),
(584, 'division_view', ''),
(585, 'division_delete', ''),
(586, 'division_list', ''),
(587, 'department_add', ''),
(588, 'department_update', ''),
(589, 'department_view', ''),
(590, 'department_delete', ''),
(591, 'department_list', ''),
(592, 'section_add', ''),
(593, 'section_update', ''),
(594, 'section_view', ''),
(595, 'section_delete', ''),
(596, 'section_list', ''),
(597, 'menu_section', ''),
(598, 'supervisor_add', ''),
(599, 'supervisor_update', ''),
(600, 'supervisor_view', ''),
(601, 'supervisor_delete', ''),
(602, 'supervisor_list', ''),
(603, 'menu_supervisor', ''),
(604, 'officer_add', ''),
(605, 'officer_update', ''),
(606, 'officer_view', ''),
(607, 'officer_delete', ''),
(608, 'officer_list', ''),
(609, 'menu_officer', ''),
(615, 'grade_level_add', ''),
(616, 'grade_level_update', ''),
(617, 'grade_level_view', ''),
(618, 'grade_level_delete', ''),
(619, 'grade_level_list', ''),
(620, 'menu_grade_job', ''),
(621, 'menu_grade_level', ''),
(622, 'grade_job_add', ''),
(623, 'grade_job_update', ''),
(624, 'grade_job_view', ''),
(625, 'grade_job_delete', ''),
(626, 'grade_job_list', ''),
(627, 'menu_late', '');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_group`
--

CREATE TABLE IF NOT EXISTS `aauth_perm_to_group` (
  `perm_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_perm_to_group`
--

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES
(94, 1),
(94, 1),
(94, 1),
(95, 1),
(97, 1),
(100, 1),
(94, 1),
(94, 4),
(85, 4),
(89, 4),
(90, 4),
(91, 4),
(92, 4),
(93, 4),
(106, 1),
(94, 3),
(181, 3),
(190, 3),
(191, 3),
(192, 3),
(193, 3),
(194, 3),
(195, 3),
(196, 3),
(364, 3),
(365, 3),
(366, 3),
(20, 3),
(21, 3),
(22, 3),
(85, 3),
(301, 3),
(303, 3),
(306, 3),
(308, 3),
(331, 3),
(333, 3),
(341, 3),
(343, 3),
(351, 3),
(353, 3),
(356, 3),
(358, 3),
(361, 3),
(363, 3),
(375, 3),
(377, 3),
(94, 2),
(181, 2),
(190, 2),
(191, 2),
(192, 2),
(193, 2),
(194, 2),
(195, 2),
(196, 2),
(364, 2),
(365, 2),
(366, 2),
(20, 2),
(21, 2),
(22, 2),
(85, 2),
(301, 2),
(303, 2),
(306, 2),
(308, 2),
(331, 2),
(333, 2),
(341, 2),
(343, 2),
(351, 2),
(353, 2),
(356, 2),
(358, 2),
(361, 2),
(363, 2),
(375, 2),
(377, 2),
(1, 1),
(1, 2),
(1, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_user`
--

CREATE TABLE IF NOT EXISTS `aauth_perm_to_user` (
  `perm_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_pms`
--

CREATE TABLE IF NOT EXISTS `aauth_pms` (
  `id` int(11) unsigned NOT NULL,
  `sender_id` int(11) unsigned NOT NULL,
  `receiver_id` int(11) unsigned NOT NULL,
  `title` varchar(225) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT NULL,
  `pm_deleted_receiver` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user`
--

CREATE TABLE IF NOT EXISTS `aauth_user` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_users`
--

CREATE TABLE IF NOT EXISTS `aauth_users` (
  `id` int(11) unsigned NOT NULL,
  `email` varchar(100) NOT NULL,
  `oauth_uid` text,
  `oauth_provider` varchar(100) DEFAULT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `avatar` text NOT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `top_secret` varchar(16) DEFAULT NULL,
  `ip_address` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `oauth_uid`, `oauth_provider`, `pass`, `username`, `full_name`, `avatar`, `banned`, `last_login`, `last_activity`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `top_secret`, `ip_address`) VALUES
(1, 'admin@asiatop.co.id', NULL, NULL, '8b235284a9f7a82364468e52dab386f33844421b481113794e0b4d634c86d0f3', 'admin', 'Admin', '', 0, '2019-01-12 06:18:51', '2019-01-12 06:18:51', '2018-09-21 23:23:46', NULL, '2018-10-11 00:00:00', 'FufQi1bgH5xPcvyz', NULL, NULL, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_to_group`
--

CREATE TABLE IF NOT EXISTS `aauth_user_to_group` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 4),
(3, 3),
(4, 2),
(5, 5),
(6, 6),
(7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_variables`
--

CREATE TABLE IF NOT EXISTS `aauth_user_variables` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `tags` text NOT NULL,
  `category` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `author` varchar(100) NOT NULL,
  `viewers` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `slug`, `content`, `image`, `tags`, `category`, `status`, `author`, `viewers`, `created_at`, `updated_at`) VALUES
(1, 'Hello Wellcome To Cicool Builder', 'Hello-Wellcome-To-Ciool-Builder', 'greetings from our team I hope to be happy! ', 'wellcome.jpg', 'greetings', '1', 'publish', 'admin', 1, '2018-09-21 23:23:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE IF NOT EXISTS `blog_category` (
  `category_id` int(11) unsigned NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `category_desc` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`category_id`, `category_name`, `category_desc`) VALUES
(1, 'Technology', ''),
(2, 'Lifestyle', '');

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` int(11) unsigned NOT NULL,
  `captcha_time` int(10) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(2, 1538321467, '180.252.132.223', 'XWKH');

-- --------------------------------------------------------

--
-- Table structure for table `cc_options`
--

CREATE TABLE IF NOT EXISTS `cc_options` (
  `id` int(11) unsigned NOT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_value` text
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cc_options`
--

INSERT INTO `cc_options` (`id`, `option_name`, `option_value`) VALUES
(1, 'active_theme', 'cicool'),
(2, 'favicon', 'default.png'),
(3, 'site_name', 'HRIS - Asiatop'),
(4, 'enable_disqus', NULL),
(5, 'disqus_id', ''),
(6, 'email', 'admin@admin.com'),
(7, 'author', 'admin'),
(8, 'site_description', ''),
(9, 'keywords', ''),
(10, 'landing_page_id', 'default'),
(11, 'timezone', 'Asia/Jakarta'),
(12, 'google_id', ''),
(13, 'google_secret', '');

-- --------------------------------------------------------

--
-- Table structure for table `cc_session`
--

CREATE TABLE IF NOT EXISTS `cc_session` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(50) NOT NULL DEFAULT '',
  `company_logo` varchar(150) DEFAULT '',
  `company_start_date` date DEFAULT NULL,
  `company_vision` varchar(255) DEFAULT '',
  `company_mission` varchar(255) DEFAULT '',
  `company_is_holding` tinyint(1) DEFAULT NULL,
  `company_type` varchar(50) DEFAULT '',
  `company_business_type` varchar(50) DEFAULT '',
  `company_address_country` varchar(150) DEFAULT '',
  `company_address_city` varchar(150) DEFAULT '',
  `company_address_location` varchar(255) DEFAULT '',
  `company_address_zip_code` varchar(15) DEFAULT '',
  `company_contact_phone` varchar(15) DEFAULT '',
  `company_contact_fax` varchar(15) DEFAULT '',
  `company_contact_email` varchar(50) DEFAULT '',
  `company_tax_npkp` varchar(50) DEFAULT '',
  `company_tax_npwp` varchar(50) DEFAULT '',
  `company_tax_location` varchar(50) DEFAULT '',
  `company_bank_name` varchar(50) DEFAULT '',
  `company_bank_account_no` varchar(25) DEFAULT '',
  `company_bank_account_name` varchar(50) DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_logo`, `company_start_date`, `company_vision`, `company_mission`, `company_is_holding`, `company_type`, `company_business_type`, `company_address_country`, `company_address_city`, `company_address_location`, `company_address_zip_code`, `company_contact_phone`, `company_contact_fax`, `company_contact_email`, `company_tax_npkp`, `company_tax_npwp`, `company_tax_location`, `company_bank_name`, `company_bank_account_no`, `company_bank_account_name`) VALUES
(1, 'CV. ASIATOP', '20181012094846-2018-10-12company094830.jpg', '2018-10-10', 'Visi', 'Misi', 0, 'Manufacture', 'Retail', 'Indonesia', 'Jakarta Utara', 'Ruko Tama Indah Kav 36 CW, Jembatan 3, Pluit', '14450', '(021) 6627989', '(021) 6627989', 'ASW-marketing@asiatop.co.id', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `corporate`
--

CREATE TABLE IF NOT EXISTS `corporate` (
  `corporate_id` int(11) NOT NULL,
  `corporate_name` varchar(150) NOT NULL DEFAULT '0',
  `corporate_grade` varchar(25) NOT NULL DEFAULT '0',
  `corporate_rank` varchar(25) NOT NULL DEFAULT '0',
  `corporate_job_title` varchar(150) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corporate`
--

INSERT INTO `corporate` (`corporate_id`, `corporate_name`, `corporate_grade`, `corporate_rank`, `corporate_job_title`) VALUES
(1, 'Presiden Director', 'A', 'Presdir', 'Presiden Direktur');

-- --------------------------------------------------------

--
-- Table structure for table `crud`
--

CREATE TABLE IF NOT EXISTS `crud` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `primary_key` varchar(200) NOT NULL,
  `page_read` varchar(20) DEFAULT NULL,
  `page_create` varchar(20) DEFAULT NULL,
  `page_update` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud`
--

INSERT INTO `crud` (`id`, `title`, `subject`, `table_name`, `primary_key`, `page_read`, `page_create`, `page_update`) VALUES
(53, 'Jadwal Training', 'Jadwal Training', 'jadwal_training', 'id', 'yes', 'yes', 'yes'),
(54, 'Materi Training', 'Materi Training', 'materi_training', 'id', 'yes', 'yes', 'yes'),
(55, 'Pengajuan Fptk', 'Pengajuan Fptk', 'pengajuan_fptk', 'id', 'yes', 'yes', 'yes'),
(61, 'Position', 'Position', 'position', 'position_id', 'yes', 'yes', 'yes'),
(63, 'Work Location', 'Work Location', 'work_location', 'work_location_id', 'yes', 'yes', 'yes'),
(65, 'Employee', 'Employee', 'employee', 'employee_id', 'yes', 'yes', 'yes'),
(66, 'Company', 'Company', 'company', 'company_id', 'yes', 'yes', 'yes'),
(67, 'Pengajuan Training', 'Pengajuan Training', 'pengajuan_training', 'id', 'yes', 'yes', 'yes'),
(68, 'PAN', 'PAN', 'pan', 'id', 'yes', 'yes', 'yes'),
(69, 'FPTK', 'FPTK', 'fptk', 'id', 'yes', 'yes', 'yes'),
(71, 'Corporate', 'Corporate', 'corporate', 'corporate_id', 'yes', 'yes', 'yes'),
(72, 'Directorate', 'Directorate', 'directorate', 'directorate_id', 'yes', 'yes', 'yes'),
(73, 'Division', 'Division', 'division', 'division_id', 'yes', 'yes', 'yes'),
(74, 'Department', 'Department', 'department', 'department_id', 'yes', 'yes', 'yes'),
(75, 'Section', 'Section', 'section', 'section_id', 'yes', 'yes', 'yes'),
(76, 'Supervisor', 'Supervisor', 'supervisor', 'supervisor_id', 'yes', 'yes', 'yes'),
(77, 'Officer', 'Officer', 'officer', 'officer_id', 'yes', 'yes', 'yes'),
(79, 'Grade Level', 'Grade Level', 'grade_level', 'id', 'yes', 'yes', 'yes'),
(80, 'Grade Job', 'Grade Job', 'grade_job', 'grade_id', 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `crud_custom_option`
--

CREATE TABLE IF NOT EXISTS `crud_custom_option` (
  `id` int(11) unsigned NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5401 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_custom_option`
--

INSERT INTO `crud_custom_option` (`id`, `crud_field_id`, `crud_id`, `option_value`, `option_label`) VALUES
(3974, 5643, 64, 'L', 'Laki-laki'),
(3975, 5643, 64, 'P', 'Perempuan'),
(3976, 5646, 64, 'D-0', 'D-0'),
(3977, 5646, 64, 'D-1', 'D-1'),
(3978, 5646, 64, 'M-0', 'M-0'),
(3979, 5646, 64, 'M-1', 'M-1'),
(3980, 5646, 64, 'M-2', 'M-2'),
(3981, 5646, 64, 'M-3', 'M-3'),
(3982, 5646, 64, 'M-4', 'M-4'),
(3983, 5646, 64, 'S-0', 'S-0'),
(3984, 5648, 64, 'A', 'A'),
(3985, 5648, 64, 'B', 'B'),
(3986, 5648, 64, 'AB', 'AB'),
(3987, 5648, 64, 'O', 'O'),
(3988, 5649, 64, 'Islam', 'Islam'),
(3989, 5649, 64, 'Katolik', 'Katolik'),
(3990, 5649, 64, 'Kristen', 'Kristen'),
(3991, 5649, 64, 'Hindu', 'Hindu'),
(3992, 5649, 64, 'Budha', 'Budha'),
(3993, 5651, 64, 'Kawin tanpa tanggungan', 'Kawin tanpa tanggungan'),
(3994, 5651, 64, 'Kawin dengan 1 tanggungan', 'Kawin dengan 1 tanggungan'),
(3995, 5651, 64, 'Kawin dengan 2 tanggungan', 'Kawin dengan 2 tanggungan'),
(3996, 5651, 64, 'Kawin dengan 3 tanggungan', 'Kawin dengan 3 tanggungan'),
(3997, 5651, 64, 'Tidak kawin tanpa tanggungan', 'Tidak kawin tanpa tanggungan'),
(3998, 5651, 64, 'Tidak kawin dengan 1 tanggungan', 'Tidak kawin dengan 1 tanggungan'),
(3999, 5651, 64, 'Tidak kawin dengan 2 tanggungan', 'Tidak kawin dengan 2 tanggungan'),
(4000, 5652, 64, 'Peg. Tetap', 'Peg. Tetap'),
(4001, 5653, 64, 'Permanent', 'Permanent'),
(4002, 5653, 64, 'Contract', 'Contract'),
(4003, 5666, 64, 'BCA', 'BCA'),
(4004, 5666, 64, 'CIMB NIAGA', 'CIMB NIAGA'),
(4005, 5666, 64, 'CIMB NIAGA II', 'CIMB NIAGA II'),
(4006, 5666, 64, 'DANAMON', 'DANAMON'),
(4007, 5668, 64, 'Diploma 1', 'Diploma 1'),
(4008, 5668, 64, 'Diploma 2', 'Diploma 2'),
(4009, 5668, 64, 'Diploma 3', 'Diploma 3'),
(4010, 5668, 64, 'Strata 1', 'Strata 1'),
(4011, 5668, 64, 'Strata 2', 'Strata 2'),
(4012, 5668, 64, 'Strata 3', 'Strata 3'),
(4013, 5668, 64, 'SMA/SMK/STM', 'SMA/SMK/STM'),
(4456, 6173, 55, 'Urgent', 'Urgent'),
(4457, 6173, 55, 'Normal', 'Normal'),
(4458, 6175, 55, 'Penambahan MPP', 'Penambahan MPP'),
(4459, 6175, 55, 'Penggantian Karyawan Resign', 'Penggantian Karyawan Resign'),
(4460, 6175, 55, 'Penggantian Karyawan Resign', 'Penggantian Karyawan Resign'),
(4461, 6175, 55, 'Pengisian Jabatan Baru', 'Pengisian Jabatan Baru'),
(4462, 6175, 55, 'Penggantian Akibat Rotasi/Mutasi', 'Penggantian Akibat Rotasi/Mutasi'),
(4463, 6177, 55, 'L', 'Laki-laki'),
(4464, 6177, 55, 'P', 'Perempuan'),
(4465, 6179, 55, 'PKWT', 'PKWT'),
(4466, 6179, 55, 'PWTT', 'PWTT'),
(4467, 6180, 55, 'Single', 'Single'),
(4468, 6180, 55, 'Married', 'Married'),
(4469, 6181, 55, 'SMA / STM / SMK', 'SMA / STM / SMK'),
(4470, 6181, 55, 'Diploma 3', 'Diploma 3'),
(4471, 6181, 55, 'S1', 'S1'),
(4472, 6181, 55, 'S2', 'S2'),
(4473, 6182, 55, '1 - 2 tahun', '1 - 2 tahun'),
(4474, 6182, 55, '2 - 3 tahun', '2 - 3 tahun'),
(4475, 6182, 55, '> 3 tahun', '> 3 tahun'),
(4476, 6183, 55, 'Administratif / Filling', 'Administratif / Filling'),
(4477, 6183, 55, 'Pelayanan Pelanggan / Collection', 'Pelayanan Pelanggan / Collection'),
(4478, 6183, 55, 'Teknisi Elektronik', 'Teknisi Elektronik'),
(4479, 6183, 55, 'Komputer Ms Word / Excel', 'Komputer Ms Word / Excel'),
(4480, 6183, 55, 'Pemrograman', 'Pemrograman'),
(4481, 6183, 55, 'Lainnya', 'Lainnya'),
(4482, 6188, 53, 'Lulus', 'Lulus'),
(4483, 6188, 53, 'Tidak Lulus', 'Tidak Lulus'),
(4500, 6215, 67, 'Technical Skill Training', 'Technical Skill Training'),
(4501, 6215, 67, 'Soft Skill Training', 'Soft Skill Training'),
(4502, 6216, 67, 'Training Eksternal', 'Training Eksternal'),
(4503, 6216, 67, 'Trainning Internal', 'Training Internal'),
(4504, 6219, 67, 'Perbaikan Kinerja', 'Perbaikan Kinerja'),
(4505, 6219, 67, 'Pengembangan Keahlian', 'Pengembangan Keahlian'),
(4506, 6219, 67, 'Penambahan Pengetahuan', 'Penambahan Pengetahuan'),
(4507, 6219, 67, 'Team Building', 'Team Building'),
(4508, 6222, 67, 'Kantor', 'Kantor'),
(4509, 6222, 67, 'Lainnya', 'Lainnya'),
(4510, 6226, 67, 'Half Day', 'Half Day'),
(4511, 6226, 67, 'Full Day', 'Full Day'),
(4512, 6228, 67, 'Transfer', 'Transfer'),
(4513, 6228, 67, 'Cash', 'Cash'),
(4514, 6231, 67, 'Bersedia', 'Bersedia'),
(4515, 6231, 67, 'Tidak Bersedia', 'Tidak Bersedia'),
(4567, 6264, 68, 'Mutasi', 'Mutasi'),
(4568, 6264, 68, 'Promosi', 'Promosi'),
(4569, 6264, 68, 'Demosi', 'Demosi'),
(4570, 6264, 68, 'Surat Teguran', 'Surat Teguran'),
(4571, 6264, 68, 'Surat Peringatan I', 'Surat Peringatan I'),
(4572, 6264, 68, 'Surat Peringatan II', 'Surat Peringatan II'),
(4573, 6264, 68, 'Surat Peringatan III', 'Surat Peringatan III'),
(4574, 6264, 68, 'PHK', 'PHK'),
(4575, 6264, 68, 'Penugasan Jabatan Sementara', 'Penugasan Jabatan Sementara'),
(4576, 6264, 68, 'Lain-lain', 'Lain-lain'),
(4577, 6270, 68, 'Hasil Assessment', 'Hasil Assessment'),
(4578, 6270, 68, 'Lampiran KPI', 'Lampiran KPI'),
(4579, 6270, 68, 'Hasil Penilaian Tahunan', 'Hasil Penilaian Tahunan'),
(4580, 6270, 68, 'Dokumen Karyawan', 'Dokumen Karyawan'),
(4581, 6270, 68, 'Folder Merah', 'Folder Merah'),
(4582, 6270, 68, 'Laporan Absensi', 'Laporan Absensi'),
(4583, 6270, 68, 'FPTK', 'FPTK'),
(5361, 7957, 65, 'Laki-laki', 'Laki-laki'),
(5362, 7957, 65, 'Perempuan', 'Perempuan'),
(5363, 7960, 65, 'S-0', 'S-0'),
(5364, 7960, 65, 'M-0', 'M-0'),
(5365, 7960, 65, 'M-1', 'M-1'),
(5366, 7960, 65, 'M-2', 'M-2'),
(5367, 7960, 65, 'M-3', 'M-3'),
(5368, 7960, 65, 'M-4', 'M-4'),
(5369, 7960, 65, 'D-0', 'D-0'),
(5370, 7961, 65, 'Islam', 'Islam'),
(5371, 7961, 65, 'Kristen', 'Kristen'),
(5372, 7961, 65, 'Katolik', 'Katolik'),
(5373, 7961, 65, 'Hindu', 'Hindu'),
(5374, 7961, 65, 'Budha', 'Budha'),
(5375, 7963, 65, 'SMA/STM/SMK', 'SMA/STM/SMK'),
(5376, 7963, 65, 'Diploma 1', 'Diploma 1'),
(5377, 7963, 65, 'Diploma 2', 'Diploma 2'),
(5378, 7963, 65, 'Diploma 3', 'Diploma 3'),
(5379, 7963, 65, 'Strata 1', 'Strata 1'),
(5380, 7963, 65, 'Strata 2', 'Strata 2'),
(5381, 7963, 65, 'Strata 3', 'Strata 3'),
(5382, 7965, 65, 'A', 'A'),
(5383, 7965, 65, 'B', 'B'),
(5384, 7965, 65, 'AB', 'AB'),
(5385, 7965, 65, 'O', 'O'),
(5386, 7966, 65, 'Salary Code 1', 'Salary Code 1'),
(5387, 7966, 65, 'Salary Code 2', 'Salary Code 2'),
(5388, 7966, 65, 'Salary Code 3', 'Salary Code 3'),
(5389, 7968, 65, 'Peg. Tetap', 'Peg. Tetap'),
(5390, 7968, 65, 'Peg. Kontrak', 'Peg. Kontrak'),
(5391, 7969, 65, 'Permanent', 'Permanent'),
(5392, 7969, 65, 'Contract', 'Contract'),
(5393, 7969, 65, 'Probation', 'Probation'),
(5394, 7982, 65, 'BCA', 'BCA'),
(5395, 7982, 65, 'CIMB NIAGA I', 'CIMB NIAGA I'),
(5396, 7982, 65, 'CIMB NIAGA II', 'CIMB NIAGA II'),
(5397, 7982, 65, 'DANAMON', 'DANAMON'),
(5398, 7982, 65, 'MANDIRI', 'MANDIRI'),
(5399, 7982, 65, 'PERMATA', 'PERMATA'),
(5400, 7982, 65, 'BNI', 'BNI');

-- --------------------------------------------------------

--
-- Table structure for table `crud_field`
--

CREATE TABLE IF NOT EXISTS `crud_field` (
  `id` int(11) unsigned NOT NULL,
  `crud_id` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `input_type` varchar(200) NOT NULL,
  `show_column` varchar(10) DEFAULT NULL,
  `show_add_form` varchar(10) DEFAULT NULL,
  `show_update_form` varchar(10) DEFAULT NULL,
  `show_detail_page` varchar(10) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7990 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_field`
--

INSERT INTO `crud_field` (`id`, `crud_id`, `field_name`, `field_label`, `input_type`, `show_column`, `show_add_form`, `show_update_form`, `show_detail_page`, `sort`, `relation_table`, `relation_value`, `relation_label`) VALUES
(4918, 54, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(4919, 54, 'materi_training', 'materi_training', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(4920, 54, 'file_training', 'file_training', 'file_multiple', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(4962, 56, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(4963, 56, 'company_name', 'company_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(4964, 56, 'company_logo', 'company_logo', 'file', '', 'yes', 'yes', 'yes', 3, '', '', ''),
(4965, 56, 'company_start_date', 'company_start_date', 'date', '', 'yes', 'yes', 'yes', 4, '', '', ''),
(4966, 56, 'company_vision', 'company_vision', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(4967, 56, 'company_mission', 'company_mission', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(4968, 56, 'company_is_holding', 'company_is_holding', 'true_false', '', 'yes', 'yes', 'yes', 7, '', '', ''),
(4969, 56, 'company_type', 'company_type', 'input', '', 'yes', 'yes', 'yes', 8, '', '', ''),
(4970, 56, 'company_business_type', 'company_business_type', 'input', '', 'yes', 'yes', 'yes', 9, '', '', ''),
(4971, 56, 'company_address_country', 'company_address_country', 'input', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(4972, 56, 'company_address_city', 'company_address_city', 'input', '', 'yes', 'yes', 'yes', 11, '', '', ''),
(4973, 56, 'company_address_location', 'company_address_location', 'textarea', '', 'yes', 'yes', 'yes', 12, '', '', ''),
(4974, 56, 'company_address_zip_code', 'company_address_zip_code', 'input', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(4975, 56, 'company_contact_phone', 'company_contact_phone', 'input', 'yes', 'yes', 'yes', 'yes', 14, '', '', ''),
(4976, 56, 'company_contact_fax', 'company_contact_fax', 'input', 'yes', 'yes', 'yes', 'yes', 15, '', '', ''),
(4977, 56, 'company_contact_email', 'company_contact_email', 'email', 'yes', 'yes', 'yes', 'yes', 16, '', '', ''),
(4978, 56, 'company_tax_npkp', 'company_tax_npkp', 'input', '', 'yes', 'yes', 'yes', 17, '', '', ''),
(4979, 56, 'company_tax_npwp', 'company_tax_npwp', 'input', '', 'yes', 'yes', 'yes', 18, '', '', ''),
(4980, 56, 'company_tax_location', 'company_tax_location', 'input', '', 'yes', 'yes', 'yes', 19, '', '', ''),
(4981, 56, 'company_bank_name', 'company_bank_name', 'input', '', 'yes', 'yes', 'yes', 20, '', '', ''),
(4982, 56, 'company_bank_account_no', 'company_bank_account_no', 'input', '', 'yes', 'yes', 'yes', 21, '', '', ''),
(4983, 56, 'company_bank_account_name', 'company_bank_account_name', 'input', '', 'yes', 'yes', 'yes', 22, '', '', ''),
(4984, 56, 'created_by', 'created_by', 'current_user_id', '', '', 'yes', 'yes', 23, '', '', ''),
(4985, 56, 'created_on', 'created_on', 'timestamp', '', '', 'yes', 'yes', 24, '', '', ''),
(4986, 56, 'updated_by', 'updated_by', 'current_user_id', '', '', 'yes', 'yes', 25, '', '', ''),
(4987, 56, 'updated_on', 'updated_on', 'timestamp', '', '', 'yes', 'yes', 26, '', '', ''),
(5159, 57, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(5160, 57, 'company_name', 'company_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(5161, 57, 'company_logo', 'company_logo', 'file', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(5162, 57, 'company_start_date', 'company_start_date', 'date', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(5163, 57, 'company_vision', 'company_vision', 'textarea', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(5164, 57, 'company_mission', 'company_mission', 'textarea', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(5165, 57, 'company_is_holding', 'company_is_holding', 'true_false', '', 'yes', 'yes', 'yes', 7, '', '', ''),
(5166, 57, 'company_type', 'company_type', 'input', '', 'yes', 'yes', 'yes', 8, '', '', ''),
(5167, 57, 'company_business_type', 'company_business_type', 'input', '', 'yes', 'yes', 'yes', 9, '', '', ''),
(5168, 57, 'company_contact_phone', 'company_contact_phone', 'input', 'yes', 'yes', 'yes', 'yes', 10, '', '', ''),
(5169, 57, 'company_contact_email', 'company_contact_email', 'input', 'yes', 'yes', 'yes', 'yes', 11, '', '', ''),
(5170, 57, 'company_contact_fax', 'company_contact_fax', 'input', 'yes', 'yes', 'yes', 'yes', 12, '', '', ''),
(5171, 57, 'company_address_location', 'company_address_location', 'textarea', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(5172, 57, 'company_address_zip_code', 'company_address_zip_code', 'input', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(5173, 57, 'company_address_city', 'company_address_city', 'input', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(5174, 57, 'company_address_country', 'company_address_country', 'input', '', 'yes', 'yes', 'yes', 16, '', '', ''),
(5175, 57, 'company_tax_npkp', 'company_tax_npkp', 'input', '', 'yes', 'yes', 'yes', 17, '', '', ''),
(5176, 57, 'company_tax_npwp', 'company_tax_npwp', 'input', '', 'yes', 'yes', 'yes', 18, '', '', ''),
(5177, 57, 'company_tax_location', 'company_tax_location', 'textarea', '', 'yes', 'yes', 'yes', 19, '', '', ''),
(5178, 57, 'company_bank_name', 'company_bank_name', 'input', '', 'yes', 'yes', 'yes', 20, '', '', ''),
(5179, 57, 'company_bank_account_no', 'company_bank_account_no', 'input', '', 'yes', 'yes', 'yes', 21, '', '', ''),
(5180, 57, 'company_bank_account_name', 'company_bank_account_name', 'input', '', 'yes', 'yes', 'yes', 22, '', '', ''),
(5181, 57, 'created_by', 'created_by', 'current_user_id', '', 'yes', 'yes', 'yes', 23, '', '', ''),
(5182, 57, 'created_on', 'created_on', 'timestamp', '', 'yes', 'yes', 'yes', 24, '', '', ''),
(5183, 57, 'updated_by', 'updated_by', 'current_user_id', '', 'yes', 'yes', 'yes', 25, '', '', ''),
(5184, 57, 'updated_on', 'updated_on', 'timestamp', '', 'yes', 'yes', 'yes', 26, '', '', ''),
(5406, 58, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(5407, 58, 'grade_name', 'grade_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(5408, 58, 'grade_value', 'grade_value', 'number', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(5409, 58, 'grade_pay_min', 'payment min', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(5410, 58, 'grade_pay_max', 'payment max', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(5411, 58, 'company_id', 'company_id', 'select', 'yes', 'yes', 'yes', 'yes', 6, 'ws_company', 'id', 'company_name'),
(5412, 58, 'grade_pay_Q1', 'payment Q1', 'input', '', 'yes', 'yes', 'yes', 7, '', '', ''),
(5413, 58, 'grade_pay_Q2', 'payment Q2', 'input', '', 'yes', 'yes', 'yes', 8, '', '', ''),
(5414, 58, 'grade_pay_Q3', 'payment Q3', 'input', '', 'yes', 'yes', 'yes', 9, '', '', ''),
(5415, 58, 'grade_pay_Q4', 'payment Q4', 'input', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(5416, 58, 'grade_pay_mid', 'payment mid', 'input', '', 'yes', 'yes', 'yes', 11, '', '', ''),
(5417, 58, 'created_by', 'created_by', 'current_user_id', '', 'yes', 'yes', 'yes', 12, '', '', ''),
(5418, 58, 'created_on', 'created_on', 'timestamp', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(5419, 58, 'updated_by', 'updated_by', 'current_user_id', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(5420, 58, 'updated_on', 'updated_on', 'timestamp', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(5436, 62, 'jobtitle_id', 'jobtitle_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(5437, 62, 'jobtitle_name', 'Nama Jobtitle', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(5438, 62, 'jobtitle_desc', 'Keterangan', 'textarea', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(5634, 64, 'employee_id', 'employee_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(5635, 64, 'employee_nik', 'employee_nik', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(5636, 64, 'employee_name', 'employee_name', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(5637, 64, 'hire_date', 'hire_date', 'date', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(5638, 64, 'seniority_date', 'seniority_date', 'date', '', 'yes', 'yes', 'yes', 5, '', '', ''),
(5639, 64, 'department_id', 'department', 'select', 'yes', 'yes', 'yes', 'yes', 6, 'department', 'department_id', 'department_name'),
(5640, 64, 'grade_id', 'grade', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'grade', 'grade_id', 'grade_name'),
(5641, 64, 'position_id', 'position', 'select', 'yes', 'yes', 'yes', 'yes', 8, 'position', 'position_id', 'position_name'),
(5642, 64, 'working_location_id', 'working_location', 'select', 'yes', 'yes', 'yes', 'yes', 9, 'work_location', 'work_location_id', 'work_location_name'),
(5643, 64, 'sex', 'sex', 'custom_select', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(5644, 64, 'birth_place', 'birth_place', 'input', '', 'yes', 'yes', 'yes', 11, '', '', ''),
(5645, 64, 'birth_date', 'birth_date', 'date', 'yes', 'yes', 'yes', 'yes', 12, '', '', ''),
(5646, 64, 'marital_status', 'marital_status', 'custom_select', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(5647, 64, 'npwp', 'npwp', 'input', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(5648, 64, 'blood_type', 'blood_type', 'custom_select', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(5649, 64, 'religion', 'religion', 'custom_select', '', 'yes', 'yes', 'yes', 16, '', '', ''),
(5650, 64, 'pay_group', 'pay_group', 'input', '', 'yes', 'yes', 'yes', 17, '', '', ''),
(5651, 64, 'ptkp', 'ptkp', 'custom_select', '', 'yes', 'yes', 'yes', 18, '', '', ''),
(5652, 64, 'tax_category', 'tax_category', 'custom_select', '', 'yes', 'yes', 'yes', 19, '', '', ''),
(5653, 64, 'employee_status', 'employee_status', 'custom_select', '', 'yes', 'yes', 'yes', 20, '', '', ''),
(5654, 64, 'termination_date', 'termination_date', 'date', '', 'yes', 'yes', 'yes', 21, '', '', ''),
(5655, 64, 'address_1', 'address_1', 'input', '', 'yes', 'yes', 'yes', 22, '', '', ''),
(5656, 64, 'address_2', 'address_2', 'input', '', 'yes', 'yes', 'yes', 23, '', '', ''),
(5657, 64, 'rt', 'rt', 'input', '', 'yes', 'yes', 'yes', 24, '', '', ''),
(5658, 64, 'rw', 'rw', 'input', '', 'yes', 'yes', 'yes', 25, '', '', ''),
(5659, 64, 'city', 'city', 'input', '', 'yes', 'yes', 'yes', 26, '', '', ''),
(5660, 64, 'zip_code', 'zip_code', 'input', '', 'yes', 'yes', 'yes', 27, '', '', ''),
(5661, 64, 'state', 'state', 'input', '', 'yes', 'yes', 'yes', 28, '', '', ''),
(5662, 64, 'phone', 'phone', 'input', '', 'yes', 'yes', 'yes', 29, '', '', ''),
(5663, 64, 'fax', 'fax', 'input', '', 'yes', 'yes', 'yes', 30, '', '', ''),
(5664, 64, 'bank_account_name', 'bank_account_name', 'input', '', 'yes', 'yes', 'yes', 31, '', '', ''),
(5665, 64, 'bank_account_no', 'bank_account_no', 'input', '', 'yes', 'yes', 'yes', 32, '', '', ''),
(5666, 64, 'bank_name', 'bank_name', 'custom_select', '', 'yes', 'yes', 'yes', 33, '', '', ''),
(5667, 64, 'social_id', 'social_id', 'input', '', 'yes', 'yes', 'yes', 34, '', '', ''),
(5668, 64, 'last_education', 'last_education', 'custom_select', '', 'yes', 'yes', 'yes', 35, '', '', ''),
(5669, 64, 'cost_group', 'cost_group', 'input', '', 'yes', 'yes', 'yes', 36, '', '', ''),
(6169, 55, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(6170, 55, 'requester_id', 'requester_id', 'current_user_id', '', 'yes', 'yes', 'yes', 3, '', '', ''),
(6171, 55, 'jabatan', 'jabatan', 'number', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6172, 55, 'jumlah', 'jumlah', 'number', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(6173, 55, 'status_dibutuhkan', 'status_dibutuhkan', 'custom_select', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(6174, 55, 'atasan_langsung', 'atasan_langsung', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'aauth_users', 'id', 'full_name'),
(6175, 55, 'alasan_permohonan', 'alasan_permohonan', 'custom_select', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(6176, 55, 'penempatan', 'penempatan', 'input', 'yes', 'yes', 'yes', 'yes', 9, '', '', ''),
(6177, 55, 'jenis_kelamin', 'jenis_kelamin', 'custom_select', 'yes', 'yes', 'yes', 'yes', 10, '', '', ''),
(6178, 55, 'usia', 'usia', 'number', 'yes', 'yes', 'yes', 'yes', 11, '', '', ''),
(6179, 55, 'status_karyawan', 'status_karyawan', 'custom_select', 'yes', 'yes', 'yes', 'yes', 12, '', '', ''),
(6180, 55, 'marital_status', 'marital_status', 'custom_select', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(6181, 55, 'pendidikan', 'pendidikan', 'custom_select', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(6182, 55, 'pengalaman', 'pengalaman', 'custom_select', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(6183, 55, 'keterampilan', 'keterampilan', 'custom_select_multiple', '', 'yes', 'yes', 'yes', 16, '', '', ''),
(6184, 53, 'id', 'id', 'number', '', '', '', 'yes', 2, '', '', ''),
(6185, 53, 'nama_training', 'nama_training', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6186, 53, 'tanggal_mulai_training', 'tanggal_mulai_training', 'date', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6187, 53, 'tanggal_evaluasi', 'tanggal_evaluasi', 'date', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(6188, 53, 'status', 'status', 'custom_select', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(6189, 53, 'url_evaluasi_tertulis', 'url_evaluasi_tertulis', 'input', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(6190, 53, 'nama_trainer', 'nama_trainer', 'input', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(6191, 53, 'penyelenggara', 'penyelenggara', 'input', 'yes', 'yes', 'yes', 'yes', 9, '', '', ''),
(6212, 67, 'id', 'id', 'number', '', '', 'yes', 'yes', 1, '', '', ''),
(6213, 67, 'nama_pemohon', 'nama_pemohon', 'select', 'yes', 'yes', 'yes', 'yes', 2, 'employee', 'employee_id', 'employee_name'),
(6214, 67, 'calon_peserta', 'calon_peserta', 'select_multiple', '', 'yes', 'yes', 'yes', 3, 'employee', 'employee_id', 'employee_name'),
(6215, 67, 'jenis_training', 'jenis_training', 'custom_select', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6216, 67, 'pengadaan_training', 'pengadaan_training', 'custom_select', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(6217, 67, 'nama_training', 'nama_training', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(6218, 67, 'deskripsi_training', 'deskripsi_training', 'textarea', '', 'yes', 'yes', 'yes', 7, '', '', ''),
(6219, 67, 'tujuan_training', 'tujuan_training', 'custom_select', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(6220, 67, 'alasan_pengajuan', 'alasan_pengajuan', 'textarea', '', 'yes', 'yes', 'yes', 9, '', '', ''),
(6221, 67, 'nama_trainer', 'nama_trainer', 'input', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(6222, 67, 'lokasi_training', 'lokasi_training', 'custom_select', 'yes', 'yes', 'yes', 'yes', 11, '', '', ''),
(6223, 67, 'lembaga_pelaksana', 'lembaga_pelaksana', 'input', '', 'yes', 'yes', 'yes', 12, '', '', ''),
(6224, 67, 'tgl_mulai_training', 'tgl_mulai_training', 'datetime', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(6225, 67, 'tgl_selesai_training', 'tgl_selesai_training', 'datetime', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(6226, 67, 'lama_training', 'lama_training', 'custom_select', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(6227, 67, 'jumlah_investasi', 'jumlah_investasi', 'input', 'yes', 'yes', 'yes', 'yes', 16, '', '', ''),
(6228, 67, 'jenis_pembayaran', 'jenis_pembayaran', 'custom_select', 'yes', 'yes', 'yes', 'yes', 17, '', '', ''),
(6229, 67, 'harapan_training', 'harapan_training', 'textarea', '', 'yes', 'yes', 'yes', 18, '', '', ''),
(6230, 67, 'rencana_implementasi', 'rencana_implementasi', 'textarea', '', 'yes', 'yes', 'yes', 19, '', '', ''),
(6231, 67, 'sharing_knowledge', 'sharing_knowledge', 'custom_select', '', 'yes', 'yes', 'yes', 20, '', '', ''),
(6262, 68, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(6263, 68, 'nama_karyawan', 'nama_karyawan', 'select', 'yes', 'yes', 'yes', 'yes', 2, 'employee', 'employee_id', 'employee_name'),
(6264, 68, 'jenis_usulan', 'jenis_usulan', 'custom_select', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6265, 68, 'tgl_berlaku_mulai', 'tgl_berlaku_mulai', 'date', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6266, 68, 'tgl_berlaku_selesai', 'tgl_berlaku_selesai', 'date', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(6267, 68, 'perubahan_dari', 'perubahan_dari', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(6268, 68, 'perubahan_menjadi', 'perubahan_menjadi', 'input', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(6269, 68, 'dasar_usulan', 'dasar_usulan', 'textarea', '', 'yes', 'yes', 'yes', 8, '', '', ''),
(6270, 68, 'lampiran_pendukung', 'lampiran_pendukung', 'custom_checkbox', 'yes', 'yes', 'yes', 'yes', 9, '', '', ''),
(6271, 68, 'pencapaian_KPI', 'pencapaian_KPI', 'textarea', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(6272, 69, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(6273, 69, 'requester_id', 'requester_id', 'number', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(6274, 69, 'jabatan', 'jabatan', 'number', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6275, 69, 'jumlah', 'jumlah', 'number', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6276, 69, 'status_dibutuhkan', 'status_dibutuhkan', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(6277, 69, 'atasan_langsung', 'atasan_langsung', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(6278, 69, 'alasan_permohonan', 'alasan_permohonan', 'input', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(6279, 69, 'penempatan', 'penempatan', 'input', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(6280, 69, 'jenis_kelamin', 'jenis_kelamin', 'input', 'yes', 'yes', 'yes', 'yes', 9, '', '', ''),
(6281, 69, 'usia', 'usia', 'number', 'yes', 'yes', 'yes', 'yes', 10, '', '', ''),
(6282, 69, 'status_karyawan', 'status_karyawan', 'input', 'yes', 'yes', 'yes', 'yes', 11, '', '', ''),
(6283, 69, 'marital_status', 'marital_status', 'input', 'yes', 'yes', 'yes', 'yes', 12, '', '', ''),
(6284, 69, 'pendidikan', 'pendidikan', 'input', 'yes', 'yes', 'yes', 'yes', 13, '', '', ''),
(6285, 69, 'pengalaman', 'pengalaman', 'input', 'yes', 'yes', 'yes', 'yes', 14, '', '', ''),
(6286, 69, 'keterampilan', 'keterampilan', 'input', 'yes', 'yes', 'yes', 'yes', 15, '', '', ''),
(6409, 70, 'division_id', 'ID', 'number', 'yes', '', '', 'yes', 1, '', '', ''),
(6410, 70, 'division_name', 'division_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(6411, 70, 'division_desc', 'description', 'textarea', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6423, 59, 'department_id', 'ID', 'number', 'yes', '', '', 'yes', 1, '', '', ''),
(6424, 59, 'department_name', 'department_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(6425, 59, 'department_division_id', 'division', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'division', 'division_id', 'division_name'),
(6426, 59, 'department_desc', 'description', 'textarea', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6667, 78, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(6668, 78, 'grade_level', 'grade_level', 'number', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(6669, 78, 'grade_range_min', 'grade_range_min', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6670, 78, 'grade_range_max', 'grade_range_max', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6848, 60, 'grade_id', 'ID', 'number', 'yes', '', '', 'yes', 1, '', '', ''),
(6849, 60, 'grade_job', 'grade_job', 'select', 'yes', 'yes', 'yes', 'yes', 2, 'grade_level', 'grade_level_name', 'grade_level_name'),
(6850, 60, 'grade_range_min', 'grade_range_min', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(6851, 60, 'grade_range_max', 'grade_range_max', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(6852, 60, 'grade_rank', 'grade_rank', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(6853, 60, 'grade_title', 'grade_title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(6854, 60, 'grade_job_title', 'grade_job_title', 'input', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(6855, 60, 'grade_sort', 'grade_sort', 'input', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(7868, 80, 'grade_id', 'grade_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7869, 80, 'grade_job_name', 'grade_job_name', 'select', 'yes', 'yes', 'yes', 'yes', 2, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7870, 80, 'grade_range_min', 'range_min', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(7871, 80, 'grade_range_max', 'range_max', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7872, 80, 'grade_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7873, 80, 'grade_title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7874, 80, 'grade_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(7875, 80, 'grade_sort', 'sort', 'number', '', 'yes', 'yes', 'yes', 8, '', '', ''),
(7876, 79, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7877, 79, 'grade_level_name', 'grade_level_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7878, 79, 'grade_level_range_min', 'range_min', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(7879, 79, 'grade_level_range_max', 'range_max', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7880, 77, 'officer_id', 'officer_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7881, 77, 'officer_name', 'officer_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7882, 77, 'officer_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7883, 77, 'officer_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7884, 77, 'officer_title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7885, 77, 'officer_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7886, 77, 'supervisor_id', 'supervisor_name', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'supervisor', 'supervisor_id', 'supervisor_name'),
(7887, 76, 'supervisor_id', 'supervisor_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7888, 76, 'supervisor_name', 'supervisor_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7889, 76, 'supervisor_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7890, 76, 'supervisor_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7891, 76, 'supervisor_title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7892, 76, 'supervisor_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7893, 76, 'section_id', 'section_name', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'section', 'section_id', 'section_name'),
(7894, 75, 'section_id', 'section_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7895, 75, 'section_name', 'section_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7896, 75, 'section_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7897, 75, 'section_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7898, 75, 'section_title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7899, 75, 'section_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7900, 75, 'department_id', 'department_name', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'department', 'department_id', 'department_name'),
(7901, 74, 'department_id', 'department_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7902, 74, 'department_name', 'department_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7903, 74, 'department_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7904, 74, 'department_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7905, 74, 'department_title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7906, 74, 'department_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7907, 74, 'division_id', 'division_name', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'division', 'division_id', 'division_name'),
(7908, 73, 'division_id', 'division_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7909, 73, 'division_name', 'division_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7910, 73, 'division_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7911, 73, 'division_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7912, 73, 'division_title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7913, 73, 'division_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7914, 73, 'directorate_id', 'directorate_name', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'directorate', 'directorate_id', 'directorate_name'),
(7915, 72, 'directorate_id', 'directorate_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7916, 72, 'directorate_name', 'directorate_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7917, 72, 'directorate_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7918, 72, 'directorate_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7919, 72, 'directorate_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7920, 72, 'corporate_id', 'corporate_name', 'select', 'yes', 'yes', 'yes', 'yes', 6, 'corporate', 'corporate_id', 'corporate_name'),
(7921, 71, 'corporate_id', 'corporate_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7922, 71, 'corporate_name', 'corporate_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7923, 71, 'corporate_grade', 'grade_level', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'grade_level', 'grade_level_name', 'grade_level_name'),
(7924, 71, 'corporate_rank', 'rank', 'input', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7925, 71, 'corporate_job_title', 'job_title', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7926, 66, 'company_id', 'ID', 'number', 'yes', '', '', 'yes', 1, '', '', ''),
(7927, 66, 'company_name', 'company_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7928, 66, 'company_logo', 'logo', 'file', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(7929, 66, 'company_start_date', 'start_date', 'date', '', 'yes', 'yes', 'yes', 4, '', '', ''),
(7930, 66, 'company_vision', 'vision', 'textarea', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(7931, 66, 'company_mission', 'mission', 'textarea', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7932, 66, 'company_contact_email', 'email', 'email', 'yes', 'yes', 'yes', 'yes', 7, '', '', ''),
(7933, 66, 'company_contact_phone', 'phone', 'input', 'yes', 'yes', 'yes', 'yes', 8, '', '', ''),
(7934, 66, 'company_contact_fax', 'fax', 'input', 'yes', 'yes', 'yes', 'yes', 9, '', '', ''),
(7935, 66, 'company_type', 'company_type', 'input', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(7936, 66, 'company_business_type', 'business_type', 'input', '', 'yes', 'yes', 'yes', 11, '', '', ''),
(7937, 66, 'company_address_location', 'address', 'textarea', '', 'yes', 'yes', 'yes', 12, '', '', ''),
(7938, 66, 'company_address_city', 'city', 'input', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(7939, 66, 'company_address_zip_code', 'zip_code', 'input', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(7940, 66, 'company_address_country', 'country', 'input', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(7941, 66, 'company_tax_npkp', 'NPPKP', 'input', '', 'yes', 'yes', 'yes', 16, '', '', ''),
(7942, 66, 'company_tax_npwp', 'NPWP', 'input', '', 'yes', 'yes', 'yes', 17, '', '', ''),
(7943, 66, 'company_tax_location', 'tax_location', 'textarea', '', 'yes', 'yes', 'yes', 18, '', '', ''),
(7944, 66, 'company_bank_name', 'bank_name', 'input', '', 'yes', 'yes', 'yes', 19, '', '', ''),
(7945, 66, 'company_bank_account_no', 'bank_account_no.', 'number', '', 'yes', 'yes', 'yes', 20, '', '', ''),
(7946, 66, 'company_bank_account_name', 'bank_account_name', 'input', '', 'yes', 'yes', 'yes', 21, '', '', ''),
(7947, 66, 'company_is_holding', 'is_holding', 'true_false', '', 'yes', 'yes', 'yes', 22, '', '', ''),
(7948, 65, 'employee_id', 'employee_id', 'number', '', '', '', 'yes', 1, '', '', ''),
(7949, 65, 'employee_nik', 'NIK', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7950, 65, 'employee_name', 'employee_name', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(7951, 65, 'employee_hire_date', 'hire_date', 'date', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7952, 65, 'employee_seniority_date', 'seniority_date', 'date', '', 'yes', 'yes', 'yes', 5, '', '', ''),
(7953, 65, 'employee_grade_id', 'grade', 'input', 'yes', 'yes', 'yes', 'yes', 6, '', '', ''),
(7954, 65, 'employee_department_id', 'department_name', 'select', 'yes', 'yes', 'yes', 'yes', 7, 'department', 'department_id', 'department_name'),
(7955, 65, 'employee_position_id', 'position_name', 'select', 'yes', 'yes', 'yes', 'yes', 8, 'position', 'position_id', 'position_name'),
(7956, 65, 'employee_work_location_id', 'work_location', 'select', 'yes', 'yes', 'yes', 'yes', 9, 'work_location', 'work_location_id', 'work_location_name'),
(7957, 65, 'employee_sex', 'sex', 'custom_select', '', 'yes', 'yes', 'yes', 10, '', '', ''),
(7958, 65, 'employee_birth_place', 'birth_place', 'input', '', 'yes', 'yes', 'yes', 11, '', '', ''),
(7959, 65, 'employee_birth_date', 'birth_date', 'date', 'yes', 'yes', 'yes', 'yes', 12, '', '', ''),
(7960, 65, 'employee_marital_status', 'marital_status', 'custom_select', '', 'yes', 'yes', 'yes', 13, '', '', ''),
(7961, 65, 'employee_religion', 'religion', 'custom_select', '', 'yes', 'yes', 'yes', 14, '', '', ''),
(7962, 65, 'employee_social_id', 'KTP No.', 'input', '', 'yes', 'yes', 'yes', 15, '', '', ''),
(7963, 65, 'employee_last_education', 'last_education', 'custom_select', '', 'yes', 'yes', 'yes', 16, '', '', ''),
(7964, 65, 'employee_npwp', 'NPWP', 'input', '', 'yes', 'yes', 'yes', 17, '', '', ''),
(7965, 65, 'employee_blood_type', 'blood_type', 'custom_select', '', 'yes', 'yes', 'yes', 18, '', '', ''),
(7966, 65, 'employee_pay_group', 'pay_group', 'custom_select', '', 'yes', 'yes', 'yes', 19, '', '', ''),
(7967, 65, 'employee_cost_group', 'cost_group', 'input', '', 'yes', 'yes', 'yes', 20, '', '', ''),
(7968, 65, 'employee_tax_category', 'tax_category', 'custom_select', '', 'yes', 'yes', 'yes', 21, '', '', ''),
(7969, 65, 'employee_status', 'employee_status', 'custom_select', '', 'yes', 'yes', 'yes', 22, '', '', ''),
(7970, 65, 'employee_termination_date', 'termination_date', 'date', '', 'yes', 'yes', 'yes', 23, '', '', ''),
(7971, 65, 'employee_contact_phone', 'phone no. ', 'input', '', 'yes', 'yes', 'yes', 24, '', '', ''),
(7972, 65, 'employee_contact_fax', 'fax no.', 'input', '', 'yes', 'yes', 'yes', 25, '', '', ''),
(7973, 65, 'employee_address_1', 'address 1', 'textarea', '', 'yes', 'yes', 'yes', 26, '', '', ''),
(7974, 65, 'employee_address_2', 'address 2', 'textarea', '', 'yes', 'yes', 'yes', 27, '', '', ''),
(7975, 65, 'employee_address_rt', 'RT', 'input', '', 'yes', 'yes', 'yes', 28, '', '', ''),
(7976, 65, 'employee_address_rw', 'RW', 'input', '', 'yes', 'yes', 'yes', 29, '', '', ''),
(7977, 65, 'employee_address_city', 'city', 'input', '', 'yes', 'yes', 'yes', 30, '', '', ''),
(7978, 65, 'employee_address_zip_code', 'zip_code', 'input', '', 'yes', 'yes', 'yes', 31, '', '', ''),
(7979, 65, 'employee_address_state', 'state', 'input', '', 'yes', 'yes', 'yes', 32, '', '', ''),
(7980, 65, 'employee_bank_account_name', 'bank_account_name', 'input', '', 'yes', 'yes', 'yes', 33, '', '', ''),
(7981, 65, 'employee_bank_account_no', 'bank_account no.', 'input', '', 'yes', 'yes', 'yes', 34, '', '', ''),
(7982, 65, 'employee_bank_name', 'bank_name', 'custom_select', '', 'yes', 'yes', 'yes', 35, '', '', ''),
(7983, 61, 'position_id', 'ID', 'number', 'yes', '', '', 'yes', 1, '', '', ''),
(7984, 61, 'position_name', 'position_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7985, 61, 'position_department_id', 'department', 'select', 'yes', 'yes', 'yes', 'yes', 3, 'department', 'department_id', 'department_name'),
(7986, 61, 'position_desc', 'description', 'textarea', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(7987, 63, 'work_location_id', 'ID', 'number', 'yes', '', '', 'yes', 1, '', '', ''),
(7988, 63, 'work_location_name', 'work_location_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(7989, 63, 'work_location_desc', 'description', 'textarea', 'yes', 'yes', 'yes', 'yes', 3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `crud_field_validation`
--

CREATE TABLE IF NOT EXISTS `crud_field_validation` (
  `id` int(11) unsigned NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3560 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_field_validation`
--

INSERT INTO `crud_field_validation` (`id`, `crud_field_id`, `crud_id`, `validation_name`, `validation_value`) VALUES
(2644, 4919, 54, 'required', ''),
(2645, 4920, 54, 'required', ''),
(2660, 4963, 56, 'required', ''),
(2661, 4964, 56, 'allowed_extension', 'jpg,png'),
(2784, 5160, 57, 'required', ''),
(2785, 5161, 57, 'allowed_extension', 'jpg'),
(2813, 5407, 58, 'required', ''),
(2820, 5437, 62, 'required', ''),
(2839, 5635, 64, 'required', ''),
(2840, 5636, 64, 'required', ''),
(2923, 6185, 53, 'required', ''),
(2924, 6186, 53, 'required', ''),
(2925, 6187, 53, 'required', ''),
(2930, 6213, 67, 'required', ''),
(2931, 6214, 67, 'required', ''),
(2932, 6215, 67, 'required', ''),
(2933, 6216, 67, 'required', ''),
(2934, 6217, 67, 'required', ''),
(2935, 6219, 67, 'required', ''),
(2951, 6263, 68, 'required', ''),
(2952, 6264, 68, 'required', ''),
(2953, 6269, 68, 'required', ''),
(2954, 6270, 68, 'required', ''),
(2955, 6271, 68, 'required', ''),
(2956, 6273, 69, 'required', ''),
(2957, 6273, 69, 'max_length', '11'),
(2958, 6274, 69, 'required', ''),
(2959, 6274, 69, 'max_length', '11'),
(2960, 6275, 69, 'required', ''),
(2961, 6275, 69, 'max_length', '11'),
(2962, 6276, 69, 'required', ''),
(2963, 6276, 69, 'max_length', '20'),
(2964, 6277, 69, 'required', ''),
(2965, 6277, 69, 'max_length', '50'),
(2966, 6278, 69, 'required', ''),
(2967, 6278, 69, 'max_length', '50'),
(2968, 6279, 69, 'required', ''),
(2969, 6279, 69, 'max_length', '50'),
(2970, 6280, 69, 'required', ''),
(2971, 6280, 69, 'max_length', '10'),
(2972, 6281, 69, 'required', ''),
(2973, 6281, 69, 'max_length', '11'),
(2974, 6282, 69, 'required', ''),
(2975, 6282, 69, 'max_length', '10'),
(2976, 6283, 69, 'required', ''),
(2977, 6283, 69, 'max_length', '10'),
(2978, 6284, 69, 'required', ''),
(2979, 6284, 69, 'max_length', '255'),
(2980, 6285, 69, 'required', ''),
(2981, 6285, 69, 'max_length', '255'),
(2982, 6286, 69, 'required', ''),
(2983, 6286, 69, 'max_length', '255'),
(3018, 6410, 70, 'required', ''),
(3024, 6424, 59, 'required', ''),
(3025, 6425, 59, 'required', ''),
(3164, 6668, 78, 'required', ''),
(3239, 6849, 60, 'required', ''),
(3240, 6850, 60, 'required', ''),
(3241, 6851, 60, 'required', ''),
(3522, 7869, 80, 'required', ''),
(3523, 7870, 80, 'required', ''),
(3524, 7871, 80, 'required', ''),
(3525, 7877, 79, 'required', ''),
(3526, 7878, 79, 'required', ''),
(3527, 7879, 79, 'required', ''),
(3528, 7881, 77, 'required', ''),
(3529, 7882, 77, 'required', ''),
(3530, 7886, 77, 'required', ''),
(3531, 7888, 76, 'required', ''),
(3532, 7889, 76, 'required', ''),
(3533, 7893, 76, 'required', ''),
(3534, 7895, 75, 'required', ''),
(3535, 7896, 75, 'required', ''),
(3536, 7900, 75, 'required', ''),
(3537, 7902, 74, 'required', ''),
(3538, 7903, 74, 'required', ''),
(3539, 7907, 74, 'required', ''),
(3540, 7909, 73, 'required', ''),
(3541, 7910, 73, 'required', ''),
(3542, 7914, 73, 'required', ''),
(3543, 7916, 72, 'required', ''),
(3544, 7917, 72, 'required', ''),
(3545, 7920, 72, 'required', ''),
(3546, 7922, 71, 'required', ''),
(3547, 7923, 71, 'required', ''),
(3548, 7927, 66, 'required', ''),
(3549, 7928, 66, 'allowed_extension', 'jpg'),
(3550, 7949, 65, 'required', ''),
(3551, 7950, 65, 'required', ''),
(3552, 7951, 65, 'required', ''),
(3553, 7953, 65, 'required', ''),
(3554, 7954, 65, 'required', ''),
(3555, 7955, 65, 'required', ''),
(3556, 7956, 65, 'required', ''),
(3557, 7984, 61, 'required', ''),
(3558, 7985, 61, 'required', ''),
(3559, 7988, 63, 'required', '');

-- --------------------------------------------------------

--
-- Table structure for table `crud_input_type`
--

CREATE TABLE IF NOT EXISTS `crud_input_type` (
  `id` int(11) unsigned NOT NULL,
  `type` varchar(200) NOT NULL,
  `relation` varchar(20) NOT NULL,
  `custom_value` int(11) NOT NULL,
  `validation_group` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_input_type`
--

INSERT INTO `crud_input_type` (`id`, `type`, `relation`, `custom_value`, `validation_group`) VALUES
(1, 'input', '0', 0, 'input'),
(2, 'textarea', '0', 0, 'text'),
(3, 'select', '1', 0, 'select'),
(4, 'editor_wysiwyg', '0', 0, 'editor'),
(5, 'password', '0', 0, 'password'),
(6, 'email', '0', 0, 'email'),
(7, 'address_map', '0', 0, 'address_map'),
(8, 'file', '0', 0, 'file'),
(9, 'file_multiple', '0', 0, 'file_multiple'),
(10, 'datetime', '0', 0, 'datetime'),
(11, 'date', '0', 0, 'date'),
(12, 'timestamp', '0', 0, 'timestamp'),
(13, 'number', '0', 0, 'number'),
(14, 'yes_no', '0', 0, 'yes_no'),
(15, 'time', '0', 0, 'time'),
(16, 'year', '0', 0, 'year'),
(17, 'select_multiple', '1', 0, 'select_multiple'),
(18, 'checkboxes', '1', 0, 'checkboxes'),
(19, 'options', '1', 0, 'options'),
(20, 'true_false', '0', 0, 'true_false'),
(21, 'current_user_username', '0', 0, 'user_username'),
(22, 'current_user_id', '0', 0, 'current_user_id'),
(23, 'custom_option', '0', 1, 'custom_option'),
(24, 'custom_checkbox', '0', 1, 'custom_checkbox'),
(25, 'custom_select_multiple', '0', 1, 'custom_select_multiple'),
(26, 'custom_select', '0', 1, 'custom_select');

-- --------------------------------------------------------

--
-- Table structure for table `crud_input_validation`
--

CREATE TABLE IF NOT EXISTS `crud_input_validation` (
  `id` int(11) unsigned NOT NULL,
  `validation` varchar(200) NOT NULL,
  `input_able` varchar(20) NOT NULL,
  `group_input` text NOT NULL,
  `input_placeholder` text NOT NULL,
  `call_back` varchar(10) NOT NULL,
  `input_validation` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `crud_input_validation`
--

INSERT INTO `crud_input_validation` (`id`, `validation`, `input_able`, `group_input`, `input_placeholder`, `call_back`, `input_validation`) VALUES
(1, 'required', 'no', 'input, file, number, text, datetime, select, password, email, editor, date, yes_no, time, year, select_multiple, options, checkboxes, true_false, address_map, custom_option, custom_checkbox, custom_select_multiple, custom_select, file_multiple', '', '', ''),
(2, 'max_length', 'yes', 'input, number, text, select, password, email, editor, yes_no, time, year, select_multiple, options, checkboxes, address_map', '', '', 'numeric'),
(3, 'min_length', 'yes', 'input, number, text, select, password, email, editor, time, year, select_multiple, address_map', '', '', 'numeric'),
(4, 'valid_email', 'no', 'input, email', '', '', ''),
(5, 'valid_emails', 'no', 'input, email', '', '', ''),
(6, 'regex', 'yes', 'input, number, text, datetime, select, password, email, editor, date, yes_no, time, year, select_multiple, options, checkboxes', '', 'yes', 'callback_valid_regex'),
(7, 'decimal', 'no', 'input, number, text, select', '', '', ''),
(8, 'allowed_extension', 'yes', 'file, file_multiple', 'ex : jpg,png,..', '', 'callback_valid_extension_list'),
(9, 'max_width', 'yes', 'file, file_multiple', '', '', 'numeric'),
(10, 'max_height', 'yes', 'file, file_multiple', '', '', 'numeric'),
(11, 'max_size', 'yes', 'file, file_multiple', '... kb', '', 'numeric'),
(12, 'max_item', 'yes', 'file_multiple', '', '', 'numeric'),
(13, 'valid_url', 'no', 'input, text', '', '', ''),
(14, 'alpha', 'no', 'input, text, select, password, editor, yes_no', '', '', ''),
(15, 'alpha_numeric', 'no', 'input, number, text, select, password, editor', '', '', ''),
(16, 'alpha_numeric_spaces', 'no', 'input, number, text,select, password, editor', '', '', ''),
(17, 'valid_number', 'no', 'input, number, text, password, editor, true_false', '', 'yes', ''),
(18, 'valid_datetime', 'no', 'input, datetime, text', '', 'yes', ''),
(19, 'valid_date', 'no', 'input, datetime, date, text', '', 'yes', ''),
(20, 'valid_max_selected_option', 'yes', 'select_multiple, custom_select_multiple, custom_checkbox, checkboxes', '', 'yes', 'numeric'),
(21, 'valid_min_selected_option', 'yes', 'select_multiple, custom_select_multiple, custom_checkbox, checkboxes', '', 'yes', 'numeric'),
(22, 'valid_alpha_numeric_spaces_underscores', 'no', 'input, text,select, password, editor', '', 'yes', ''),
(23, 'matches', 'yes', 'input, number, text, password, email', 'any field', 'no', 'callback_valid_alpha_numeric_spaces_underscores'),
(24, 'valid_json', 'no', 'input, text, editor', '', 'yes', ' '),
(25, 'valid_url', 'no', 'input, text, editor', '', 'no', ' '),
(26, 'exact_length', 'yes', 'input, text, number', '0 - 99999*', 'no', 'numeric'),
(27, 'alpha_dash', 'no', 'input, text', '', 'no', ''),
(28, 'integer', 'no', 'input, text, number', '', 'no', ''),
(29, 'differs', 'yes', 'input, text, number, email, password, editor, options, select', 'any field', 'no', 'callback_valid_alpha_numeric_spaces_underscores'),
(30, 'is_natural', 'no', 'input, text, number', '', 'no', ''),
(31, 'is_natural_no_zero', 'no', 'input, text, number', '', 'no', ''),
(32, 'less_than', 'yes', 'input, text, number', '', 'no', 'numeric'),
(33, 'less_than_equal_to', 'yes', 'input, text, number', '', 'no', 'numeric'),
(34, 'greater_than', 'yes', 'input, text, number', '', 'no', 'numeric'),
(35, 'greater_than_equal_to', 'yes', 'input, text, number', '', 'no', 'numeric'),
(36, 'in_list', 'yes', 'input, text, number, select, options', '', 'no', 'callback_valid_multiple_value'),
(37, 'valid_ip', 'no', 'input, text', '', 'no', '');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(150) NOT NULL DEFAULT '',
  `department_grade` varchar(25) DEFAULT NULL,
  `department_rank` varchar(25) DEFAULT NULL,
  `department_title` varchar(50) DEFAULT NULL,
  `department_job_title` varchar(150) DEFAULT NULL,
  `division_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_name`, `department_grade`, `department_rank`, `department_title`, `department_job_title`, `division_id`) VALUES
(17, 'Warehouse', 'C', 'Senior Manager', 'Dept. Head', 'Dept. Head of Warehouse', 4),
(18, 'Logistic', 'C', 'Senior Manager', 'Dept. Head', 'Dept. Head of Logistic', 4),
(19, 'Procurement', 'C', 'Senior Manager', 'Dept. Head', 'Dept. Head of Procurement', 5);

-- --------------------------------------------------------

--
-- Table structure for table `directorate`
--

CREATE TABLE IF NOT EXISTS `directorate` (
  `directorate_id` int(11) NOT NULL,
  `directorate_name` varchar(150) NOT NULL DEFAULT '0',
  `directorate_grade` varchar(25) NOT NULL DEFAULT '0',
  `directorate_rank` varchar(25) NOT NULL DEFAULT '0',
  `directorate_job_title` varchar(150) NOT NULL DEFAULT '0',
  `corporate_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directorate`
--

INSERT INTO `directorate` (`directorate_id`, `directorate_name`, `directorate_grade`, `directorate_rank`, `directorate_job_title`, `corporate_id`) VALUES
(1, 'SCM', 'A', 'Direktur', 'Direktur', 1),
(2, 'Finance & ACC', 'A', 'Direktur', 'Direktur', 1),
(3, 'Compliance', 'A', 'Direktur', 'Direktur', 1),
(4, 'Marketing & Sales', 'A', 'DIrektur', 'Direktur', 1),
(5, 'HC - GA', 'A', 'Direktur', 'Direktur', 1),
(6, 'Internal Audit', 'A', 'Direktur', 'Direktur', 1),
(7, 'CMS', 'A', 'Direktur', 'Direktur', 1);

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE IF NOT EXISTS `division` (
  `division_id` int(11) NOT NULL,
  `division_name` varchar(150) NOT NULL DEFAULT '',
  `division_grade` varchar(25) DEFAULT NULL,
  `division_rank` varchar(25) DEFAULT NULL,
  `division_title` varchar(50) DEFAULT NULL,
  `division_job_title` varchar(150) DEFAULT NULL,
  `directorate_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`division_id`, `division_name`, `division_grade`, `division_rank`, `division_title`, `division_job_title`, `directorate_id`) VALUES
(4, 'SCM', 'B', 'Ass. Dir.', 'Div. Head', 'SCM Div. Head', 1),
(5, 'Sourcing Management', 'B', 'Ass. Dir.', 'Div. Head', 'Sourcing Management Div. Head', 1),
(6, 'Finance & ACC', 'B', 'Ass. Dir.', 'Div. Head', 'Finance & ACC div. Head', 2),
(7, 'Legal', 'B', 'Ass. Dir.', 'Div. Head', 'Legal Div. Head', 2),
(8, 'IT', 'B', 'Ass. Dir.', 'Div. Head', 'IT Div. Head', 3);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_id` int(11) NOT NULL,
  `employee_nik` varchar(10) NOT NULL DEFAULT '',
  `employee_name` varchar(100) NOT NULL DEFAULT '',
  `employee_hire_date` date DEFAULT NULL,
  `employee_seniority_date` date DEFAULT NULL,
  `employee_department_id` int(11) DEFAULT NULL,
  `employee_grade_id` int(11) DEFAULT NULL,
  `employee_position_id` int(11) DEFAULT NULL,
  `employee_work_location_id` int(11) DEFAULT NULL,
  `employee_sex` varchar(20) DEFAULT NULL,
  `employee_birth_place` varchar(50) DEFAULT NULL,
  `employee_birth_date` date DEFAULT NULL,
  `employee_marital_status` varchar(10) DEFAULT NULL,
  `employee_religion` varchar(20) DEFAULT NULL,
  `employee_social_id` varchar(20) DEFAULT NULL,
  `employee_last_education` varchar(15) DEFAULT NULL,
  `employee_npwp` varchar(15) DEFAULT NULL,
  `employee_blood_type` varchar(10) DEFAULT NULL,
  `employee_pay_group` varchar(50) DEFAULT NULL,
  `employee_cost_group` varchar(50) DEFAULT NULL,
  `employee_tax_category` varchar(50) DEFAULT NULL,
  `employee_status` varchar(50) DEFAULT NULL,
  `employee_termination_date` date DEFAULT NULL,
  `employee_contact_phone` varchar(15) DEFAULT NULL,
  `employee_contact_fax` varchar(15) DEFAULT NULL,
  `employee_address_1` varchar(255) DEFAULT NULL,
  `employee_address_2` varchar(255) DEFAULT NULL,
  `employee_address_rt` varchar(3) DEFAULT NULL,
  `employee_address_rw` varchar(3) DEFAULT NULL,
  `employee_address_city` varchar(50) DEFAULT NULL,
  `employee_address_zip_code` varchar(5) DEFAULT NULL,
  `employee_address_state` varchar(50) DEFAULT NULL,
  `employee_bank_account_name` varchar(50) DEFAULT NULL,
  `employee_bank_account_no` varchar(15) DEFAULT NULL,
  `employee_bank_name` varchar(15) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `employee_nik`, `employee_name`, `employee_hire_date`, `employee_seniority_date`, `employee_department_id`, `employee_grade_id`, `employee_position_id`, `employee_work_location_id`, `employee_sex`, `employee_birth_place`, `employee_birth_date`, `employee_marital_status`, `employee_religion`, `employee_social_id`, `employee_last_education`, `employee_npwp`, `employee_blood_type`, `employee_pay_group`, `employee_cost_group`, `employee_tax_category`, `employee_status`, `employee_termination_date`, `employee_contact_phone`, `employee_contact_fax`, `employee_address_1`, `employee_address_2`, `employee_address_rt`, `employee_address_rw`, `employee_address_city`, `employee_address_zip_code`, `employee_address_state`, `employee_bank_account_name`, `employee_bank_account_no`, `employee_bank_name`) VALUES
(1, '9117270', 'JESSIE', '2019-06-02', '2025-12-13', 19, 4, 4, 3, 'Perempuan', 'Jakarta', '1991-12-08', 'S-0', 'Budha', '3172014812910001', 'Strata 1', '', 'O', 'Salary Code 1', '', 'Peg. Tetap', 'Permanent', '2022-06-12', '', '', 'JL. PLUIT MAS VI E NO. 15A\r\nJL. PLUIT MAS VI E NO. 15A\r\nJL. PLUIT MAS VI E NO. 15A\r\nJL. PLUIT MAS VI E NO. 15A\r\nJL. PLUIT MAS VI E NO. 15A', 'PEJAGALAN, PENJARINGAN\r\nPEJAGALAN, PENJARINGAN\r\nPEJAGALAN, PENJARINGAN\r\nPEJAGALAN, PENJARINGAN\r\nPEJAGALAN, PENJARINGAN\r\n', '002', '018', 'JAKARTA UTARA', '14440', 'DKI JAKARTA', 'JESSIE', '704507119800', 'BCA');

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `title`, `subject`, `table_name`) VALUES
(1, 'Employees', 'Employees', 'form_employees');

-- --------------------------------------------------------

--
-- Table structure for table `form_custom_attribute`
--

CREATE TABLE IF NOT EXISTS `form_custom_attribute` (
  `id` int(11) unsigned NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_value` text NOT NULL,
  `attribute_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_custom_option`
--

CREATE TABLE IF NOT EXISTS `form_custom_option` (
  `id` int(11) unsigned NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `form_employees`
--

CREATE TABLE IF NOT EXISTS `form_employees` (
  `id` int(11) unsigned NOT NULL,
  `nik` varchar(225) NOT NULL,
  `departemen` varchar(225) NOT NULL,
  `grade` varchar(225) NOT NULL,
  `address` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_employees`
--

INSERT INTO `form_employees` (`id`, `nik`, `departemen`, `grade`, `address`) VALUES
(1, '123', '12', '4', 'Rawamangun, Pulo Gadung, East Jakarta City, Jakarta, Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `form_field`
--

CREATE TABLE IF NOT EXISTS `form_field` (
  `id` int(11) unsigned NOT NULL,
  `form_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `input_type` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `placeholder` text,
  `auto_generate_help_block` varchar(10) DEFAULT NULL,
  `help_block` text,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_field`
--

INSERT INTO `form_field` (`id`, `form_id`, `sort`, `field_name`, `input_type`, `field_label`, `placeholder`, `auto_generate_help_block`, `help_block`, `relation_table`, `relation_value`, `relation_label`) VALUES
(1, 1, 2, 'h1', 'heading', 'Employee List', '', '', '', '', '', ''),
(2, 1, 3, 'nik', 'input', 'NIK', '', 'yes', '', '', '', ''),
(3, 1, 4, 'departemen', 'select', 'Departemen', '', 'yes', '', 'department', 'department_id', 'department_name'),
(4, 1, 5, 'grade', 'select', 'Grade', '', 'yes', '', 'grade', 'grade_id', 'grade_name'),
(5, 1, 6, 'address', 'address_map', 'Address', '', 'yes', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `form_field_validation`
--

CREATE TABLE IF NOT EXISTS `form_field_validation` (
  `id` int(11) unsigned NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_field_validation`
--

INSERT INTO `form_field_validation` (`id`, `form_field_id`, `form_id`, `validation_name`, `validation_value`) VALUES
(1, 2, 1, 'required', ''),
(2, 3, 1, 'required', ''),
(3, 4, 1, 'required', '');

-- --------------------------------------------------------

--
-- Table structure for table `fptk`
--

CREATE TABLE IF NOT EXISTS `fptk` (
  `id` int(11) NOT NULL,
  `pemohon_employee_id` int(11) NOT NULL,
  `pengajuan_jabatan` int(11) NOT NULL,
  `pengajuan_level` int(11) NOT NULL,
  `pengajuan_departemen` int(11) NOT NULL,
  `pengajuan_total_jumlah` int(11) NOT NULL,
  `additional_jumlah` int(11) DEFAULT NULL,
  `additional_alasan` varchar(255) DEFAULT '',
  `replacement_jumlah` int(11) DEFAULT NULL,
  `replacement_employee_id` int(11) DEFAULT NULL,
  `replacement_alasan` varchar(255) NOT NULL DEFAULT '',
  `pengajuan_proses_pemenuhan` varchar(50) NOT NULL DEFAULT '',
  `pengajuan_proses_pemenuhan_jumlah` int(11) NOT NULL,
  `jabatan_tugas_umum` varchar(255) NOT NULL DEFAULT '',
  `kualifikasi_usia` int(11) DEFAULT NULL,
  `kualifikasi_jenis_kelamin` varchar(20) DEFAULT NULL,
  `kualifikasi_marital_status` varchar(20) DEFAULT NULL,
  `kualifikasi_pengalaman` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE IF NOT EXISTS `grade` (
  `grade_id` int(11) NOT NULL,
  `grade_job` varchar(25) NOT NULL,
  `grade_range_min` decimal(3,1) NOT NULL,
  `grade_range_max` decimal(3,1) NOT NULL,
  `grade_rank` varchar(25) NOT NULL,
  `grade_title` varchar(50) NOT NULL,
  `grade_job_title` varchar(150) NOT NULL,
  `grade_sort` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`grade_id`, `grade_job`, `grade_range_min`, `grade_range_max`, `grade_rank`, `grade_title`, `grade_job_title`, `grade_sort`) VALUES
(1, '7', 1.0, 0.0, 'Non Staff', 'Support Staff', '', 19),
(2, '7', 2.0, 0.0, 'Non Staff', 'Support Staff', '', 18),
(3, '7', 3.0, 0.0, 'Non Staff', 'Support Staff', '', 17),
(4, '6', 4.0, 0.0, 'Staff', 'Officer', '', 16),
(5, '6', 5.0, 0.0, 'Staff', 'Officer', '', 15),
(6, '6', 6.0, 0.0, 'Staff', 'Officer', '', 14),
(7, '5', 7.0, 0.0, 'Supervisor', 'Supervisor', 'KAS, FAS, HC SPV, FINANCE SPV, ACC SPV,GA SPV, EDP SPV, KASIR, LEGAL, DLL', 13),
(8, '5', 8.0, 0.0, 'Supervisor', 'Supervisor', 'KAS, FAS, HC SPV, FINANCE SPV, ACC SPV,GA SPV, EDP SPV, KASIR, LEGAL, DLL', 12),
(9, '5', 9.0, 0.0, 'Supervisor', 'Supervisor', 'KAS, FAS, HC SPV, FINANCE SPV, ACC SPV,GA SPV, EDP SPV, KASIR, LEGAL, DLL', 11),
(10, '4', 10.0, 0.0, 'Manager', 'Section Head', 'ASM', 10),
(11, '4', 11.0, 0.0, 'Manager', 'Section Head', 'ASM', 9),
(12, '4', 12.0, 0.0, 'Manager', 'Section Head', 'ASM', 8),
(13, '3', 13.0, 0.0, 'Senior Manager', 'Associate Department Head', 'Acting Manager', 7),
(14, '3', 14.0, 0.0, 'Senior Manager', 'Deputy Department Head', 'KAM', 6),
(15, '3', 15.0, 0.0, 'Senior Manager', 'Department Head', 'RSM, HC Mgr, FA Mgr, BI Mgr, MKT Mgr, SCM Mgr ', 5),
(16, '2', 16.0, 0.0, 'Assistant Director', 'Associate Division Head', 'GRSM', 4),
(17, '2', 17.0, 0.0, 'Assistant Director', 'Deputy Division Head', 'NSM / NKAM / Dep GM', 3),
(18, '2', 18.0, 0.0, 'Assistant Director', 'Division Head', 'GM', 2),
(19, '1', 19.0, 0.0, 'Director', 'Company', 'Director', 1),
(20, 'A', 20.0, 20.0, 'President Director', 'Company', 'President Director', 0);

-- --------------------------------------------------------

--
-- Table structure for table `grade_job`
--

CREATE TABLE IF NOT EXISTS `grade_job` (
  `grade_id` int(11) NOT NULL,
  `grade_job_name` varchar(25) NOT NULL,
  `grade_range_min` decimal(3,1) NOT NULL,
  `grade_range_max` decimal(3,1) NOT NULL,
  `grade_rank` varchar(25) NOT NULL,
  `grade_title` varchar(50) NOT NULL,
  `grade_job_title` varchar(150) NOT NULL,
  `grade_sort` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_job`
--

INSERT INTO `grade_job` (`grade_id`, `grade_job_name`, `grade_range_min`, `grade_range_max`, `grade_rank`, `grade_title`, `grade_job_title`, `grade_sort`) VALUES
(1, 'G', 1.0, 1.3, 'Non Staff', 'Support Staff', '', 19),
(2, 'G', 2.0, 2.3, 'Non Staff', 'Support Staff', '', 18),
(3, 'G', 3.0, 3.3, 'Non Staff', 'Support Staff', '', 17),
(4, 'F', 4.0, 4.3, 'Staff', 'Officer', '', 16),
(5, 'F', 5.0, 5.3, 'Staff', 'Officer', '', 15),
(6, 'F', 6.0, 6.3, 'Staff', 'Officer', '', 14),
(7, 'E', 7.0, 7.3, 'Supervisor', 'Supervisor', 'KAS, FAS, HC SPV, FINANCE SPV, ACC SPV,GA SPV, EDP SPV, KASIR, LEGAL, DLL', 13),
(8, 'E', 8.0, 8.3, 'Supervisor', 'Supervisor', 'KAS, FAS, HC SPV, FINANCE SPV, ACC SPV,GA SPV, EDP SPV, KASIR, LEGAL, DLL', 12),
(9, 'E', 9.0, 9.3, 'Supervisor', 'Supervisor', 'KAS, FAS, HC SPV, FINANCE SPV, ACC SPV,GA SPV, EDP SPV, KASIR, LEGAL, DLL', 11),
(10, 'D', 10.0, 10.3, 'Manager', 'Section Head', 'ASM', 10),
(11, 'D', 11.0, 11.3, 'Manager', 'Section Head', 'ASM', 9),
(12, 'D', 12.0, 12.3, 'Manager', 'Section Head', 'ASM', 8),
(13, 'C', 13.0, 13.3, 'Senior Manager', 'Associate Department Head', 'Acting Manager', 7),
(14, 'C', 14.0, 14.3, 'Senior Manager', 'Deputy Department Head', 'KAM', 6),
(15, 'C', 15.0, 15.3, 'Senior Manager', 'Department Head', 'RSM, HC Mgr, FA Mgr, BI Mgr, MKT Mgr, SCM Mgr ', 5),
(16, 'B', 16.0, 16.3, 'Assistant Director', 'Associate Division Head', 'GRSM', 4),
(17, 'B', 17.0, 17.3, 'Assistant Director', 'Deputy Division Head', 'NSM / NKAM / Dep GM', 3),
(18, 'B', 18.0, 18.3, 'Assistant Director', 'Division Head', 'GM', 2),
(19, 'A', 19.0, 19.3, 'Director', 'Company', 'Director', 1),
(20, 'A', 20.0, 20.0, 'President Director', 'Company', 'President Director', 0);

-- --------------------------------------------------------

--
-- Table structure for table `grade_level`
--

CREATE TABLE IF NOT EXISTS `grade_level` (
  `id` int(11) NOT NULL,
  `grade_level_name` varchar(11) NOT NULL,
  `grade_level_range_min` decimal(3,1) NOT NULL,
  `grade_level_range_max` decimal(3,1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_level`
--

INSERT INTO `grade_level` (`id`, `grade_level_name`, `grade_level_range_min`, `grade_level_range_max`) VALUES
(1, 'A', 19.0, 20.0),
(2, 'B', 16.0, 18.0),
(3, 'C', 13.0, 15.0),
(4, 'D', 10.0, 12.0),
(5, 'E', 7.0, 9.0),
(6, 'F', 4.0, 6.0),
(7, 'G', 1.0, 3.0);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_training`
--

CREATE TABLE IF NOT EXISTS `jadwal_training` (
  `id` int(11) NOT NULL,
  `nama_training` varchar(150) NOT NULL,
  `tanggal_mulai_training` date NOT NULL,
  `tanggal_evaluasi` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `url_evaluasi_tertulis` varchar(50) NOT NULL,
  `nama_trainer` varchar(50) NOT NULL,
  `penyelenggara` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_training`
--

INSERT INTO `jadwal_training` (`id`, `nama_training`, `tanggal_mulai_training`, `tanggal_evaluasi`, `status`, `url_evaluasi_tertulis`, `nama_trainer`, `penyelenggara`) VALUES
(1, 'Training A', '2018-10-10', '2018-10-10', 'Lulus', 'http://google.com', 'Budi Santoso', 'PPM');

-- --------------------------------------------------------

--
-- Table structure for table `jobtitle`
--

CREATE TABLE IF NOT EXISTS `jobtitle` (
  `jobtitle_id` int(11) NOT NULL,
  `jobtitle_name` varchar(50) NOT NULL DEFAULT '',
  `jobtitle_desc` varchar(150) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE IF NOT EXISTS `keys` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL,
  `is_private_key` tinyint(1) NOT NULL,
  `ip_addresses` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, 'A1336F70C2BEF1D3C325F228A06B4447', 0, 0, 0, NULL, '2018-09-21 09:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `materi_training`
--

CREATE TABLE IF NOT EXISTS `materi_training` (
  `id` int(11) NOT NULL,
  `materi_training` varchar(150) NOT NULL,
  `file_training` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi_training`
--

INSERT INTO `materi_training` (`id`, `materi_training`, `file_training`) VALUES
(1, 'Program Sales', '20181002121137-2018-10-02materi_training121115.pdf,20181002121137-2018-10-02materi_training121115.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) unsigned NOT NULL,
  `label` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `icon_color` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `menu_type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `label`, `type`, `icon_color`, `link`, `sort`, `parent`, `icon`, `menu_type_id`, `active`) VALUES
(2, 'Dashboard', 'menu', 'text-blue', 'administrator/dashboard', 1, 0, 'fa-dashboard', 1, 1),
(3, 'CRUD', 'menu', 'default', 'administrator/crud', 54, 39, 'fa-table', 1, 1),
(4, 'Rest API', 'menu', 'default', 'administrator/rest', 55, 39, 'fa-code', 1, 1),
(5, 'Kelola Berita', 'menu', 'default', '#', 47, 66, 'fa-file-text-o', 1, 1),
(8, 'Menu', 'menu', 'default', 'administrator/menu', 53, 39, 'fa-bars', 1, 1),
(10, 'Pengguna', 'menu', 'default', 'administrator/user', 44, 25, 'fa-user', 1, 1),
(11, 'Grup', 'menu', 'default', 'administrator/group', 43, 25, 'fa-users', 1, 1),
(12, 'Hak Akses', 'menu', 'default', 'administrator/access', 45, 25, 'fa-check-square-o', 1, 1),
(13, 'Permisi', 'menu', 'default', 'administrator/permission', 46, 25, 'fa-shield', 1, 1),
(14, 'API Key', 'menu', 'default', 'administrator/keys', 58, 40, '', 1, 1),
(15, 'Ekstensi', 'menu', 'default', 'administrator/extension', 59, 40, 'fa-puzzle-piece', 1, 1),
(17, 'Umum', 'menu', 'default', 'administrator/setting', 57, 40, 'fa-circle-o', 1, 1),
(20, 'Home', 'menu', '', '/', 1, 0, '', 2, 1),
(23, 'Employee Self Service', 'menu', 'text-blue', 'administrator/#', 2, 0, 'fa-child', 1, 1),
(24, 'Superadmin', 'menu', 'default', '#', 51, 66, 'fa-gears', 1, 1),
(25, 'Kelola Pengguna', 'menu', 'default', '#', 42, 66, 'fa-shield', 1, 1),
(33, 'Berita', 'menu', 'default', 'administrator/berita', 49, 5, '', 1, 1),
(34, 'Kategori', 'menu', 'default', 'administrator/kategori_berita', 48, 5, '', 1, 1),
(35, 'Kelola Halaman', 'menu', 'default', 'administrator/page', 50, 66, 'fa-file-o', 1, 1),
(38, 'Backup Data', 'menu', 'default', 'administrator/backup', 60, 24, 'fa-database', 1, 1),
(39, 'Builder', 'menu', 'default', '#', 52, 24, '', 1, 1),
(40, 'Pengaturan', 'menu', 'default', '#', 56, 24, '', 1, 1),
(45, 'Organisation Dev', 'menu', 'text-blue', '#', 28, 0, 'fa-university', 1, 1),
(46, 'Recruitment', 'menu', 'text-blue', '#', 30, 0, 'fa-user-plus', 1, 1),
(47, 'Training and Dev', 'menu', 'text-blue', '#', 33, 0, 'fa-graduation-cap', 1, 1),
(48, 'Pengajuan FPTK', 'menu', 'default', 'administrator/pengajuan_fptk', 31, 46, '', 1, 1),
(49, 'Proses Recruitment', 'menu', 'default', '#', 32, 46, '', 1, 1),
(62, 'Materi Training', 'menu', 'default', 'administrator/materi_training', 35, 47, '', 1, 1),
(63, 'Jadwal Training', 'menu', 'default', 'administrator/jadwal_training', 34, 47, '', 1, 1),
(65, 'Personal Admin', 'menu', 'text-blue', '#', 37, 0, 'fa-laptop', 1, 1),
(66, 'Administrator', 'menu', 'text-blue', '#', 41, 0, 'fa-shield', 1, 1),
(67, 'Check In/Out', 'menu', 'default', '#', 8, 73, '', 1, 1),
(68, 'Leave', 'menu', 'default', '#', 9, 73, '', 1, 1),
(69, 'Permit', 'menu', 'default', '#', 10, 73, '', 1, 1),
(70, 'Sick', 'menu', 'default', '#', 11, 73, '', 1, 1),
(71, 'Overtime', 'menu', 'default', '#', 13, 73, '', 1, 1),
(72, 'My HR', 'menu', 'default', '#', 3, 23, '', 1, 1),
(73, 'My Request', 'menu', 'default', '#', 7, 23, '', 1, 1),
(74, 'Education', 'menu', 'default', '#', 4, 72, '', 1, 1),
(75, 'Family', 'menu', 'default', '#', 6, 72, '', 1, 1),
(76, 'Training', 'menu', 'default', '#', 5, 72, '', 1, 1),
(78, 'Performance (PMS)', 'menu', 'default', '#', 29, 45, '', 1, 1),
(79, 'Pengajuan Training', 'menu', 'default', 'administrator/pengajuan_training', 36, 47, '', 1, 1),
(80, 'PAN', 'menu', 'default', 'administrator/pan', 38, 65, '', 1, 1),
(81, 'Evaluasi Kerja', 'menu', 'default', '#', 39, 65, '', 1, 1),
(82, 'Exit Interview', 'menu', 'default', '#', 40, 65, '', 1, 1),
(83, 'Work Structure', 'menu', 'text-blue', '#', 14, 0, 'fa-sitemap', 1, 1),
(84, 'Company', 'menu', 'default', 'administrator/company', 15, 83, '', 1, 1),
(85, 'Grade Job', 'menu', 'default', 'administrator/grade_job', 17, 83, '', 1, 1),
(86, 'Employment', 'menu', 'text-blue', '#', 26, 0, 'fa-users', 1, 1),
(87, 'Report', 'menu', 'text-blue', '#', 61, 0, 'fa-bar-chart', 1, 1),
(88, 'Employee', 'menu', 'default', 'administrator/employee', 27, 86, '', 1, 1),
(92, 'Work Location', 'menu', 'default', 'administrator/work_location', 25, 83, '', 1, 1),
(94, 'Corporate', 'menu', 'default', 'administrator/corporate', 18, 83, 'fa-briefcase', 1, 1),
(95, 'Directorate', 'menu', 'default', 'administrator/directorate', 19, 83, '', 1, 1),
(96, 'Division', 'menu', 'default', 'administrator/division', 20, 83, '', 1, 1),
(97, 'Department', 'menu', 'default', 'administrator/department', 21, 83, '', 1, 1),
(98, 'Section', 'menu', 'default', 'administrator/section', 22, 83, '', 1, 1),
(99, 'Supervisor', 'menu', 'default', 'administrator/supervisor', 23, 83, '', 1, 1),
(100, 'Officer', 'menu', 'default', 'administrator/officer', 24, 83, '', 1, 1),
(101, 'Grade Level', 'menu', 'default', 'administrator/grade_level', 16, 83, '', 1, 1),
(102, 'Late', 'menu', 'default', '#', 12, 73, '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_type`
--

CREATE TABLE IF NOT EXISTS `menu_type` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(200) NOT NULL,
  `definition` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_type`
--

INSERT INTO `menu_type` (`id`, `name`, `definition`) VALUES
(1, 'side menu', NULL),
(2, 'top menu', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `officer`
--

CREATE TABLE IF NOT EXISTS `officer` (
  `officer_id` int(11) NOT NULL,
  `officer_name` varchar(150) NOT NULL DEFAULT '0',
  `officer_grade` varchar(25) NOT NULL DEFAULT '0',
  `officer_rank` varchar(25) NOT NULL DEFAULT '0',
  `officer_title` varchar(50) NOT NULL DEFAULT '0',
  `officer_job_title` varchar(150) NOT NULL DEFAULT '0',
  `supervisor_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officer`
--

INSERT INTO `officer` (`officer_id`, `officer_name`, `officer_grade`, `officer_rank`, `officer_title`, `officer_job_title`, `supervisor_id`) VALUES
(1, 'RMW', 'F', 'Officer', 'Coordinator', 'Coordinator of RMW', 1),
(2, 'RMW', 'F', 'Officer', 'Staff', 'Staff RMW', 1),
(3, 'FGW / DC', 'F', 'Officer', 'Coordinator', 'Coordinator of FGW', 2),
(4, 'FGW / DC', 'F', 'Officer', 'Staff', 'Staff FGW', 2);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `fresh_content` text NOT NULL,
  `keyword` text,
  `description` text,
  `link` varchar(200) DEFAULT NULL,
  `template` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_block_element`
--

CREATE TABLE IF NOT EXISTS `page_block_element` (
  `id` int(11) unsigned NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image_preview` varchar(200) NOT NULL,
  `block_name` varchar(200) NOT NULL,
  `content_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pan`
--

CREATE TABLE IF NOT EXISTS `pan` (
  `id` int(11) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL DEFAULT '',
  `jenis_usulan` varchar(50) NOT NULL DEFAULT '',
  `tgl_berlaku_mulai` date DEFAULT NULL,
  `tgl_berlaku_selesai` date DEFAULT NULL,
  `perubahan_dari` varchar(50) DEFAULT NULL,
  `perubahan_menjadi` varchar(50) DEFAULT '',
  `dasar_usulan` varchar(255) DEFAULT NULL,
  `lampiran_pendukung` varchar(255) DEFAULT NULL,
  `pencapaian_KPI` varchar(255) DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pan`
--

INSERT INTO `pan` (`id`, `nama_karyawan`, `jenis_usulan`, `tgl_berlaku_mulai`, `tgl_berlaku_selesai`, `perubahan_dari`, `perubahan_menjadi`, `dasar_usulan`, `lampiran_pendukung`, `pencapaian_KPI`) VALUES
(1, '1', 'Mutasi', '2018-10-29', '2018-10-31', 'Grade 4', 'Grade 5', 'Hasil Kerja memuaskan', 'Hasil Assessment,Lampiran KPI,Hasil Penilaian Tahunan', '-');

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_fptk`
--

CREATE TABLE IF NOT EXISTS `pengajuan_fptk` (
  `id` int(11) NOT NULL,
  `requester_id` int(11) NOT NULL,
  `jabatan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status_dibutuhkan` varchar(20) NOT NULL,
  `atasan_langsung` varchar(50) NOT NULL,
  `alasan_permohonan` varchar(50) NOT NULL,
  `penempatan` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `usia` int(11) NOT NULL,
  `status_karyawan` varchar(10) NOT NULL,
  `marital_status` varchar(10) NOT NULL,
  `pendidikan` varchar(255) NOT NULL,
  `pengalaman` varchar(255) NOT NULL,
  `keterampilan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_training`
--

CREATE TABLE IF NOT EXISTS `pengajuan_training` (
  `id` int(11) NOT NULL,
  `nama_pemohon` varchar(50) DEFAULT NULL,
  `calon_peserta` varchar(255) NOT NULL DEFAULT '',
  `jenis_training` varchar(50) DEFAULT NULL,
  `pengadaan_training` varchar(50) DEFAULT NULL,
  `nama_training` varchar(150) NOT NULL DEFAULT '',
  `deskripsi_training` varchar(255) DEFAULT '',
  `tujuan_training` varchar(50) DEFAULT '',
  `alasan_pengajuan` varchar(255) DEFAULT '',
  `nama_trainer` varchar(50) DEFAULT '',
  `lokasi_training` varchar(150) DEFAULT NULL,
  `lembaga_pelaksana` varchar(150) DEFAULT '',
  `tgl_mulai_training` datetime DEFAULT NULL,
  `tgl_selesai_training` datetime DEFAULT NULL,
  `lama_training` varchar(20) DEFAULT NULL,
  `jumlah_investasi` decimal(11,0) DEFAULT NULL,
  `jenis_pembayaran` varchar(20) DEFAULT NULL,
  `harapan_training` varchar(255) DEFAULT NULL,
  `rencana_implementasi` varchar(255) DEFAULT NULL,
  `sharing_knowledge` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajuan_training`
--

INSERT INTO `pengajuan_training` (`id`, `nama_pemohon`, `calon_peserta`, `jenis_training`, `pengadaan_training`, `nama_training`, `deskripsi_training`, `tujuan_training`, `alasan_pengajuan`, `nama_trainer`, `lokasi_training`, `lembaga_pelaksana`, `tgl_mulai_training`, `tgl_selesai_training`, `lama_training`, `jumlah_investasi`, `jenis_pembayaran`, `harapan_training`, `rencana_implementasi`, `sharing_knowledge`) VALUES
(1, '1', '1', 'Soft Skill Training', 'Training Eksternal', 'Project Management', 'Training Project Management', 'Perbaikan Kinerja', 'Untuk memperbaiki kinerja para Project Manager', 'Mario Teguh', 'Kantor', 'Mario Teguh & Friends', '2018-10-10 08:00:00', '2018-10-10 15:00:00', 'Full Day', 3000000, 'Transfer', '-', '-', 'Bersedia');

-- --------------------------------------------------------

--
-- Table structure for table `permohonan_cuti`
--

CREATE TABLE IF NOT EXISTS `permohonan_cuti` (
  `id` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `pengganti` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permohonan_cuti`
--

INSERT INTO `permohonan_cuti` (`id`, `tanggal_mulai`, `tanggal_selesai`, `alasan`, `pengganti`, `status`, `userid`) VALUES
(1, '2018-10-02', '2018-10-02', 'Cuti Lebaran', 3, 'Requested', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permohonan_izin`
--

CREATE TABLE IF NOT EXISTS `permohonan_izin` (
  `id` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `pengganti` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permohonan_izin`
--

INSERT INTO `permohonan_izin` (`id`, `tanggal_mulai`, `tanggal_selesai`, `alasan`, `pengganti`, `status`, `userid`) VALUES
(1, '2018-10-16', '2018-10-10', 'scsdc', 3, 'Requested', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permohonan_sakit`
--

CREATE TABLE IF NOT EXISTS `permohonan_sakit` (
  `id` int(11) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `pengganti` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `position_id` int(11) NOT NULL,
  `position_name` varchar(50) NOT NULL DEFAULT '',
  `position_desc` varchar(150) DEFAULT '',
  `position_department_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `position_name`, `position_desc`, `position_department_id`) VALUES
(1, 'HC  Manager', '-', 12),
(2, 'Legal & IR Supervisor', '-', 12),
(3, 'General Affair Supervisor', '-', 12),
(4, 'HRD', '-', 12),
(5, 'Operator', '-', 12),
(6, 'Receptionist', '-', 12),
(7, 'Driver', '-', 12),
(8, 'OB / OG', '-', 12),
(9, 'Humas', '-', 12),
(10, 'GA Staff', '-', 12),
(11, 'OD Supervisor & Recruitment', '-', 12),
(12, 'OD &Training & Learning SPV', '-', 12),
(13, 'IT & Project Manager', '-', 13),
(14, 'IT Supervisor', '-', 13),
(15, 'IT Infrastuktur Staff', '-', 13),
(16, 'IT HelpDesk  Staff', '-', 13),
(17, 'IT Aplication Staff', '-', 13);

-- --------------------------------------------------------

--
-- Table structure for table `rest`
--

CREATE TABLE IF NOT EXISTS `rest` (
  `id` int(11) unsigned NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `primary_key` varchar(200) NOT NULL,
  `x_api_key` varchar(20) DEFAULT NULL,
  `x_token` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest`
--

INSERT INTO `rest` (`id`, `subject`, `table_name`, `primary_key`, `x_api_key`, `x_token`) VALUES
(1, 'Naskah Nusantara', 'naskah_nusantara', 'id', 'no', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `rest_field`
--

CREATE TABLE IF NOT EXISTS `rest_field` (
  `id` int(11) unsigned NOT NULL,
  `rest_id` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `input_type` varchar(200) NOT NULL,
  `show_column` varchar(10) DEFAULT NULL,
  `show_add_api` varchar(10) DEFAULT NULL,
  `show_update_api` varchar(10) DEFAULT NULL,
  `show_detail_api` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest_field`
--

INSERT INTO `rest_field` (`id`, `rest_id`, `field_name`, `field_label`, `input_type`, `show_column`, `show_add_api`, `show_update_api`, `show_detail_api`) VALUES
(1, 1, 'id', NULL, 'input', 'yes', '', '', 'yes'),
(2, 1, 'jenis_bahan', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(3, 1, 'judul', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(4, 1, 'pengarang', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(5, 1, 'deskripsi_fisik', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(6, 1, 'informasi_teknis', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(7, 1, 'subjek', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(8, 1, 'abstrak', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(9, 1, 'catatan', NULL, 'input', 'yes', 'yes', 'yes', 'yes'),
(10, 1, 'file_cover', NULL, 'input', 'yes', 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `rest_field_validation`
--

CREATE TABLE IF NOT EXISTS `rest_field_validation` (
  `id` int(11) unsigned NOT NULL,
  `rest_field_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest_field_validation`
--

INSERT INTO `rest_field_validation` (`id`, `rest_field_id`, `rest_id`, `validation_name`, `validation_value`) VALUES
(1, 2, 1, 'required', ''),
(2, 2, 1, 'max_length', '20'),
(3, 3, 1, 'required', ''),
(4, 3, 1, 'max_length', '100'),
(5, 4, 1, 'required', ''),
(6, 4, 1, 'max_length', '50'),
(7, 5, 1, 'required', ''),
(8, 5, 1, 'max_length', '255'),
(9, 6, 1, 'required', ''),
(10, 6, 1, 'max_length', '255'),
(11, 7, 1, 'required', ''),
(12, 7, 1, 'max_length', '20'),
(13, 8, 1, 'required', ''),
(14, 8, 1, 'max_length', '150'),
(15, 9, 1, 'required', ''),
(16, 9, 1, 'max_length', '255'),
(17, 10, 1, 'required', ''),
(18, 10, 1, 'max_length', '150');

-- --------------------------------------------------------

--
-- Table structure for table `rest_input_type`
--

CREATE TABLE IF NOT EXISTS `rest_input_type` (
  `id` int(11) unsigned NOT NULL,
  `type` varchar(200) NOT NULL,
  `relation` varchar(20) NOT NULL,
  `validation_group` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rest_input_type`
--

INSERT INTO `rest_input_type` (`id`, `type`, `relation`, `validation_group`) VALUES
(1, 'input', '0', 'input'),
(2, 'timestamp', '0', 'timestamp'),
(3, 'file', '0', 'file');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(150) NOT NULL DEFAULT '0',
  `section_grade` varchar(25) NOT NULL DEFAULT '0',
  `section_rank` varchar(25) NOT NULL DEFAULT '0',
  `section_title` varchar(50) NOT NULL DEFAULT '0',
  `section_job_title` varchar(150) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`section_id`, `section_name`, `section_grade`, `section_rank`, `section_title`, `section_job_title`, `department_id`) VALUES
(1, 'RMW', 'D', 'Junior Manager', 'Section Head', 'Section Head of RMW', 17),
(2, 'FGW / DC', 'D', 'Junior Manager', 'Section Head', 'Section Head of FGW', 17),
(3, 'PPIC', 'D', 'Junior Manager', 'Section Head', 'Section Head of PPIC', 17),
(4, 'SCM Plant', 'D', 'Junior Manager', 'Section Head', 'Section Head of SCM Plant', 18),
(5, 'RMPM', 'D', 'Junior Manager', 'Section Head', 'Section Head of RMPM', 19),
(6, 'ATK / SPAREPART', 'D', 'Junior Manager', 'Section Head', 'Section Head of SPAREPART', 19),
(7, 'MKT Activity', 'D', 'Junior Manager', 'Section Head', 'Section Head of MKTA', 19);

-- --------------------------------------------------------

--
-- Table structure for table `supervisor`
--

CREATE TABLE IF NOT EXISTS `supervisor` (
  `supervisor_id` int(11) NOT NULL,
  `supervisor_name` varchar(150) NOT NULL DEFAULT '0',
  `supervisor_grade` varchar(25) NOT NULL DEFAULT '0',
  `supervisor_rank` varchar(25) NOT NULL DEFAULT '0',
  `supervisor_title` varchar(50) NOT NULL DEFAULT '0',
  `supervisor_job_title` varchar(150) NOT NULL DEFAULT '0',
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervisor`
--

INSERT INTO `supervisor` (`supervisor_id`, `supervisor_name`, `supervisor_grade`, `supervisor_rank`, `supervisor_title`, `supervisor_job_title`, `section_id`) VALUES
(1, 'RMW', 'E', 'Supervisor', 'Supervisor', 'Supervisor of RMW', 1),
(2, 'FGW / DC', 'E', 'Supervisor', 'Supervisor', 'Supervisor of FGW / DC', 2),
(3, 'PPIC', 'E', 'Supervisor', 'Supervisor', 'Supervisor of PPIC', 3),
(4, 'SCM Plant', 'E', 'Supervisor', 'Supervisor', 'Supervisor of SCM Plant', 4),
(5, 'RMPM', 'E', 'Supervisor', 'Supervisor', 'Supervisor of RMPM', 5),
(6, 'ATK / SPAREPART', 'E', 'Supervisor', 'Supervisor', 'Supervisor of SPAREPART', 6),
(7, 'MKT Activity', 'E', 'Supervisor', 'Supervisor', 'Supervisor of MKTA', 7);

-- --------------------------------------------------------

--
-- Table structure for table `work_location`
--

CREATE TABLE IF NOT EXISTS `work_location` (
  `work_location_id` int(11) NOT NULL,
  `work_location_name` varchar(50) NOT NULL DEFAULT '',
  `work_location_desc` varchar(150) DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `work_location`
--

INSERT INTO `work_location` (`work_location_id`, `work_location_name`, `work_location_desc`) VALUES
(1, 'BANDUNG', '-'),
(2, 'BEKASI', '-'),
(3, 'JAKARTA', '-'),
(4, 'JOGJAKARTA', '-'),
(5, 'LAMPUNG', '-'),
(6, 'PONTIANAK', '-');

-- --------------------------------------------------------

--
-- Table structure for table `ws_company`
--

CREATE TABLE IF NOT EXISTS `ws_company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(50) NOT NULL DEFAULT '',
  `company_logo` varchar(150) DEFAULT '',
  `company_start_date` date DEFAULT NULL,
  `company_vision` varchar(255) DEFAULT '',
  `company_mission` varchar(255) DEFAULT '',
  `company_is_holding` tinyint(1) DEFAULT NULL,
  `company_type` varchar(50) DEFAULT '',
  `company_business_type` varchar(50) DEFAULT '',
  `company_address_country` varchar(150) DEFAULT '',
  `company_address_city` varchar(150) DEFAULT '',
  `company_address_location` varchar(255) DEFAULT '',
  `company_address_zip_code` varchar(15) DEFAULT '',
  `company_contact_phone` varchar(15) DEFAULT '',
  `company_contact_fax` varchar(15) DEFAULT '',
  `company_contact_email` varchar(50) DEFAULT '',
  `company_tax_npkp` varchar(50) DEFAULT '',
  `company_tax_npwp` varchar(50) DEFAULT '',
  `company_tax_location` varchar(50) DEFAULT '',
  `company_bank_name` varchar(50) DEFAULT '',
  `company_bank_account_no` varchar(25) DEFAULT '',
  `company_bank_account_name` varchar(50) DEFAULT '',
  `created_by` int(11) DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT '0',
  `updated_on` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ws_company`
--

INSERT INTO `ws_company` (`id`, `company_name`, `company_logo`, `company_start_date`, `company_vision`, `company_mission`, `company_is_holding`, `company_type`, `company_business_type`, `company_address_country`, `company_address_city`, `company_address_location`, `company_address_zip_code`, `company_contact_phone`, `company_contact_fax`, `company_contact_email`, `company_tax_npkp`, `company_tax_npwp`, `company_tax_location`, `company_bank_name`, `company_bank_account_no`, `company_bank_account_name`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 'CV. ASIATOP', '20181011235122-2018-10-11ws_company235119.jpg', '2018-10-10', 'Visi', 'Misi', 0, 'Manufacture', 'Retail', 'Indonesia', 'Jakarta Utara', 'Ruko Tama Indah Kav 36 CW, Jembatan 3, Pluit', '14450', '(021) 6627989', '', 'ASW-marketing@asiatop.co.id', '', '', '', '', '', '', 1, '2018-10-11 09:51:22', 1, '2018-10-11 09:51:22');

-- --------------------------------------------------------

--
-- Table structure for table `ws_grade`
--

CREATE TABLE IF NOT EXISTS `ws_grade` (
  `id` int(11) NOT NULL,
  `grade_name` varchar(50) NOT NULL,
  `grade_value` int(11) DEFAULT NULL,
  `grade_pay_min` decimal(10,0) DEFAULT NULL,
  `grade_pay_max` decimal(10,0) DEFAULT NULL,
  `grade_pay_Q1` decimal(10,0) DEFAULT NULL,
  `grade_pay_Q2` decimal(10,0) DEFAULT NULL,
  `grade_pay_Q3` decimal(10,0) DEFAULT NULL,
  `grade_pay_Q4` decimal(10,0) DEFAULT NULL,
  `grade_pay_mid` decimal(10,0) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT '0',
  `updated_on` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ws_grade`
--

INSERT INTO `ws_grade` (`id`, `grade_name`, `grade_value`, `grade_pay_min`, `grade_pay_max`, `grade_pay_Q1`, `grade_pay_Q2`, `grade_pay_Q3`, `grade_pay_Q4`, `grade_pay_mid`, `company_id`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(3, 'S1', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2018-10-11 10:11:07', 1, '2018-10-11 10:11:07'),
(4, 'S2', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2018-10-11 10:11:33', 1, '2018-10-11 10:11:33'),
(5, 'S3', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2018-10-11 10:11:41', 1, '2018-10-11 10:11:41'),
(6, 'S4', 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2018-10-11 10:47:49', 1, '2018-10-11 10:47:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_group_to_group`
--
ALTER TABLE `aauth_group_to_group`
  ADD PRIMARY KEY (`group_id`,`subgroup_id`);

--
-- Indexes for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perm_to_user`
--
ALTER TABLE `aauth_perm_to_user`
  ADD PRIMARY KEY (`user_id`,`perm_id`);

--
-- Indexes for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_user`
--
ALTER TABLE `aauth_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_users`
--
ALTER TABLE `aauth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_user_to_group`
--
ALTER TABLE `aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`);

--
-- Indexes for table `cc_options`
--
ALTER TABLE `cc_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `corporate`
--
ALTER TABLE `corporate`
  ADD PRIMARY KEY (`corporate_id`);

--
-- Indexes for table `crud`
--
ALTER TABLE `crud`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_custom_option`
--
ALTER TABLE `crud_custom_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_field`
--
ALTER TABLE `crud_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_field_validation`
--
ALTER TABLE `crud_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_input_type`
--
ALTER TABLE `crud_input_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crud_input_validation`
--
ALTER TABLE `crud_input_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `directorate`
--
ALTER TABLE `directorate`
  ADD PRIMARY KEY (`directorate_id`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`division_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_custom_attribute`
--
ALTER TABLE `form_custom_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_custom_option`
--
ALTER TABLE `form_custom_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_employees`
--
ALTER TABLE `form_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_field`
--
ALTER TABLE `form_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `form_field_validation`
--
ALTER TABLE `form_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fptk`
--
ALTER TABLE `fptk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `grade_job`
--
ALTER TABLE `grade_job`
  ADD PRIMARY KEY (`grade_id`);

--
-- Indexes for table `grade_level`
--
ALTER TABLE `grade_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_training`
--
ALTER TABLE `jadwal_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobtitle`
--
ALTER TABLE `jobtitle`
  ADD PRIMARY KEY (`jobtitle_id`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materi_training`
--
ALTER TABLE `materi_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_type`
--
ALTER TABLE `menu_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `officer`
--
ALTER TABLE `officer`
  ADD PRIMARY KEY (`officer_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_block_element`
--
ALTER TABLE `page_block_element`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pan`
--
ALTER TABLE `pan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengajuan_fptk`
--
ALTER TABLE `pengajuan_fptk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengajuan_training`
--
ALTER TABLE `pengajuan_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permohonan_cuti`
--
ALTER TABLE `permohonan_cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permohonan_izin`
--
ALTER TABLE `permohonan_izin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permohonan_sakit`
--
ALTER TABLE `permohonan_sakit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `rest`
--
ALTER TABLE `rest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest_field`
--
ALTER TABLE `rest_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest_field_validation`
--
ALTER TABLE `rest_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rest_input_type`
--
ALTER TABLE `rest_input_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `supervisor`
--
ALTER TABLE `supervisor`
  ADD PRIMARY KEY (`supervisor_id`);

--
-- Indexes for table `work_location`
--
ALTER TABLE `work_location`
  ADD PRIMARY KEY (`work_location_id`);

--
-- Indexes for table `ws_company`
--
ALTER TABLE `ws_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ws_grade`
--
ALTER TABLE `ws_grade`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=628;
--
-- AUTO_INCREMENT for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aauth_user`
--
ALTER TABLE `aauth_user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aauth_users`
--
ALTER TABLE `aauth_users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cc_options`
--
ALTER TABLE `cc_options`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `corporate`
--
ALTER TABLE `corporate`
  MODIFY `corporate_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `crud`
--
ALTER TABLE `crud`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `crud_custom_option`
--
ALTER TABLE `crud_custom_option`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5401;
--
-- AUTO_INCREMENT for table `crud_field`
--
ALTER TABLE `crud_field`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7990;
--
-- AUTO_INCREMENT for table `crud_field_validation`
--
ALTER TABLE `crud_field_validation`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3560;
--
-- AUTO_INCREMENT for table `crud_input_type`
--
ALTER TABLE `crud_input_type`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `crud_input_validation`
--
ALTER TABLE `crud_input_validation`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `directorate`
--
ALTER TABLE `directorate`
  MODIFY `directorate_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `division_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_custom_attribute`
--
ALTER TABLE `form_custom_attribute`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `form_custom_option`
--
ALTER TABLE `form_custom_option`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `form_employees`
--
ALTER TABLE `form_employees`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `form_field`
--
ALTER TABLE `form_field`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `form_field_validation`
--
ALTER TABLE `form_field_validation`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fptk`
--
ALTER TABLE `fptk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `grade_job`
--
ALTER TABLE `grade_job`
  MODIFY `grade_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `grade_level`
--
ALTER TABLE `grade_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `jadwal_training`
--
ALTER TABLE `jadwal_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jobtitle`
--
ALTER TABLE `jobtitle`
  MODIFY `jobtitle_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `materi_training`
--
ALTER TABLE `materi_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `menu_type`
--
ALTER TABLE `menu_type`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `officer`
--
ALTER TABLE `officer`
  MODIFY `officer_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_block_element`
--
ALTER TABLE `page_block_element`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pan`
--
ALTER TABLE `pan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pengajuan_fptk`
--
ALTER TABLE `pengajuan_fptk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengajuan_training`
--
ALTER TABLE `pengajuan_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permohonan_cuti`
--
ALTER TABLE `permohonan_cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permohonan_izin`
--
ALTER TABLE `permohonan_izin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permohonan_sakit`
--
ALTER TABLE `permohonan_sakit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `rest`
--
ALTER TABLE `rest`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rest_field`
--
ALTER TABLE `rest_field`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `rest_field_validation`
--
ALTER TABLE `rest_field_validation`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `rest_input_type`
--
ALTER TABLE `rest_input_type`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `supervisor`
--
ALTER TABLE `supervisor`
  MODIFY `supervisor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `work_location`
--
ALTER TABLE `work_location`
  MODIFY `work_location_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ws_company`
--
ALTER TABLE `ws_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ws_grade`
--
ALTER TABLE `ws_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
